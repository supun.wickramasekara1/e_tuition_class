<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentDetails extends Model
{

    protected $fillable = [
        'module_content_id', 'user_id', 'uploaded_file', 'mark'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function moduleContent()
    {
        return $this->belongsTo(ModuleContent::class);
    }
}
