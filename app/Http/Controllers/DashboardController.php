<?php

namespace App\Http\Controllers;

use App\Module;
use App\SubscribeDetail;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'checkStatus', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboardView()
    {
        $moduleIdsArray = Auth::user()->modules()->pluck('id')->toArray();
        $moduleObj = Auth::user()->modules()->select('status','type')->get();
        $typeObj = $moduleObj->groupBy('type');
        $statusObj = $moduleObj->groupBy('status');
        $approvedCount = SubscribeDetail::whereIn('module_id', $moduleIdsArray)->where('status', 'APPROVED')->count();
        $modulesObj = Auth::user()->modules()->with('subModules', 'user', 'subject', 'subscribeDetails')->orderBy('created_at', 'desc')->take(3);
        $modulesObj = $modulesObj->orderBy('created_at', 'asc')->get();
        $dashboardCounts = [
            'pubCounts' => isset($statusObj['PUBLISHED']) ? count($statusObj['PUBLISHED']) : 0,
            'onCounts' => isset($typeObj['ONLINE']) ? count($typeObj['ONLINE']) : 0,
            'offCounts' => isset($typeObj['OFFLINE']) ? count($typeObj['OFFLINE']) : 0,
            'subCounts' => $approvedCount,
        ];

        return view('dashboard.index', ['stats' => $dashboardCounts, 'modules' => $modulesObj->toArray()]);
    }
}
