<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Module;
use App\ModuleComment;
use App\Subject;
use App\SubModule;
use App\SubscribeDetail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'checkStatus']);
    }

    function moduleView()
    {
        $tableData = Auth::user()->modules()->with('grade')->with('subject')->get()->toArray();
        $grades = Grade::all()->toArray();
        $userSubjects = Auth::user()->subjects()->get()->toArray();
        return view('modules.index', ['grades' => $grades, 'subjects' => $userSubjects, 'tableData' => $tableData]);
    }

    function addModule(Request $request)
    {
        $this->validateModule($request);
        $requestData = $request->all();

        if ($requestData['id']) {
            $updateModuleObj = Module::find($requestData['id']);
            $updateModuleObj->grade_id = $requestData['grade_id'];
            $updateModuleObj->subject_id = $requestData['subject_id'];
            $updateModuleObj->type = $requestData['type'];
            $updateModuleObj->topic = $requestData['topic'];
            $updateModuleObj->hours = $requestData['hours'];
            $updateModuleObj->short_desc = $requestData['short_desc'];
            if (isset($requestData['is_free'])) {
                $updateModuleObj->is_free = true;
            } else {
                $updateModuleObj->is_free = false;
                $updateModuleObj->module_fee = number_format($requestData['module_fee'], 2, '.', '');
            }

            if ($request->hasFile('module_image')) {
                $image = $request->file('module_image');
                $imageName = 'module_' . time() . '.' . $image->getClientOriginalExtension();
                $request->module_image->storeAs('modules', $imageName, 'public');
                $updateModuleObj->module_image = $imageName;
            } else {
                $updateModuleObj->module_image = $requestData['module_image_hidden'];
            }

            $updateModuleResp = $updateModuleObj->save();
            if ($updateModuleResp) {
                return Redirect::back()->with('message', 'Module has been update successfully.');
            }
        } else {
            $newModuleObj = new Module();
            $newModuleObj->user_id = Auth::user()->id;
            $newModuleObj->grade_id = $requestData['grade_id'];
            $newModuleObj->subject_id = $requestData['subject_id'];
            $newModuleObj->topic = $requestData['topic'];
            $newModuleObj->type = $requestData['type'];
            $newModuleObj->short_desc = $requestData['short_desc'];
            if (isset($requestData['is_free'])) {
                $newModuleObj->is_free = true;
            } else {
                $newModuleObj->module_fee = number_format($requestData['module_fee'], 2, '.', '');
            }
            if ($requestData['type'] == 'ONLINE') {
                for ($i = 0; $i < $requestData['occurrence']; $i++) {
                    $scheduleDate = Carbon::create($requestData['scheduled_at'])->addWeeks($i)->format('Y-m-d H:i');

                    $isAvail = $this->isScheduleAvailable($scheduleDate, $requestData['hours'], $requestData['occurrence']);
                    if ($isAvail) {
                        return Redirect::back()->withInput()->with('error', 'This schedule date conflict with other schedule you entered earlier.');
                    }
                }
                $newModuleObj->hours = $requestData['hours'];
                $newModuleObj->occurrence = $requestData['occurrence'];
                $newModuleObj->scheduled_at = $requestData['scheduled_at'];
            }

            if ($request->hasFile('module_image')) {
                $image = $request->file('module_image');
                $imageName = 'module_' . time() . '.' . $image->getClientOriginalExtension();
                $request->module_image->storeAs('modules', $imageName, 'public');
                $newModuleObj->module_image = $imageName;
            } else {
                $newModuleObj->module_image = $requestData['image_hidden'];
            }

            $newModuleResp = $newModuleObj->save();
            if ($newModuleResp) {
                $newScheduleResp = $this->addSubModule($requestData, $newModuleObj->id);
                if ($newScheduleResp) {
                    return Redirect::back()->with('message', 'Module has been added successfully.');
                }
            }
        }
        return Redirect::back()->withInput()->withErrors('Something went wrong. Please try again later.');
    }

    private function isScheduleAvailable($scheduled_at, $duration, $occurrence)
    {
        $reqTime = explode(':', $duration);
        $requestFromTime = $scheduled_at;
        $requestToTime = Carbon::create($scheduled_at)->addHours($reqTime[0])->addMinutes($reqTime[1])->format('Y-m-d H:i');

        $moduleIds = Auth::user()->modules()->where('type', 'ONLINE')->pluck('id')->toArray();
        $subModuleObj = SubModule::with('module')->whereIn('module_id', $moduleIds)->get()->toArray();
        foreach ($subModuleObj as $obj) {
            $time = explode(':', $obj['module']['hours']);
            $fromTime = $obj['scheduled_at'];
            $toTime = Carbon::create($obj['scheduled_at'])->addHours($time[0])->addMinutes($time[1])->format('Y-m-d H:i');
            if ($requestFromTime > $fromTime && $requestFromTime < $toTime) {
                return true;
            }
            if ($requestToTime > $fromTime && $requestToTime < $toTime) {
                return true;
            }
        }
        return false;
    }

    private function addSubModule($requestData, $moduleId)
    {
        $newSubModuleResp = false;
        if ($requestData['type'] == 'ONLINE' && $requestData['occurrence'] > 1) {
            for ($i = 0; $i < $requestData['occurrence']; $i++) {
                $scheduleDate = Carbon::create($requestData['scheduled_at'])->addWeeks($i)->format('Y-m-d H:i');
                $newSubModuleObj = new SubModule();
                $newSubModuleObj->module_id = $moduleId;
                $newSubModuleObj->scheduled_at = $scheduleDate;
                $newSubModuleResp = $newSubModuleObj->save();
            }
        } else {
            $newSubModuleObj = new SubModule();
            $newSubModuleObj->module_id = $moduleId;
            $newSubModuleResp = $newSubModuleObj->save();
        }
        return $newSubModuleResp;
    }

    private function updateSubModule($requestData, $moduleId)
    {
        $newSubModuleResp = false;
        if ($requestData['type'] == 'ONLINE' && $requestData['occurrence'] > 1) {
            for ($i = 0; $i < $requestData['occurrence']; $i++) {
                $newSubModuleObj = new SubModule();
                $newSubModuleObj->module_id = $moduleId;
                $newSubModuleObj->scheduled_at = Carbon::create($requestData['scheduled_at'])->addWeeks($i)->toDateTimeString();
                $newSubModuleResp = $newSubModuleObj->save();
            }
        } else {
            $newSubModuleObj = new SubModule();
            $newSubModuleObj->module_id = $moduleId;
            $newSubModuleResp = $newSubModuleObj->save();
        }
        return $newSubModuleResp;
    }

    private function validateModule($request)
    {
        $rules = [
            'grade_id' => 'required',
            'subject_id' => 'required',
            'topic' => 'required',
            'short_desc' => 'required|min:100',
        ];

        if (!$request->input('is_free')) {
            $rules['module_fee'] = 'required|numeric|gt:0';
        }
        if ($request->input('type') === 'ONLINE') {
            $rules['hours'] = 'required|date_format:H:i';
            $rules['occurrence'] = 'required';
            $rules['scheduled_at'] = 'required';
        }

        if ($request->hasFile('module_image') && $request->input('module_image_hidden')) {
            $rules['module_image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:1048';
        } else {
            if (!$request->input('module_image_hidden')) {
                $rules['module_image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1048';
            }
        }

        $customMessages = [
            'grade_id.required' => 'Grade is required.',
            'subject_id.required' => 'Subject is required.',
            'topic.required' => 'Topic is required.',
            'module_fee.required' => 'Module fee is required.',
            'module_fee.min' => 'Module fee should be more than 0.',
            'module_fee.gt' => 'Module fee should be more than 0.',
            'scheduled_at.required' => 'Module start date/time is required.',
            'hours.required' => 'Module Duration is required.',
            'hours.date_format' => 'Invalid format. Format should be XX:XX',
            'occurrence.required' => 'Sub Module count is required.',
            'short_desc.required' => 'Short Description is required.',
            'short_desc.min' => 'Short Description should have more then 200 characters.',
            'module_image.image' => 'Module image should be an image.',
            'module_image.max' => 'Module image size should be less than 1MB.',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    function getModuleView($id)
    {
        $moduleDataObj = Module::find($id);
        if ($moduleDataObj) {
            $grades = Grade::all()->toArray();
            $userSubjects = Auth::user()->subjects()->get()->toArray();
            return json_encode([
                'valid' => true,
                'moduleFormHtml' => view('modules.form', ['grades' => $grades, 'subjects' => $userSubjects, 'moduleData' => $moduleDataObj])->render()
            ]);
        }
        return json_encode([
            'valid' => false,
            'message' => 'Something went wrong. Please try again later.'
        ]);
    }

    function deleteModule($id)
    {
        $moduleDataObj = Module::find($id);
        $valid = false;
        $message = 'Something went wrong. Please try again later.';
        if ($moduleDataObj) {
            $subscribedObj = SubscribeDetail::where('module_id', $moduleDataObj->id)->first();
            if ($subscribedObj) {
                $message = 'Module can\'t delete. One or more student subscribed with this module.';
            } else {
                if ($moduleDataObj->status === 'PUBLISHED') {
                    $message = 'Module can\'t delete. Unpublish the module before delete.';
                } else {
                    $moduleDataObj->delete();
                    $valid = true;
                    $message = 'Module deleted successfully.';
                }
            }
        }
        return json_encode([
            'valid' => $valid,
            'message' => $message
        ]);
    }

    function publishModule($id)
    {
        $moduleDataObj = Module::with('subModules', 'subject')->find($id);
        $valid = true;
        $message = 'Module published successfully.';

        foreach ($moduleDataObj->toArray()['sub_modules'] as $subModule) {
            if ($subModule['status'] == 'INITIAL') {
                return json_encode([
                    'valid' => false,
                    'message' => 'Please update all sub modules data before publish the module.'
                ]);
            }
        }

        if ($moduleDataObj->status == 'PUBLISHED') {
            $valid = false;
            $message = 'This module already published. Please refresh the page.';
        } else {
//            Notification::send(User::all(), new ModulePublish($moduleDataObj)); TODO - 20210628
            $moduleDataObj->status = 'PUBLISHED';
            $moduleDataObj->save();
        }
        return json_encode([
            'valid' => $valid,
            'message' => $message,
            'moduleTableHtml' => view('modules.table', ['tableData' => Auth::user()->modules()->get()])->render()
        ]);
    }

    function unpublishModule($id)
    {
        $subscribedArray = SubscribeDetail::where('module_id', $id)->get()->toArray();
        $moduleDataObj = Module::find($id);
        $valid = true;
        $message = 'Module unpublished successfully.';

        if ($subscribedArray) {
            return json_encode([
                'valid' => false,
                'message' => 'This module can\'t be unpublish, because this is already subscribed by students.'
            ]);
        }

        if ($moduleDataObj->status == 'UNPUBLISHED') {
            $valid = false;
            $message = 'This module already unpublished. Please refresh the page.';
        } else {
            $moduleDataObj->status = 'UNPUBLISHED';
            $moduleDataObj->save();
        }
        return json_encode([
            'valid' => $valid,
            'message' => $message,
            'moduleTableHtml' => view('modules.table', ['tableData' => Auth::user()->modules()->get()])->render()
        ]);
    }

    function holdModule($id)
    {
        $moduleDataObj = Module::find($id);
        $valid = true;
        $message = 'Module hold successfully.';

        if ($moduleDataObj->status == 'HOLD') {
            $valid = false;
            $message = 'This module already hold. Please refresh the page.';
        } else {
            $moduleDataObj->status = 'HOLD';
            $moduleDataObj->save();
        }
        return json_encode([
            'valid' => $valid,
            'message' => $message,
            'moduleTableHtml' => view('modules.table', ['tableData' => Auth::user()->modules()->get()])->render()
        ]);
    }

    function classBoardView()
    {
        $subjects = Subject::get(['id', 'name']);
        $subscribedModuleIds = Auth::user()->subscribeDetails()->pluck('module_id')->toArray();
        $subscribedModuleIds = $this->removeExpireModules($subscribedModuleIds); // TODO : Filter Expire Module
        $pendingModuleIds = $this->getPendingModuleIds();

        $relatedModulesObj = Module::with('subModules', 'user', 'subject', 'subscribeDetails')->where('grade_id', Auth::user()->grade_id)->whereIn('status', ['PUBLISHED', 'HOLD'])->get();
        $relatedModulesIds = $relatedModulesObj->pluck('id')->toArray();
        $relatedModulesIds = $this->removeExpireModules($relatedModulesIds); // TODO : Filter Expire Module
        $relatedModulesObj = $relatedModulesObj->whereIn('id', $relatedModulesIds);
        $subscribedCount = $relatedModulesObj->whereIn('id', $subscribedModuleIds)->count();
        $otherCount = $relatedModulesObj->whereNotIn('id', $subscribedModuleIds)->whereIn('status', ['PUBLISHED'])->count();
        $counts = [
            'subCount' => $subscribedCount,
            'otherCount' => $otherCount
        ];
        return view('class_board.index', ['modules' => $relatedModulesObj->toArray(), 'counts' => $counts, 'pendingModuleIds' => $pendingModuleIds, 'subscribedModuleIds' => $subscribedModuleIds, 'subjects' => $subjects]);
    }

    private function removeExpireModules($reqArray)
    {
        $existingModuleArray = [];
        $index = 0;
        $moduleArray = Module::whereIn('id', $reqArray)->with('subModules')->get()->toArray();
        foreach ($moduleArray as $module) {
            if (Carbon::parse($module['sub_modules'][count($module['sub_modules']) - 1]['scheduled_at']) > Carbon::now()) {
                $existingModuleArray[$index] = $module['id'];
                $index++;
            } else {
                if ($module['type'] === 'OFFLINE') {
                    $existingModuleArray[$index] = $module['id'];
                    $index++;
                }
            }
        }
        return $existingModuleArray;
    }

    function filterClassBoardView(Request $request)
    {
        $relatedModulesObj = Module::with('subModules', 'user', 'subject', 'subscribeDetails')->where('grade_id', Auth::user()->grade_id)->whereIn('status', ['PUBLISHED', 'HOLD'])->get();
        $subscribedModuleIds = Auth::user()->subscribeDetails()->pluck('module_id')->toArray();
        $pendingModuleIds = $this->getPendingModuleIds();
        if ($request->filter_subject) {
            $relatedModulesObj = $relatedModulesObj->where('subject_id', $request->filter_subject);
        }
        if ($request->search_field) {
            $userIds = User::where('name', 'LIKE', '%' . $request->search_field . '%')->pluck('id')->toArray();
            $moduleIds = Module::where('topic', 'LIKE', '%' . $request->search_field . '%')->pluck('id')->toArray();
            $relatedModulesFilterObj = $relatedModulesObj->whereIn('user_id', $userIds);
            if (!count($relatedModulesFilterObj->toArray()) > 0) {
                $relatedModulesFilterObj = $relatedModulesObj->whereIn('id', $moduleIds);
            }
            $relatedModulesObj = $relatedModulesFilterObj;
        }

        $subscribedCount = $relatedModulesObj->whereIn('id', $subscribedModuleIds)->count();
        $otherCount = $relatedModulesObj->whereNotIn('id', $subscribedModuleIds)->whereIn('status', ['PUBLISHED'])->count();
        $counts = [
            'subCount' => $subscribedCount,
            'otherCount' => $otherCount
        ];
        return json_encode([
            'valid' => true,
            'modulesHtml' => view('class_board.related_modules', ['modules' => $relatedModulesObj->toArray(), 'counts' => $counts, 'subscribedModuleIds' => $subscribedModuleIds, 'pendingModuleIds' => $pendingModuleIds])->render()
        ]);
    }

    function addReview(Request $request)
    {
        $request = $request->all();
        $valid = true;
        $message = 'Review added successfully.';
        $userId = Auth::user()->id;
        $subscribedObj = SubscribeDetail::where('module_id', $request['module_id'])->where('user_id', $userId)->first();
        if (isset($subscribedObj->id) && $subscribedObj->status === 'APPROVED') {
            $moduleCommentObj = ModuleComment::where('subscribe_id', $subscribedObj->id)->first();
            if (!$moduleCommentObj) {
                $moduleCommentObj = new ModuleComment();
                $moduleCommentObj->subscribe_id = $subscribedObj->id;
                $moduleCommentObj->rating = $request['star'];
                $moduleCommentObj->comment = $request['comment'];
                $moduleCommentObj->save();
            } else {
                $valid = false;
                $message = 'Your already reviewed this Module.';
            }
        } else {
            $valid = false;
            $message = 'You can add review to this Module, hence your not subscribe it.';
        }

        return json_encode([
            'valid' => $valid,
            'message' => $message
        ]);
    }

    private function getPendingModuleIds()
    {
        $pendingModuleIds = Auth::user()->subscribeDetails()->where('status', 'PENDING')->pluck('module_id')->toArray();
        return $pendingModuleIds;
    }

    function viewModule($id)
    {
        $userType = 'REG';
        $userTypeMsg = '';
        if (Auth::user()->user_type === 'STUDENT') {
            $moduleArray = Auth::user()->subscribeDetails()->where('module_id', $id)->first();
            if ($moduleArray) {
                if ($moduleArray['status'] === 'PENDING') {
                    $userType = 'PENDING';
                    $userTypeMsg = 'The module subscription is still pending. You can\'t view all the contents related to the Module.';
                }
            } else {
                $userType = 'NO_REG';
                $userTypeMsg = 'You are not subscribe to this Module. You can\'t view all the contents related to the Module.';
            }
        }

        $moduleObj = Module::with('user', 'subModules', 'grade', 'subject')->find($id);
        $overallRating = $this->getOverallRatingByUser($moduleObj->user->id);
        $overallRatingByModule = $this->getOverallRatingByModule($moduleObj->id);
        return view('modules.view', ['userType' => $userType, 'userTypeMsg' => $userTypeMsg, 'moduleData' => $moduleObj->toArray(), 'overallRating' => $overallRating, 'ratingByModule' => $overallRatingByModule]);
    }

    function getOverallRatingByUser($tutorId)
    {
        $moduleIds = Module::where('user_id', $tutorId)->where('status', 'PUBLISHED')->pluck('id')->toArray();
        $subIds = SubscribeDetail::whereIn('module_id', $moduleIds)->pluck('id')->toArray();
        $ratingDetails = ModuleComment::whereIn('subscribe_id', $subIds)->get()->toArray();
        $ratingArrCount = count($ratingDetails);
        $ratingTotal = array_sum(array_column($ratingDetails, 'rating'));
        $overallRating = ($ratingArrCount) ? round($ratingTotal / $ratingArrCount) : 0;
        return (int)$overallRating;
    }

    function getOverallRatingByModule($moduleId)
    {
        $subIds = SubscribeDetail::where('module_id', $moduleId)->pluck('id')->toArray();
        $ratingDetails = ModuleComment::whereIn('subscribe_id', $subIds)->get()->toArray();
        $latestRatingDetails = ModuleComment::whereIn('subscribe_id', $subIds)->latest()->take(5)->get()->toArray();
        $ratingArrCount = count($ratingDetails);
        $ratingTotal = array_sum(array_column($ratingDetails, 'rating'));
        $overallRating = ($ratingArrCount) ? round($ratingTotal / $ratingArrCount) : 0;
        $returnObj = [
            'overallRating' => (int)$overallRating,
            'ratingDetails' => $latestRatingDetails
        ];
        return $returnObj;
    }

    function viewSubscribe($id)
    {
        return view('subscribed.index', ['id' => $id]);
    }

    function scheduleView(Request $request)
    {
        if ($request->ajax()) {
            if (Auth::user()->user_type == 'STUDENT') {
                $moduleIds = Auth::user()->subscribeDetails()->where('status', 'APPROVED')->pluck('module_id')->toArray();
                $scheduleData = SubModule::with('module')->whereIn('module_id', $moduleIds)->get()->toArray();
            } else {
                $moduleIds = Auth::user()->modules()->where('type', 'ONLINE')->pluck('id')->toArray();
                $scheduleData = SubModule::with('module')->whereIn('module_id', $moduleIds)->get()->toArray();
            }

            $data = [];
            foreach ($scheduleData as $key => $schedule) {
                if ($schedule['module']['type'] == 'ONLINE') {
                    $resp = $this->getSubjectById($schedule['module']['subject_id']);
                    $time = explode(':', $schedule['module']['hours']);

                    $data[$key]['title'] = $schedule['sub_topic'];
                    $data[$key]['description'] = ($schedule['status'] === 'ACTIVE') ? $resp['name'] . ' - ' . $schedule['content'] : $resp['name'] . ' - ' . 'This sub module is ' . $schedule['status'] . '.';
//                    $data[$key]['backgroundColor'] = $resp['color'];
//                    $data[$key]['borderColor'] = 'red';
                    $data[$key]['color'] = ($schedule['status'] === 'CANCELED') ? 'red' : 'green';
                    $data[$key]['start'] = $schedule['scheduled_at'];
                    $data[$key]['end'] = Carbon::create($schedule['scheduled_at'])->addHours($time[0])->addMinutes($time[1])->toDateTimeString();
                }
            }
            return response()->json($data);
        }
        return view('schedule_details.index');
    }

    private function getSubjectById($id)
    {
        $subject = Subject::find($id);
        return ['name' => $subject['name']];
    }
}
