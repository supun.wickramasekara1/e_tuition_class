<?php

namespace App\Http\Controllers;

use App\AssignmentDetails;
use App\ModuleContent;
use App\Notifications\SubModuleCancel;
use App\SubModule;
use App\SubscribeDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SubModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'checkStatus']);
    }

    function subModuleView()
    {
        $tableData = [];
        $modulesObj = Auth::user()->modules()->get()->toArray();
        return view('sub_modules.index', ['modules' => $modulesObj, 'tableData' => $tableData]);
    }

    function cancelSubModule($id)
    {
        $subModuleDataObj = SubModule::find($id);
        $valid = true;
        $message = 'Sub module canceled successfully.';
        $tableDataObj = [];

        if ($subModuleDataObj->status == 'CANCELED') {
            $valid = false;
            $message = 'This sub module already canceled. Please refresh the page.';
        } else {
            $subModuleDataObj->status = 'CANCELED';
            $subModuleDataObj->save();

            $usersIdsArray = SubscribeDetail::where('module_id', $subModuleDataObj->module_id)->pluck('user_id')->toArray();
            Notification::send(User::whereIn('id', $usersIdsArray)->get(), new SubModuleCancel($subModuleDataObj));
            $tableDataObj = SubModule::where('module_id', $subModuleDataObj->module_id)->get();
        }
        return json_encode([
            'valid' => $valid,
            'message' => $message,
            'subModuleTable' => view('sub_modules.table', ['tableData' => $tableDataObj])->render()
        ]);
    }

    function viewSubModule($id)
    {
        $subModuleArray = SubModule::with('moduleContents', 'module')->find($id)->toArray();
        if (Auth::user()->user_type === 'TUTOR') {
            $moduleId = Auth::user()->modules()->find($subModuleArray['module_id']);
        } else {
            $moduleId = Auth::user()->subscribeDetails()->where('module_id', $subModuleArray['module_id']);
        }

        if ($moduleId) {
            $subModuleArray = $this->addAssignmentData($subModuleArray);
            return view('sub_modules.view', ['subModuleData' => $subModuleArray]);
        }
        return Redirect::back()->with('error', 'Something went wrong. Please try again later.');
    }

    function addAssignmentData($subModuleArray)
    {
        foreach ($subModuleArray['module_contents'] as $key => $content) {
            if ($content['content_type'] === 'ASSIGNMENT') {
                $assignmentData = AssignmentDetails::where('module_content_id', $content['id'])->where('user_id', Auth::user()->id)->first();
                $subModuleArray['module_contents'][$key]['assignment'] = $assignmentData;
            }
        }
        return $subModuleArray;
    }

    function schedulesByModule($id)
    {
        $subModuleObj = SubModule::with('moduleContents')->where('module_id', $id)->get();
        if ($subModuleObj) {
            $scheduleHtml = $this->createScheduleHtml($subModuleObj);
            foreach ($subModuleObj as $key => $obj) {
                $subModuleObj[$key]['counts'] = $this->uploadContentCount($obj->toArray());
            }
            return json_encode([
                'valid' => true,
                'scheduleHtml' => $scheduleHtml,
                'subModuleTable' => view('sub_modules.table', ['tableData' => $subModuleObj])->render()
            ]);
        }
        return json_encode([
            'valid' => false,
            'message' => 'No sub modules associated with selected module.'
        ]);
    }

    private function createScheduleHtml($subModuleObj)
    {
        $scheduleHtml = '';
        foreach ($subModuleObj as $subModule) {
            if (!$subModule->sub_topic) {
                $scheduleHtml .= "<option value='{$subModule->id}'>{$subModule->scheduled_at} - {$subModule->sub_topic}</option>";
            }
        }
        return $scheduleHtml;
    }

    function getSubModuleView($id)
    {
        $subModuleDataObj = SubModule::find($id);
        if ($subModuleDataObj) {
            return json_encode([
                'valid' => true,
                'moduleId' => $subModuleDataObj['module_id'],
                'subModuleFormPartHtml' => view('sub_modules.form_part', ['subModuleData' => $subModuleDataObj, 'subModules' => $subModuleDataObj->toArray()])->render()
            ]);
        }
        return json_encode([
            'valid' => false,
            'message' => 'Something went wrong. Please try again later.'
        ]);
    }

    function addSubModule(Request $request)
    {
        $validator = $this->validateSubModule($request);
        $valid = false;
        $message = 'Something went wrong. Please try again later.';

        if ($validator->passes()) {
            $requestData = $request->all();
            $subModulesObj = [];
            $subModuleObj = SubModule::find($requestData['sub_module_id']);
            if ($subModuleObj) {
                $subModuleObj->sub_topic = $requestData['sub_topic'];
                $subModuleObj->content = $requestData['content'];
                $subModuleObj->url = $requestData['module_url'];
                $subModuleObj->status = 'ACTIVE';
                $updateSubModuleResp = $subModuleObj->save();

                if ($updateSubModuleResp) {
                    $subModulesObj = SubModule::where('module_id', $requestData['module_id'])->get();
                    $valid = true;
                    $message = 'Sub module successfully updated.';
                }
            } else {
                $message = 'Selected schedule cann\'t be found.';
            }
            return json_encode([
                'valid' => $valid,
                'message' => $message,
                'moduleId' => $requestData['module_id'],
                'scheduleHtml' => $this->createScheduleHtml($subModulesObj),
                'subModuleTable' => view('sub_modules.table', ['tableData' => $subModulesObj])->render()
            ]);
        } else {
            return response()->json(['error' => $validator->errors()->all()]);
        }
    }

    private function validateSubModule($request)
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

        $validator = Validator::make($request->all(), [
            'module_id' => 'required',
            'sub_module_id' => 'required',
            'sub_topic' => 'required',
            'module_url' => 'required|regex:' . $regex,
            'content' => 'required',
        ], [
            'module_id.required' => 'module_id - Module is required.',
            'sub_module_id.required' => 'sub_module_id - Schedule is required.',
            'sub_topic.required' => 'sub_topic - Sub topic is required.',
            'module_url.required' => 'module_url - Course url is required.',
            'module_url.regex' => 'module_url - Invalid URL.',
            'content.required' => 'content - Content is required.',
        ]);

        return $validator;
    }

    function moduleContentView($id)
    {
        $subModuleObj = SubModule::with('moduleContents')->find($id);

        if ($subModuleObj) {
            $subModuleObj = $subModuleObj->toArray();
            $contentArray = $this->uploadContentCount($subModuleObj);
            return view('sub_modules/contents', ['id' => $id, 'counts' => $contentArray, 'tableData' => $subModuleObj]);
        }
        return Redirect::back()->with('error', 'Something went wrong. Please try again later.');
    }

    private function uploadContentCount($subModuleObj)
    {
        $contentArray = [
            'vCount' => 0,
            'dCount' => 0,
            'aCount' => 0
        ];
        foreach ($subModuleObj['module_contents'] as $content) {
            if ($content['content_type'] == 'VIDEO') {
                $contentArray['vCount'] = $contentArray['vCount'] + 1;
            }
            if ($content['content_type'] == 'ASSIGNMENT') {
                $contentArray['aCount'] = +$contentArray['aCount'] + 1;
            }
            if ($content['content_type'] == 'DOCUMENT') {
                $contentArray['dCount'] = +$contentArray['dCount'] + 1;
            }
        }
        return $contentArray;
    }

    function contentsUpload(Request $request)
    {
        $this->validateContents($request);

        $addedContentResp = false;

        if ($request->hasFile('videos')) {
            $videos = $request->file('videos');

            foreach ($videos as $video) {
                $videoName = 'doc_' . time() . '.' . $video->getClientOriginalExtension();
                $path = $video->storeAs('contents/videos', $videoName, 'public');
                $newContentObj = new ModuleContent();
                $newContentObj->content_name = $videoName;
                $newContentObj->content_type = 'VIDEO';
                $newContentObj->sub_module_id = $request->id;
                $addedContentResp = $newContentObj->save();
            }
        }
        if ($request->hasFile('docs')) {
            $docs = $request->file('docs');

            foreach ($docs as $doc) {
                $docName = 'doc_' . time() . '.' . $doc->getClientOriginalExtension();
                $path = $doc->storeAs('contents/docs', $docName, 'public');
                $newContentObj = new ModuleContent();
                $newContentObj->content_name = $docName;
                $newContentObj->content_type = 'DOCUMENT';
                $newContentObj->sub_module_id = $request->id;
                $addedContentResp = $newContentObj->save();
            }
        }
        if ($request->hasFile('assignments')) {
            $assignments = $request->file('assignments');

            foreach ($assignments as $assignment) {
                $assignmentName = 'doc_' . time() . '.' . $assignment->getClientOriginalExtension();
                $path = $assignment->storeAs('contents/assignments', $assignmentName, 'public');
                $newContentObj = new ModuleContent();
                $newContentObj->content_name = $assignmentName;
                $newContentObj->content_type = 'ASSIGNMENT';
                $newContentObj->sub_module_id = $request->id;
                $addedContentResp = $newContentObj->save();
            }
        }

        if ($addedContentResp) {
            return Redirect::back()->with('message', 'Module contents have been uploaded successfully.');
        }
        return Redirect::back()->withInput()->withErrors('Something went wrong. Please try again later.');
    }

    private function validateContents($request)
    {
        $rules = [];
        $customMessages = [];

        if ($request->hasFile('videos')) {
            if (count($request->all()['videos']) > 3) {
                $rules['videos'] = 'max:3';
                $customMessages['videos.max'] = 'Only 3 videos can be upload at once.';
            } else {
                $rules['videos.*'] = 'mimes:mp4,mkv,webm,mov,ogg|max:20480';
                $customMessages['videos.*.mimes'] = 'Video format not support.';
                $customMessages['videos.*.max'] = 'Video size should be less than 20MB.';
            }
        }

        if ($request->hasFile('docs')) {
            if (count($request->all()['docs']) > 3) {
                $rules['docs'] = 'max:3';
                $customMessages['docs.max'] = 'Only 3 documents can be upload at once.';
            } else {
                $rules['docs.*'] = 'mimes:pdf,ppt,pptx,doc,docx,zip|max:2048';
                $customMessages['docs.*.mimes'] = 'Document format not support.';
                $customMessages['docs.*.max'] = 'Document size should be less than 2MB.';
            }
        }

        if ($request->hasFile('assignment')) {
            if (count($request->all()['assignment']) > 3) {
                $rules['assignments'] = 'max:3';
                $customMessages['assignments.max'] = 'Only 3 assignment can upload at once.';
            } else {
                $rules['assignments.*'] = 'mimes:pdf,doc,docx,zip|max:2048';
                $customMessages['assignments.*.mimes'] = 'Assignment format not support.';
                $customMessages['assignments.*.max'] = 'Assignment size should be less than 2MB.';
            }
        }

        $this->validate($request, $rules, $customMessages);
    }

    function moduleContentDelete($id)
    {
        $moduleContentObj = ModuleContent::find($id);
        if ($moduleContentObj) {
            $moduleContentObj->delete();
            return Redirect::back()->with('message', 'Module contents have been deleted successfully.');
        }
        return Redirect::back()->with('error', 'Something went wrong. Please try again later.');
    }

    function downloadContent($id)
    {
        $moduleContentObj = ModuleContent::find($id);
        if ($moduleContentObj->content_type === 'DOCUMENT') {
            $folderName = 'docs';
        } else {
            $folderName = strtolower($moduleContentObj->content_type) . 's/';
        }
        $path = storage_path() . '/app/public/contents/' . $folderName . '/' . $moduleContentObj->content_name;
        if (file_exists($path)) {
            return response()->download($path);
        }
    }

    function downloadMark($id)
    {
        $moduleContentObj = ModuleContent::find($id);
        $path = storage_path() . '/app/public/markSheet/' . $moduleContentObj->answer_sheet;
        if (file_exists($path)) {
            return response()->download($path);
        }
    }

    function downloadAnswer($id)
    {
        $assignmentDetailsObj = AssignmentDetails::find($id);
        $path = storage_path() . '/app/public/studentAnsSheet/' . $assignmentDetailsObj->uploaded_file;
        if (file_exists($path)) {
            return response()->download($path);
        }
    }

    function moduleAssignmentView($id)
    {
        $tableData = AssignmentDetails::where('module_content_id', $id)->with('user', 'moduleContent')->get()->toArray();
        if (count($tableData) > 0) {
            $moduleData = $this->addModuleData($tableData[0]['module_content']['sub_module_id']);
            return view('sub_modules.content_view', ['tableData' => $tableData, 'moduleData' => $moduleData]);
        }
        return Redirect::back()->with('error', 'Nothings to review.');
    }

    function addModuleData($subModId)
    {
        return SubModule::with('module')->find($subModId)->toArray();
    }

    function uploadStdAnsSheet(Request $request)
    {
        $requestData = $request->all();
        $assignmentDataObj = AssignmentDetails::where('module_content_id', $requestData['module_content_id'])->where('user_id', Auth::user()->id)->first();
        if ($assignmentDataObj) {
            if ($request->hasFile('uploaded_file')) {
                $image = $request->file('uploaded_file');
                $imageName = 'studentAnsSheet_' . time() . '.' . $image->getClientOriginalExtension();
                $request->uploaded_file->storeAs('studentAnsSheet', $imageName, 'public');
                $assignmentDataObj->uploaded_file = $imageName;
                $assignmentDataObj->save();
                if ($assignmentDataObj) {
                    return redirect()->route('viewSubModule', $requestData['sub_mod_id'])->with('message', 'Your successfully uploaded the answer sheet.');
                }
            }
        } else {
            $newAssignmentDetail = new AssignmentDetails();

            if ($request->hasFile('uploaded_file')) {
                $image = $request->file('uploaded_file');
                $imageName = 'studentAnsSheet_' . time() . '.' . $image->getClientOriginalExtension();
                $request->uploaded_file->storeAs('studentAnsSheet', $imageName, 'public');
                $newAssignmentDetail->uploaded_file = $imageName;
            }
            $newAssignmentDetail->user_id = Auth::user()->id;
            $newAssignmentDetail->module_content_id = $requestData['module_content_id'];
            $newAssignmentDetail->save();
            if ($newAssignmentDetail) {
                return redirect()->route('viewSubModule', $requestData['sub_mod_id'])->with('message', 'Your successfully uploaded the answer sheet.');
            }
            return redirect()->route('viewSubModule', $requestData['sub_mod_id'])->with('error', 'Something went wrong. Please try again later.');
        }
    }

    function uploadMarkSheet(Request $request)
    {
        $requestData = $request->all();
        $moduleContentData = ModuleContent::find($requestData['mod_content']);
        if ($moduleContentData) {
            if ($request->hasFile('answer_sheet')) {
                $image = $request->file('answer_sheet');
                $imageName = 'markSheet_' . time() . '.' . $image->getClientOriginalExtension();
                $request->answer_sheet->storeAs('markSheet', $imageName, 'public');
                $moduleContentData->answer_sheet = $imageName;
                $moduleContentData->save();
                if ($moduleContentData) {
                    return Redirect::back()->with('message', 'Your successfully uploaded the mark sheet.');
                }
            }
        } else {
            $newModuleContent = new ModuleContent();
            if ($request->hasFile('answer_sheet')) {
                $image = $request->file('answer_sheet');
                $imageName = 'markSheet_' . time() . '.' . $image->getClientOriginalExtension();
                $request->answer_sheet->storeAs('markSheet', $imageName, 'public');
                $newModuleContent->answer_sheet = $imageName;
            }
            $newModuleContent->save();
            if ($newModuleContent) {
                return Redirect::back()->with('message', 'Your successfully uploaded the mark sheet.');
            }
            return Redirect::back()->with('error', 'Something went wrong. Please try again later.');
        }
    }

    function addMarks(Request $request)
    {
        $requestData = $request->all();
        $valid = true;
        $message = 'Marks saved successfully..';

        $assignmentDataObj = AssignmentDetails::find($requestData['id']);
        if ($assignmentDataObj) {
            $assignmentDataObj->mark = $requestData['std_mark'];
            $assignmentDataObj->is_review = true;
            $assignmentDataObj->save();
        } else {
            $valid = false;
            $message = 'Something went wrong. Please try again later.';
        }

        return json_encode([
            'valid' => $valid,
            'updatedMark' => $requestData['std_mark'],
            'message' => $message
        ]);
    }
}
