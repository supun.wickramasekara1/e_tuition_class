<?php

namespace App\Http\Controllers;

use App\Module;
use App\SubscribeDetail;
use App\User;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SubscribedController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(['auth', 'checkStatus']);
    }

    public function notifyUrl(Request $request)
    {
        $newSubscribeObj = new SubscribeDetail();
        $newSubscribeObj->user_id = Auth::user()->id;
        $newSubscribeObj->module_id = Session::get('PAYMENT_MODULE_ID');
        $newSubscribeObj->type = 'MANUAL';
        $newSubscribeObj->status = 'APPROVED';
        $newSubscribeObj->save();

        return redirect()->route('classBoard')->with('message', 'Module has been subscribed successfully.');
    }

    public function cancelUrl()
    {
        return Redirect::back()->with('error', 'Payment cancelled. Please try again later.');
    }

    public function paymentDetails($id)
    {
        $moduleObj = Module::with('user')->get()->find($id);

        if ($moduleObj) {
            $moduleArray = $moduleObj->toArray();
            if ($moduleArray['is_free']) {
                $newSubscribeObj = new SubscribeDetail();

                $newSubscribeObj->user_id = Auth::user()->id;
                $newSubscribeObj->module_id = $id;
                $newSubscribeObj->type = 'MANUAL';
                $newSubscribeObj->status = 'APPROVED';

                $newSubscribeObj->save();
                if ($newSubscribeObj) {
                    return Redirect::back()->with('message', 'Module has been subscribed successfully');
                }
            } else {
                $invId = 'inv_' . time();
                Session::put('PAYMENT_MODULE_ID', $id);
                Session::put('PAYMENT_INVOICE_ID    ', $invId);
                return view('subscribed.index', ['invId' => $invId, 'moduleData' => $moduleArray]);
            }
        }
        return Redirect::back()->with('error', 'Something went wrong. Please try again later.');
    }

    public function addPayment(Request $request)
    {
        $this->validatePayment($request);
        $requestData = $request->all();
        $newSubscribeObj = new SubscribeDetail();

        if ($request->hasFile('bank_slip')) {
            $image = $request->file('bank_slip');
            $imageName = 'bankSlip_' . time() . '.' . $image->getClientOriginalExtension();
            $request->bank_slip->storeAs('bankSlip', $imageName, 'public');
            $newSubscribeObj->bank_slip = $imageName;
        }

        $newSubscribeObj->user_id = Auth::user()->id;
        $newSubscribeObj->module_id = $requestData['module_id'];
        $newSubscribeObj->type = 'MANUAL';
        $newSubscribeObj->bank_name = $requestData['bank_name'];
        $newSubscribeObj->bank_branch = $requestData['bank_branch'];
        $newSubscribeObj->bank_account = $requestData['bank_account'];
        $newSubscribeObj->status = 'PENDING';

        $newSubscribeObj->save();
        if ($newSubscribeObj) {
            return redirect()->route('classBoard')->with('message', 'Module has been subscribed successfully.');
        }
        return Redirect::back()->withInput()->withErrors('Something went wrong. Please try again later.');
    }

    private function validatePayment($request)
    {
        $rules = [
            'bank_name' => 'required',
            'bank_branch' => 'required',
            'bank_account' => 'required|regex:/^[0-9]+$/',
        ];


        if ($request->hasFile('bank_slip') && $request->input('bank_slip_hidden')) {
            $rules['bank_slip'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:1048';
        } else {
            if (!$request->input('bank_slip_hidden')) {
                $rules['bank_slip'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1048';
            }
        }

        $customMessages = [
            'bank_name.required' => 'Bank name is required.',
            'bank_branch.required' => 'Branch is required.',
            'bank_account.required' => 'Bank account is required.',
            'bank_account.regex' => 'Bank account should be only numbers.',
            'bank_slip.image' => 'Bank slip should be an image.',
            'bank_slip.max' => 'Bank slip size should be less than 1MB.',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function approveSubscription()
    {
        $myModuleIds = Auth::user()->modules()->pluck('id')->toArray();
        $subscribedObj = SubscribeDetail::with('module', 'user')->whereIn('module_id', $myModuleIds)->where('status', 'PENDING')->get();
        $subscribedArray = $this->addSubjectObj($subscribedObj->toArray());
        return view('subscribed.approve', ['tableData' => $subscribedArray]);
    }

    private function addSubjectObj($subscribedArray)
    {
        $newSubsArray = [];
        foreach ($subscribedArray as $key => $subscribedItem) {
            $subArray = Module::with('subject')->find($subscribedItem['module_id'])->toArray();
            $newSubsArray[$key] = $subscribedItem;
            $newSubsArray[$key]['subject'] = $subArray['subject'];
        }
        return $newSubsArray;
    }

    public function doApprove($id)
    {
        $myModuleIds = Auth::user()->modules()->pluck('id')->toArray();
        $subscribedObj = SubscribeDetail::whereIn('module_id', $myModuleIds)->where('status', 'PENDING')->find($id);

        if ($subscribedObj) {
            $subscribedObj->status = 'APPROVED';
            $subscribedObj->save();
            return Redirect::back()->with('message', 'Module has been approved successfully.');
        }
        return Redirect::back()->with('error', 'Something went wrong. Please try again later.');
    }

    public function subscribedDataView()
    {
        $myModuleIds = Auth::user()->modules()->pluck('id')->toArray();
        $modulesArray = [];
        $frmDate = Carbon::now()->addDays(-13)->format('Y-m-d');
        $toDate = Carbon::now()->addDays(1)->format('Y-m-d');

        if (Auth::user()->user_type === 'ADMIN') {
            $newArray = [];
            $subscribedModuleIds = SubscribeDetail::pluck('module_id')->toArray();
            $modulesArray = Module::with('user')->whereIn('id', $subscribedModuleIds)->groupBy('user_id')->get('user_id')->toArray();
            $subscribedArray = SubscribeDetail::with('module', 'user')->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            foreach ($subscribedArray as $subscribe) {
                $tutorName = $this->addTutorName($subscribe['module_id']);
                $subscribe['tutor_name'] = $tutorName;
                array_push($newArray, $subscribe);
            }
            $subscribedArray = $newArray;

        } else if (Auth::user()->user_type === 'TUTOR') {
            $subscribedArray = SubscribeDetail::with('module', 'user')->whereIn('module_id', $myModuleIds)->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
        } else {
            $newArray = [];
            $subscribedModuleIds = Auth::user()->subscribeDetails()->pluck('module_id')->toArray();
            $modulesArray = Module::with('user')->whereIn('id', $subscribedModuleIds)->groupBy('user_id')->get('user_id')->toArray();
            $subscribedArray = SubscribeDetail::with('module', 'user')->where('user_id', Auth::user()->id)->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            foreach ($subscribedArray as $subscribe) {
                $tutorName = $this->addTutorName($subscribe['module_id']);
                $subscribe['tutor_name'] = $tutorName;
                array_push($newArray, $subscribe);
            }
            $subscribedArray = $newArray;
        }
        return view('subscribed.details', ['tableData' => $subscribedArray, 'modules' => $modulesArray]);
    }

    public function filterSubscribedData($filterVal, $dateRange)
    {
        $dateRange = explode(" - ", $dateRange);
        $frmDate = $dateRange[0];
        $toDate = Carbon::createFromFormat('Y-m-d', $dateRange[1])->addDays(1)->format('Y-m-d');

        if (Auth::user()->user_type === 'ADMIN') {
            $newArray = [];
            if ($filterVal) {
                $modulesId = Module::where('user_id', $filterVal)->pluck('id')->toArray();
                $subscribedArray = SubscribeDetail::with('module', 'user')->whereIn('module_id', $modulesId)->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            } else {
                $subscribedArray = SubscribeDetail::with('module', 'user')->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            }
            foreach ($subscribedArray as $subscribe) {
                $tutorName = $this->addTutorName($subscribe['module_id']);
                $subscribe['tutor_name'] = $tutorName;
                array_push($newArray, $subscribe);
            }
            $subscribedArray = $newArray;
        } else if (Auth::user()->user_type === 'TUTOR') {
            $modulesId = Auth::user()->modules()->pluck('id')->toArray();
            if ($filterVal) {
                $userIds = User::where('name', 'LIKE', '%' . strtolower($filterVal) . '%')->where('user_type', 'STUDENT')->pluck('id')->toArray();
                $subscribedArray = SubscribeDetail::with('module', 'user')->whereIn('module_id', $modulesId)->whereIn('user_id', $userIds)->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            } else {
                $subscribedArray = SubscribeDetail::with('module', 'user')->whereIn('module_id', $modulesId)->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            }
        } else {
            $newArray = [];
            if ($filterVal) {
                $modulesId = Module::where('user_id', $filterVal)->pluck('id')->toArray();
                $subscribedArray = SubscribeDetail::with('module', 'user')->whereIn('module_id', $modulesId)->where('user_id', Auth::user()->id)->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            } else {
                $subscribedArray = SubscribeDetail::with('module', 'user')->where('user_id', Auth::user()->id)->whereBetween('updated_at', [$frmDate, $toDate])->get()->toArray();
            }
            foreach ($subscribedArray as $subscribe) {
                $tutorName = $this->addTutorName($subscribe['module_id']);
                $subscribe['tutor_name'] = $tutorName;
                array_push($newArray, $subscribe);
            }
            $subscribedArray = $newArray;
        }
        return json_encode([
            'tableHtml' => view('subscribed.table', ['tableData' => $subscribedArray])->render()
        ]);

    }

    private function addTutorName($id)
    {
        $moduleUser = Module::where('id', $id)->with('user')->get('user_id')->toArray();
        return $moduleUser[0]['user']['name'];
    }

    public function payment($id)
    {
//        dd(env('PAYPAL_CLIENT_ID'), env('PAYPAL_CLIENT_SECRET'));
        $moduleArray = Module::find($id)->toArray();
        $newSubscribeObj = new SubscribeDetail();
        $newSubscribeObj->module_id = $id;
        $newSubscribeObj->user_id = Auth::id();
        dd($id, Auth::id(), $moduleArray);
        $newSubscribeObj->save();


        $data = [];
        $data['items'] = [
            [
                'name' => 'eTuitionClass',
                'price' => $moduleArray['module_fee'],
                'desc' => 'Subscribe ' . $moduleArray['topic'] . 'module.',
                'qty' => 1
            ]
        ];

        $data['invoice_id'] = time() . '_inv';
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = route('payment.success');
        $data['cancel_url'] = route('payment.cancel');
        $data['total'] = $moduleArray['module_fee'];

//        dd($data);

        $provider = new ExpressCheckout;

        $response = $provider->setExpressCheckout($data);

        $response = $provider->setExpressCheckout($data, true);

        return redirect($response['paypal_link']);
    }
}
