<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Module;
use App\Subject;
use App\SubscribeDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application user profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function userProfileView()
    {
        $userObj = User::with('subjects')->find(auth()->user()->id);
        $userArray = $userObj->toArray();
        $userArray['selected_subjects'] = $userObj->subjects->pluck('id')->toArray();

        if (Auth::user()->user_type === 'TUTOR') {
            $moduleIdsArray = Auth::user()->modules()->pluck('id')->toArray();
            $moduleObj = Auth::user()->modules()->select('status', 'type')->get();
            $typeObj = $moduleObj->groupBy('type');
            $statusObj = $moduleObj->groupBy('status');
            $approvedCount = SubscribeDetail::whereIn('module_id', $moduleIdsArray)->where('status', 'APPROVED')->count();
            $dashboardCounts = [
                'pubCounts' => isset($statusObj['PUBLISHED']) ? count($statusObj['PUBLISHED']) : 0,
                'onCounts' => isset($typeObj['ONLINE']) ? count($typeObj['ONLINE']) : 0,
                'offCounts' => isset($typeObj['OFFLINE']) ? count($typeObj['OFFLINE']) : 0,
                'subCounts' => $approvedCount,
            ];
        } else {
            $onlineModulesIds = Module::where('type', 'ONLINE')->pluck('id')->toArray();
            $approvedCount = Auth::user()->subscribeDetails()->where('status', 'APPROVED')->get();
            $onlineCounts = $approvedCount->whereIn('module_id', $onlineModulesIds)->toArray();
            $offlineCounts = $approvedCount->whereNotIn('module_id', $onlineModulesIds)->toArray();
            $dashboardCounts = [
                'onCounts' => isset($onlineCounts) ? count($onlineCounts) : 0,
                'offCounts' => isset($offlineCounts) ? count($offlineCounts) : 0,
            ];
        }
        return view('users.index', ['userData' => $userArray, 'grades' => Grade::all()->toArray(), 'dashboardCounts' => $dashboardCounts, 'subjects' => Subject::all()->toArray()]);
    }

    // Update user
    public function update(Request $request)
    {
        $this->validateUser($request);

        $requestData = $request->all();
        $userObj = User::find($requestData['id']);
        if ($userObj) {
            $userObj->image = 'sampleProfile.png';
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = 'profilePic_' . time() . '.' . $image->getClientOriginalExtension();
                $request->image->storeAs('images', $imageName, 'public');
                $userObj->image = $imageName;
            } else {
                $userObj->image = $requestData['image_hidden'];
            }

            if ($request->input('user_type') == 'TUTOR') {
                $userObj->experience = $requestData['experience'];
                $userObj->description = $requestData['description'];
                $userObj->bank_name = $requestData['bank_name'];
                $userObj->bank_branch = $requestData['bank_branch'];
                $userObj->swift_code = $requestData['swift_code'];
                $userObj->account_no = $requestData['account_no'];
            } else {
                $userObj->grade_id = $requestData['grade_id'];
            }
            $userObj->name = $requestData['name'];
            $userObj->location = $requestData['location'];
            $userObj->is_updated = true;
            $resp = $userObj->save();
            if ($resp && isset($requestData['subject'])) {
                $userObj->subjects()->sync($requestData['subject']);
            }
        } else {
            return Redirect::back()->withError('Something went wrong. Please try again later.');
        }
        return Redirect::back()->with('message', 'Your profile has been updated successfully.');
    }

    private function validateUser($request)
    {
        $rules = [
            'name' => 'required|max:255',
            'location' => 'required',
        ];

        if ($request->hasFile('image') && $request->input('image_hidden')) {
            $rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:1024';
        } else {
            if (!$request->input('image_hidden')) {
                $rules['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024';
            }
        }

        if ($request->input('user_type') == 'TUTOR') {
            $rules['experience'] = 'required';
            $rules['description'] = 'required';
            $rules['subject'] = 'required';
            $rules['bank_name'] = 'required';
            $rules['bank_branch'] = 'required';
            $rules['swift_code'] = 'required';
            $rules['account_no'] = 'required|max:16';
        }
        if ($request->input('user_type') == 'STUDENT') {
            $rules['grade_id'] = 'required';
        }

        $customMessages = [
            'name.required' => 'Name is required.',
            'grade_id.required' => 'Grade is required.',
            'experience.required' => 'Experience is required.',
            'description.required' => 'Description is required.',
            'bank_name.required' => 'Bank name is required.',
            'bank_branch.required' => 'Bank branch is required.',
            'swift_code.required' => 'Swift code is required.',
            'account_no.required' => 'Account number is required.',
            'account_no.max' => 'Account number can have maximum 16 digits.',
            'location.required' => 'Location is required.',
            'subject.required' => 'Subject is required.',
            'image.required' => 'Profile picture is required.',
            'image.image' => 'Profile picture should be an image.',
            'image.max' => 'Profile picture size should be less than 1MB.',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    // Change password
    public function changePassword(Request $request)
    {
        $requestData = $request->all();
//        dd($requestData , auth()->user()->id);
        $validator = $this->validatePasswords($requestData);

        if ($validator->fails()) {
            return back()->withErrors($validator->getMessageBag());
        } else {
            $currentPassword = auth()->user()->password;
            if (Hash::check($requestData['current_password'], $currentPassword)) {
                $user = User::find(auth()->user()->id);
                $user->password = Hash::make($requestData['password']);;
                $user->save();
                return redirect()->back()->with('message', 'Your password has been updated successfully.');
            } else {
                return redirect()->back()->withError('Current password is incorrect. Please try again.');
            }
        }
    }

    private function validatePasswords(array $data)
    {
        $messages = [
            'current_password.required' => 'Please enter current password',
            'password.required' => 'Please enter a new password',
            'password.min' => 'The password must be at least 8 characters.',
            'password.confirmed' => 'The password confirmation does not match.',
            'password.regex' => 'Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        ];

        $validator = Validator::make($data, [
            'current_password' => 'required',
            'password' => ['required', 'min:8', 'confirmed', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/'],
        ], $messages);

        return $validator;
    }
}
