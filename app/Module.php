<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'user_id', 'grade_id', 'subject_id', 'topic', 'short_desc', 'module_image', 'module_fee', 'is_free', 'type', 'scheduled_at', 'hours', 'occurrence', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function scheduleDetails()
    {
        return $this->hasMany(ScheduleDetail::class);
    }

    public function subModules()
    {
        return $this->hasMany(SubModule::class);
    }

    public function subscribeDetails()
    {
        return $this->hasMany(SubscribeDetail::class);
    }
}
