<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleComment extends Model
{
    protected $fillable = [
        'subscribe_id', 'rating', 'comment'
    ];

    public function subscribeDetail()
    {
        return $this->belongsTo(SubscribeDetail::class);
    }
}
