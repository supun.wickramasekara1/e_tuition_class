<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleContent extends Model
{
    protected $fillable = [
        'sub_module_id', 'content_type', 'content_name'
    ];

    public function subModule()
    {
        return $this->belongsTo(SubModule::class);
    }

    public function assignmentDetails()
    {
        return $this->hasMany(AssignmentDetails::class);
    }
}
