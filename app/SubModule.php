<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\AssignOp\Mod;

class SubModule extends Model
{
    protected $fillable = [
        'module_id', 'scheduled_at', 'sub_topic', 'video_count', 'doc_count', 'assignment_count', 'content', 'url'
    ];

    public function scheduleDetail()
    {
        return $this->belongsTo(ScheduleDetail::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function moduleContents()
    {
        return $this->hasMany(ModuleContent::class);
    }
}
