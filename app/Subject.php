<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'name'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'subject_user');
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }
}
