<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscribeDetail extends Model
{
    protected $fillable = [
        'module_id', 'user_id', 'type', 'status', 'slip_image', 'is_expired', 'expired_at'
    ];

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function moduleComment()
    {
        return $this->belongsTo(ModuleComment::class);
    }
}
