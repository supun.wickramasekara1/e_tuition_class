<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_name', 'email', 'user_type', 'grade_id', 'image', 'location', 'experience', 'password', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'subject_user');
    }

    public function grades()
    {
        return $this->hasMany(Grade::class);
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function subscribeDetails()
    {
        return $this->hasMany(SubscribeDetail::class);
    }

    public function assignmentDetails()
    {
        return $this->hasMany(AssignmentDetails::class);
    }
}
