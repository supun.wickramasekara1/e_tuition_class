<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/7/2021
 * Time: 9:12 AM
 */

namespace App\Utils;


use App\Providers\RouteServiceProvider;

class UserCheck
{
    public static function fillRequiredData()
    {
        if (!auth()->user()->image) {
            return redirect(RouteServiceProvider::NOT_FILLED_USER);
        }
        return true;
    }
}