<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('grade_id');
            $table->unsignedInteger('subject_id');
            $table->string('topic');
            $table->string('short_desc', 500);
            $table->string('module_fee')->nullable();
            $table->boolean('is_free')->default(false);
            $table->enum('type', ['ONLINE', 'OFFLINE'])->default('ONLINE');
            $table->timestamp('scheduled_at')->useCurrent();
            $table->string('hours')->nullable();
            $table->string('module_image')->nullable();
            $table->integer('occurrence')->default(1);
            $table->enum('status', ['UNPUBLISHED', 'PUBLISHED', 'EXPIRED', 'HOLD'])->default('UNPUBLISHED');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
