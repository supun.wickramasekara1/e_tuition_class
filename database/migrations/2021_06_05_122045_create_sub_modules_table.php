<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_modules', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('module_id');
            $table->timestamp('scheduled_at')->useCurrent();
            $table->string('sub_topic')->nullable();
            $table->enum('status', ['INITIAL', 'ACTIVE', 'CANCELED', 'EXPIRED'])->default('INITIAL');
            $table->string('content', 2000)->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_modules');
    }
}
