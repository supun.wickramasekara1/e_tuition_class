<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscribeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribe_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('module_id');
            $table->unsignedInteger('user_id');
            $table->enum('type', ['PAYPAL', 'MANUAL'])->default('PAYPAL');
            $table->enum('status', ['PENDING', 'APPROVED', 'ERROR', 'CANCELED'])->default('PENDING');
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_slip')->nullable();
            $table->boolean('is_expired')->default(false);
            $table->timestamp('expired_at')->useCurrent();
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribe_details');
    }
}
