<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('subscribe_id');
            $table->integer('rating')->default(0);
            $table->string('comment', 2000)->nullable();
            $table->timestamps();
            $table->foreign('subscribe_id')->references('id')->on('subscribe_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_comments');
    }
}
