<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin Test',
            'user_name' => 'adminTest',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('dialog@123'),
            'user_type' => 'ADMIN',
            'is_updated' => true,
        ]);
    }
}
