-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 03, 2021 at 05:12 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tuition_db`
--
CREATE DATABASE IF NOT EXISTS `tuition_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tuition_db`;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_details`
--

DROP TABLE IF EXISTS `assignment_details`;
CREATE TABLE IF NOT EXISTS `assignment_details` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_content_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `uploaded_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_review` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assignment_details_module_content_id_foreign` (`module_content_id`),
  KEY `assignment_details_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignment_details`
--

INSERT INTO `assignment_details` (`id`, `module_content_id`, `user_id`, `uploaded_file`, `mark`, `is_review`, `created_at`, `updated_at`) VALUES
(1, 3, 3, 'studentAnsSheet_1632474654.doc', NULL, 0, '2021-09-24 03:40:54', '2021-09-24 03:40:54'),
(2, 6, 3, 'studentAnsSheet_1632543607.doc', NULL, 0, '2021-09-24 22:50:07', '2021-09-24 22:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
CREATE TABLE IF NOT EXISTS `grades` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Grade 1', NULL, NULL),
(2, 'Grade 2', NULL, NULL),
(3, 'Grade 3', NULL, NULL),
(4, 'Grade 4', NULL, NULL),
(5, 'Grade 5', NULL, NULL),
(6, 'Grade 6', NULL, NULL),
(7, 'Grade 7', NULL, NULL),
(8, 'Grade 8', NULL, NULL),
(9, 'Grade 9', NULL, NULL),
(10, 'Grade 10', NULL, NULL),
(11, 'Grade 11', NULL, NULL),
(12, 'Grade 12', NULL, NULL),
(13, 'Grade 13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_04_043940_create_subjects_table', 1),
(5, '2021_06_04_044154_create_grades_table', 1),
(6, '2021_06_04_045043_create_subject_user_table', 1),
(7, '2021_06_05_121311_create_modules_table', 1),
(8, '2021_06_05_122045_create_sub_modules_table', 1),
(9, '2021_06_05_122141_create_schedule_details_table', 1),
(10, '2021_06_05_122230_create_module_contents_table', 1),
(11, '2021_06_05_122354_create_subscribe_details_table', 1),
(12, '2021_06_18_103513_create_notifications_table', 1),
(13, '2021_09_08_135332_add_bank_details_to_users_table', 1),
(14, '2021_09_09_030003_add_enum_to_users_table', 1),
(15, '2021_09_09_033414_change_enum_on_subscribe_details_table', 1),
(16, '2021_09_10_112725_create_module_comments_table', 1),
(17, '2021_09_14_023631_create_assignment_details_table', 1),
(18, '2021_09_16_031627_add_answer_sheet_to_module_contents_table', 1),
(19, '2021_09_20_172641_add_is_update_to_users_table', 1),
(20, '2021_09_24_092820_add_surname_to_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `grade_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `topic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_desc` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_free` tinyint(1) NOT NULL DEFAULT 0,
  `type` enum('ONLINE','OFFLINE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ONLINE',
  `scheduled_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occurrence` int(11) NOT NULL DEFAULT 1,
  `status` enum('UNPUBLISHED','PUBLISHED','EXPIRED','HOLD') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UNPUBLISHED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modules_user_id_foreign` (`user_id`),
  KEY `modules_grade_id_foreign` (`grade_id`),
  KEY `modules_subject_id_foreign` (`subject_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `user_id`, `grade_id`, `subject_id`, `topic`, `short_desc`, `module_fee`, `is_free`, `type`, `scheduled_at`, `hours`, `module_image`, `occurrence`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 10, 2, 'වීජ ගණිතය-I', 'රාශි සම්බන්ධතා හා ව්‍යුහ පිළිබද අධ්‍යයනය කරන ගණිත ක්ෂේත්‍ර වීජ ගණිතය නම් වේ. වීජ ගණිතය හදුන්වන ඉංග්‍රීසි පදය “ඇල්ජිබ්රා” නම් වන අතර මොහොමඩ් බින් මුසා අල් ක්වරිස්මි නම් පර්සියානු ජාතිකයා විසින් රචිත කිටාබ් අල්ජබර් වල් මූ යන ග්‍රන්ථයේ නාමයෙන් එම වදන උපුටාගෙන ඇත. අල් - ක්වරිසම් ගණිතඥයකු , තාරකා විද්‍යාඥයකු , නක්ෂත්‍රඥයෙකු සහ භූගෝල විද්‍යාඥයෙකු විය. ඔහුගේ ඉහත ග්‍රන්ථයේ නාමය “සංතුලනය පූරණය හා ගණනය පිළිබද සංක්ෂිප්ත ග්‍රන්ථය” යන අරුත ලබා දේ. ඔහු සිය ග්‍රන්ථය මගින් ඒකජ හා වර්ගජ සමීකරණ ක්‍රමානුකූලව විසදීම සදහා සංකේතමය කර්ම හදුන්වාදෙන ලදී.', '1000.00', 0, 'ONLINE', '2021-09-25 02:30:00', '02:00', 'module_1632473115.png', 4, 'PUBLISHED', '2021-09-24 03:15:15', '2021-09-24 03:33:18'),
(9, 2, 10, 1, 'Sample Topic', 'samplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesamplesample', '1000.00', 0, 'ONLINE', '2021-11-07 03:00:00', '02:00', 'module_1635778124.png', 2, 'PUBLISHED', '2021-11-01 09:18:44', '2021-11-01 09:20:16'),
(3, 4, 11, 2, 'Sample Topic 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nulla, laoreet nec diam ac, ornare mattis dui. Pellentesque in massa nec nulla convallis feugiat ut sit amet neque. Maecenas rutrum quis sapien sit amet efficitur. Nunc in posuere augue, non euismod urna. Pellentesque sed quam orci. Donec pulvinar laoreet malesuada. Quisque eget ante metus. Donec feugiat justo vitae magna ornare, in interdum dui ullamcorper. Fusce ullamcorper tortor urna, sit amet porttitor diam feugiat eget. Aenean quis metus nibh. Aenean bibendum felis ac mauris imperdiet, vestibulum egestas ante consectetur. Sed dignissim consectetur massa sit amet dictum. Morbi vestibulum nunc neque, ac luctus eros consectetur vel. Nunc urna dui, pellentesque et nulla eget, porttitor interdum massa. Donec luctus leo id elit tempor cursus. In vitae justo eu ipsum sodales pharetra in vitae velit.', '1000.00', 0, 'ONLINE', '2021-10-02 04:00:00', '02:00', 'module_1632536956.png', 4, 'UNPUBLISHED', '2021-09-24 20:59:16', '2021-09-24 20:59:16'),
(4, 5, 10, 2, 'Sample topic', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nulla, laoreet nec diam ac, ornare mattis dui. Pellentesque in massa nec nulla convallis feugiat ut sit amet neque. Maecenas rutrum quis sapien sit amet efficitur. Nunc in posuere augue, non euismod urna. Pellentesque sed quam orci. Donec pulvinar laoreet malesuada. Quisque eget ante metus. Donec feugiat justo vitae magna ornare, in interdum dui ullamcorper. Fusce ullamcorper tortor urna, sit amet porttitor diam feugiat eget. Aenean quis metus nibh. Aenean bibendum felis ac mauris imperdiet, vestibulum egestas ante consectetur. Sed dignissim consectetur massa sit amet dictum. Morbi vestibulum nunc neque, ac luctus eros consectetur vel. Nunc urna dui, pellentesque et nulla eget, porttitor interdum massa. Donec luctus leo id elit tempor cursus. In vitae justo eu ipsum sodales pharetra in vitae velit.', '1000.00', 0, 'ONLINE', '2021-09-26 02:30:00', '01:00', 'module_1632542865.png', 4, 'PUBLISHED', '2021-09-24 22:37:45', '2021-09-24 22:43:12'),
(5, 5, 10, 14, 'clash course', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras facilisis magna quis dignissim blandit. Morbi dignissim tortor nec elementum venenatis. Morbi quis est ornare, egestas risus at, rhoncus augue. Nullam convallis quam vitae erat tempus, quis semper augue semper. Nunc consequat metus mi, at commodo ex tinc', NULL, 1, 'ONLINE', '2021-10-02 02:30:00', '01:00', 'module_1632544469.png', 2, 'PUBLISHED', '2021-09-24 23:04:29', '2021-09-24 23:05:28'),
(8, 2, 10, 1, 'Sample module 1', 'sample description 1 sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1sample description 1', NULL, 1, 'ONLINE', '2021-10-30 14:38:00', '02:00', 'module_1635172905.png', 4, 'PUBLISHED', '2021-10-25 09:11:45', '2021-10-25 09:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `module_comments`
--

DROP TABLE IF EXISTS `module_comments`;
CREATE TABLE IF NOT EXISTS `module_comments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subscribe_id` int(10) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL DEFAULT 0,
  `comment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_comments_subscribe_id_foreign` (`subscribe_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_comments`
--

INSERT INTO `module_comments` (`id`, `subscribe_id`, `rating`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 5, NULL, '2021-09-24 03:40:05', '2021-09-24 03:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `module_contents`
--

DROP TABLE IF EXISTS `module_contents`;
CREATE TABLE IF NOT EXISTS `module_contents` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sub_module_id` int(10) UNSIGNED NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_sheet` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_contents_sub_module_id_foreign` (`sub_module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_contents`
--

INSERT INTO `module_contents` (`id`, `sub_module_id`, `content_type`, `content_name`, `answer_sheet`, `created_at`, `updated_at`) VALUES
(1, 1, 'VIDEO', 'doc_1632473744.mp4', NULL, '2021-09-24 03:25:44', '2021-09-24 03:25:44'),
(2, 1, 'DOCUMENT', 'doc_1632473744.doc', NULL, '2021-09-24 03:25:44', '2021-09-24 03:25:44'),
(3, 1, 'ASSIGNMENT', 'doc_1632473744.doc', NULL, '2021-09-24 03:25:44', '2021-09-24 03:25:44'),
(4, 14, 'VIDEO', 'doc_1632543044.mp4', NULL, '2021-09-24 22:40:44', '2021-09-24 22:40:44'),
(5, 14, 'DOCUMENT', 'doc_1632543044.doc', NULL, '2021-09-24 22:40:44', '2021-09-24 22:40:44'),
(6, 14, 'ASSIGNMENT', 'doc_1632543044.doc', NULL, '2021-09-24 22:40:44', '2021-09-24 22:40:44'),
(7, 21, 'VIDEO', 'doc_1635172962.mp4', NULL, '2021-10-25 09:12:42', '2021-10-25 09:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_details`
--

DROP TABLE IF EXISTS `schedule_details`;
CREATE TABLE IF NOT EXISTS `schedule_details` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_id` int(10) UNSIGNED NOT NULL,
  `scheduled_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_details_module_id_foreign` (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Science', NULL, NULL),
(2, 'Maths', NULL, NULL),
(13, 'Business & Accounting Studies', NULL, NULL),
(6, 'Music', NULL, NULL),
(9, 'Sinhala Language and Literature', NULL, NULL),
(10, 'Tamil Language and Literature', NULL, NULL),
(14, 'Information & Communication Technology', NULL, NULL),
(15, 'Business & Accounting Studies', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject_user`
--

DROP TABLE IF EXISTS `subject_user`;
CREATE TABLE IF NOT EXISTS `subject_user` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject_user_user_id_foreign` (`user_id`),
  KEY `subject_user_subject_id_foreign` (`subject_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_user`
--

INSERT INTO `subject_user` (`id`, `user_id`, `subject_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 4, 2, NULL, NULL),
(4, 5, 2, NULL, NULL),
(5, 5, 14, NULL, NULL),
(6, 7, 1, NULL, NULL),
(7, 7, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_details`
--

DROP TABLE IF EXISTS `subscribe_details`;
CREATE TABLE IF NOT EXISTS `subscribe_details` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` enum('PAYHERE','MANUAL') COLLATE utf8mb4_unicode_ci DEFAULT 'PAYHERE',
  `status` enum('PENDING','APPROVED','ERROR','CANCELED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PENDING',
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_branch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_slip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_expired` tinyint(1) NOT NULL DEFAULT 0,
  `expired_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscribe_details_module_id_foreign` (`module_id`),
  KEY `subscribe_details_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribe_details`
--

INSERT INTO `subscribe_details` (`id`, `module_id`, `user_id`, `type`, `status`, `bank_name`, `bank_branch`, `bank_account`, `bank_slip`, `is_expired`, `expired_at`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 'MANUAL', 'APPROVED', 'NDB', 'Nugegoda', '223344', 'bankSlip_1632474520.jpg', 0, '2021-09-24 09:08:40', '2021-09-24 03:38:40', '2021-09-24 03:39:06'),
(2, 4, 3, 'MANUAL', 'APPROVED', 'NDB', 'nugegoda', '112233', 'bankSlip_1632543389.jpg', 0, '2021-09-25 04:16:29', '2021-09-24 22:46:29', '2021-09-24 22:47:50'),
(3, 5, 3, 'MANUAL', 'PENDING', 'no paymen', 'no paymen', '112233', 'bankSlip_1632544589.jpg', 0, '2021-09-25 04:36:29', '2021-09-24 23:06:29', '2021-09-24 23:06:29'),
(4, 8, 3, 'MANUAL', 'APPROVED', NULL, NULL, NULL, NULL, 0, '2021-11-01 15:10:57', '2021-11-01 09:40:57', '2021-11-01 09:40:57'),
(5, 8, 8, 'MANUAL', 'APPROVED', NULL, NULL, NULL, NULL, 0, '2021-11-07 06:30:11', '2021-11-07 01:00:11', '2021-11-07 01:00:11');

-- --------------------------------------------------------

--
-- Table structure for table `sub_modules`
--

DROP TABLE IF EXISTS `sub_modules`;
CREATE TABLE IF NOT EXISTS `sub_modules` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_id` int(10) UNSIGNED NOT NULL,
  `scheduled_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `sub_topic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('INITIAL','ACTIVE','CANCELED','EXPIRED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INITIAL',
  `content` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_modules_module_id_foreign` (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_modules`
--

INSERT INTO `sub_modules` (`id`, `module_id`, `scheduled_at`, `sub_topic`, `status`, `content`, `url`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-09-25 02:30:00', 'ක්ෂේත්‍ර වීජ ගණිතය', 'ACTIVE', 'රාශි සම්බන්ධතා හා ව්‍යුහ පිළිබද අධ්‍යයනය කරන ගණිත ක්ෂේත්‍ර වීජ ගණිතය නම් වේ. වීජ ගණිතය හදුන්වන ඉංග්‍රීසි පදය “ඇල්ජිබ්රා” නම් වන අතර මොහොමඩ් බින් මුසා අල් ක්වරිස්මි නම් පර්සියානු ජාතිකයා විසින් රචිත කිටාබ් අල්ජබර් වල් මූ යන ග්‍රන්ථයේ නාමයෙන් එම වදන උපුටාගෙන ඇත. අල් - ක්වරිසම් ගණිතඥයකු , තාරකා විද්‍යාඥයකු , නක්ෂත්‍රඥයෙකු සහ භූගෝල විද්‍යාඥයෙකු විය. ඔහුගේ ඉහත ග්‍රන්ථයේ නාමය “සංතුලනය පූරණය හා ගණනය පිළිබද සංක්ෂිප්ත ග්‍රන්ථය” යන අරුත ලබා දේ. ඔහු සිය ග්‍රන්ථය මගින් ඒකජ හා වර්ගජ සමීකරණ ක්‍රමානුකූලව විසදීම සදහා සංකේතමය කර්ම හදුන්වාදෙන ලදී.', 'www.url.com', '2021-09-24 03:15:15', '2021-09-24 03:21:02'),
(2, 1, '2021-10-02 02:30:00', 'බහුපද ප්‍රකාශන', 'ACTIVE', 'මූලික වීජ ගණිතය යනු වීජ ගණිතයෙහි සරලම ආකාරයයි. එය අංක ගණිතයේ මූලික මූලධර්ම ඉක්මවූ ගණිත දැනුමක් නොමැති යැයි සිතිය හැකි සිසුන් හට උගන්වනු ලැබේ. අංක ගණිතයේදී ඉලක්කම් හා ඒවායේ ගණිත කර්මයන් (+, −, ×, ÷ වැනි) පමණක් භාවිතා වේ. වීජ ගණිතයේදී ඉලක්කම් (අංක) නිතරම සංකේත (a, x, හෝ y වැනි) වලින් දක්වයි.', 'url.com', '2021-09-24 03:15:15', '2021-09-24 03:31:58'),
(3, 1, '2021-10-09 02:30:00', 'සාධාරණීකරණය', 'ACTIVE', 'මූලික වීජ ගණිතය යනු වීජ ගණිතයෙහි සරලම ආකාරයයි. එය අංක ගණිතයේ මූලික මූලධර්ම ඉක්මවූ ගණිත දැනුමක් නොමැති යැයි සිතිය හැකි සිසුන් හට උගන්වනු ලැබේ. අංක ගණිතයේදී ඉලක්කම් හා ඒවායේ ගණිත කර්මයන් (+, −, ×, ÷ වැනි) පමණක් භාවිතා වේ. වීජ ගණිතයේදී ඉලක්කම් (අංක) නිතරම සංකේත (a, x, හෝ y වැනි) වලින් දක්වයි.', 'url.com', '2021-09-24 03:15:15', '2021-09-24 03:32:34'),
(4, 1, '2021-10-16 02:30:00', 'සාධාරණීකරණය  II', 'ACTIVE', 'එය නොදන්නා සංඛ්‍යා සමුද්දේශ ලෙස යොදා ගැනීමට ඉඩ සලසන අතර සමීකරණ නිර්මාණයට සහ ඒවා විසඳන ආකාරය අධ්‍යයනයට ආධාර වේ. (නිදසුනක් ලෙස 3x + 1 = 10\" ආකාරයේ x නම් සංඛ්‍යාව සෙවීම)', 'url2.com', '2021-09-24 03:15:15', '2021-09-24 03:33:04'),
(5, 2, '2021-10-09 04:30:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 03:19:19', '2021-09-24 03:19:19'),
(6, 2, '2021-10-16 04:30:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 03:19:19', '2021-09-24 03:19:19'),
(7, 2, '2021-10-23 04:30:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 03:19:19', '2021-09-24 03:19:19'),
(8, 2, '2021-10-30 04:30:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 03:19:19', '2021-09-24 03:19:19'),
(9, 3, '2021-10-02 04:00:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 20:59:16', '2021-09-24 20:59:16'),
(10, 3, '2021-10-09 04:00:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 20:59:16', '2021-09-24 20:59:16'),
(11, 3, '2021-10-16 04:00:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 20:59:16', '2021-09-24 20:59:16'),
(12, 3, '2021-10-23 04:00:00', NULL, 'INITIAL', NULL, NULL, '2021-09-24 20:59:16', '2021-09-24 20:59:16'),
(13, 4, '2021-09-26 02:30:00', 'sub topic 2', 'ACTIVE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nulla, laoreet nec diam ac, ornare mattis dui. Pellentesque in massa nec nulla convallis feugiat ut sit amet neque. Maecenas rutrum quis sapien sit amet efficitur. Nunc in posuere augue, non euismod urna. Pellentesque sed quam orci. Donec pulvinar laoreet malesuada. Quisque eget ante metus. Donec feugiat justo vitae magna ornare, in interdum dui ullamcorper. Fusce ullamcorper tortor urna, sit amet porttitor diam feugiat eget. Aenean quis metus nibh. Aenean bibendum felis ac mauris imperdiet, vestibulum egestas ante consectetur. Sed dignissim consectetur massa sit amet dictum. Morbi vestibulum nunc neque, ac luctus eros consectetur vel. Nunc urna dui, pellentesque et nulla eget, porttitor interdum massa. Donec luctus leo id elit tempor cursus. In vitae justo eu ipsum sodales pharetra in vitae velit.', 'url.com', '2021-09-24 22:37:45', '2021-09-24 22:42:18'),
(14, 4, '2021-10-03 02:30:00', 'sub topic', 'ACTIVE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nulla, laoreet nec diam ac, ornare mattis dui. Pellentesque in massa nec nulla convallis feugiat ut sit amet neque. Maecenas rutrum quis sapien sit amet efficitur. Nunc in posuere augue, non euismod urna. Pellentesque sed quam orci. Donec pulvinar laoreet malesuada. Quisque eget ante metus. Donec feugiat justo vitae magna ornare, in interdum dui ullamcorper. Fusce ullamcorper tortor urna, sit amet porttitor diam feugiat eget. Aenean quis metus nibh. Aenean bibendum felis ac mauris imperdiet, vestibulum egestas ante consectetur. Sed dignissim consectetur massa sit amet dictum. Morbi vestibulum nunc neque, ac luctus eros consectetur vel. Nunc urna dui, pellentesque et nulla eget, porttitor interdum massa. Donec luctus leo id elit tempor cursus. In vitae justo eu ipsum sodales pharetra in vitae velit.', 'url.com', '2021-09-24 22:37:45', '2021-09-24 22:39:53'),
(15, 4, '2021-10-10 02:30:00', 'sub topic 3', 'ACTIVE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nulla, laoreet nec diam ac, ornare mattis dui. Pellentesque in massa nec nulla convallis feugiat ut sit amet neque. Maecenas rutrum quis sapien sit amet efficitur. Nunc in posuere augue, non euismod urna. Pellentesque sed quam orci. Donec pulvinar laoreet malesuada. Quisque eget ante metus. Donec feugiat justo vitae magna ornare, in interdum dui ullamcorper. Fusce ullamcorper tortor urna, sit amet porttitor diam feugiat eget. Aenean quis metus nibh. Aenean bibendum felis ac mauris imperdiet, vestibulum egestas ante consectetur. Sed dignissim consectetur massa sit amet dictum. Morbi vestibulum nunc neque, ac luctus eros consectetur vel. Nunc urna dui, pellentesque et nulla eget, porttitor interdum massa. Donec luctus leo id elit tempor cursus. In vitae justo eu ipsum sodales pharetra in vitae velit.', 'url.com', '2021-09-24 22:37:45', '2021-09-24 22:42:39'),
(16, 4, '2021-10-17 02:30:00', 'sub topic 4', 'ACTIVE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nulla, laoreet nec diam ac, ornare mattis dui. Pellentesque in massa nec nulla convallis feugiat ut sit amet neque. Maecenas rutrum quis sapien sit amet efficitur. Nunc in posuere augue, non euismod urna. Pellentesque sed quam orci. Donec pulvinar laoreet malesuada. Quisque eget ante metus. Donec feugiat justo vitae magna ornare, in interdum dui ullamcorper. Fusce ullamcorper tortor urna, sit amet porttitor diam feugiat eget. Aenean quis metus nibh. Aenean bibendum felis ac mauris imperdiet, vestibulum egestas ante consectetur. Sed dignissim consectetur massa sit amet dictum. Morbi vestibulum nunc neque, ac luctus eros consectetur vel. Nunc urna dui, pellentesque et nulla eget, porttitor interdum massa. Donec luctus leo id elit tempor cursus. In vitae justo eu ipsum sodales pharetra in vitae velit.', 'url.com', '2021-09-24 22:37:45', '2021-09-24 22:42:55'),
(17, 5, '2021-10-02 02:30:00', 'clash1', 'ACTIVE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras facilisis magna quis dignissim blandit. Morbi dignissim tortor nec elementum venenatis. Morbi quis est ornare, egestas risus at, rhoncus augue. Nullam convallis quam vitae erat tempus, quis semper augue semper. Nunc consequat metus mi, at commodo ex tinc', 'url.com', '2021-09-24 23:04:29', '2021-09-24 23:04:58'),
(18, 5, '2021-10-09 02:30:00', 'clash 2', 'ACTIVE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras facilisis magna quis dignissim blandit. Morbi dignissim tortor nec elementum venenatis. Morbi quis est ornare, egestas risus at, rhoncus augue. Nullam convallis quam vitae erat tempus, quis semper augue semper. Nunc consequat metus mi, at commodo ex tinc', 'url.com', '2021-09-24 23:04:29', '2021-09-24 23:05:12'),
(19, 6, '2021-09-25 04:50:16', 'Abc', 'ACTIVE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras facilisis magna quis dignissim blandit. Morbi dignissim tortor nec elementum venenatis. Morbi quis est ornare, egestas risus at, rhoncus augue. Nullam convallis quam vitae erat tempus, quis semper augue semper. Nunc consequat metus mi, at commodo ex tinc', 'url.com', '2021-09-24 23:20:16', '2021-09-24 23:21:19'),
(20, 7, '2021-09-25 05:18:36', NULL, 'INITIAL', NULL, NULL, '2021-09-24 23:48:36', '2021-09-24 23:48:36'),
(21, 8, '2021-10-30 14:38:00', 'Sample sub 1', 'ACTIVE', 'sub.comsub.comsub.comsub.comsub.comsub.comsub.com', 'sub.com', '2021-10-25 09:11:45', '2021-10-25 09:14:04'),
(22, 8, '2021-11-06 14:38:00', 'sub 2', 'ACTIVE', 'sub 2sub 2sub 2sub 2sub 2sub 2sub 2vsub 2sub 2', 'sub2.com', '2021-10-25 09:11:45', '2021-10-25 09:14:31'),
(23, 8, '2021-11-13 14:38:00', 'sub 3', 'ACTIVE', 'sub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.com', 'sub3.com', '2021-10-25 09:11:45', '2021-10-25 09:14:51'),
(24, 8, '2021-11-20 14:38:00', 'sub 4', 'ACTIVE', 'sub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.comsub3.com', 'sub4.com', '2021-10-25 09:11:45', '2021-10-25 09:15:08'),
(25, 9, '2021-11-07 03:00:00', 'sub module 1', 'ACTIVE', 'sub module 1', 'sub1.com', '2021-11-01 09:18:44', '2021-11-01 09:19:49'),
(26, 9, '2021-11-14 03:00:00', 'sub module 2', 'ACTIVE', 'sub module 2sub module 2sub module 2sub module 2sub module 2', 'sub1.com', '2021-11-01 09:18:44', '2021-11-01 09:20:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` enum('STUDENT','TUTOR','ADMIN') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience` int(11) DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_updated` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_branch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `swift_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_grade_id_foreign` (`grade_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `user_name`, `email`, `email_verified_at`, `password`, `user_type`, `grade_id`, `image`, `experience`, `location`, `description`, `is_updated`, `remember_token`, `created_at`, `updated_at`, `bank_name`, `bank_branch`, `swift_code`, `account_no`) VALUES
(1, 'Admin Test', NULL, 'adminTest', 'admin@gmail.com', NULL, '$2y$10$3irAlZTb.oUaXgAKFwGAUuajXP1Afxp06K7QhcLjq/wWZE/4pu5Z2', 'ADMIN', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2021-09-23 12:49:55', '2021-09-23 12:49:55', NULL, NULL, NULL, NULL),
(2, 'Tutor', 'aa', 'Tutor1', 'tutor1@gmail.com', '2021-09-23 13:20:12', '$2y$10$Vio2h4E05zduIkRax0NH9ue2DpX1lI4z/MEKtJnTWco5BOJnqNmKS', 'TUTOR', NULL, 'profilePic_1632464140.jpg', 7, 'Nugegoda', 'HI there my name is tutor one', 1, NULL, '2021-09-23 13:15:09', '2021-09-24 07:10:32', 'NDB', 'Nungegoda', '1234', '2435'),
(3, 'Student 1', NULL, 'student1', 'student1@gmail.com', '2021-09-23 13:22:40', '$2y$10$nSvHpwUpqVDyhN1i8r3L7eIk/BJvnstxx0.Tv7nR6AXDlUO0y9hpK', 'STUDENT', 10, 'profilePic_1632473943.jpg', NULL, 'Nugegoda', NULL, 1, NULL, '2021-09-23 13:22:21', '2021-09-24 03:29:03', NULL, NULL, NULL, NULL),
(4, 'tutor2', NULL, 'tutor2', 'tutor2@gmail.com', '2021-09-24 10:01:29', '$2y$10$6jk9X6cmJiUp4mTjAvV8guxq4ZtXhlwzMjW2lT6gQr6vbfdn0Facm', 'TUTOR', NULL, 'profilePic_1632533700.jpg', 5, 'nugegoda', 'orem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae risus tortor. Donec', 1, NULL, '2021-09-24 10:01:03', '2021-09-24 20:05:00', 'BOC', 'Nugegoda', '12345', '2233445'),
(5, 'tutor3', NULL, 'tutor3', 'tutor3@gmail.com', '2021-09-24 22:32:05', '$2y$10$rGRQbtSe1bBQRu4HSyPMh.mAgBPVrAm4S/jADjgOqdmNM.GFiBZtO', 'TUTOR', NULL, 'profilePic_1632542639.jpg', 4, 'Nawala', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras facilisis magna quis dignissim blandit. Morbi dignissim tortor nec elementum venenatis. Morbi quis est ornare', 1, NULL, '2021-09-24 22:31:39', '2021-09-24 22:33:59', 'NDB Bank', 'Nugegoda', '123', '12345'),
(6, 'sample1', NULL, 'Sample1', 'sample1@gmail.com', '2021-11-06 18:30:00', '$2y$10$Vio2h4E05zduIkRax0NH9ue2DpX1lI4z/MEKtJnTWco5BOJnqNmKS', 'TUTOR', NULL, NULL, NULL, NULL, NULL, 0, NULL, '2021-11-06 22:49:38', '2021-11-06 22:49:38', NULL, NULL, NULL, NULL),
(7, 'tutor4', NULL, 'tutor4', 'tutor4@gmail.com', '2021-11-01 18:30:00', '$2y$10$OByRe9eX7wEb3JmGK/kV1.WgaGyP0LhcKJAkoNsb.o1K1hBX84WC2', 'TUTOR', NULL, 'profilePic_1636263365.jpg', 7, 'Nugegoda', 'samplesamplesamplesamplevsamplesamplesamplesamplesamplesamplesamplesamplevsamplesamplesamplesamplesample', 1, NULL, '2021-11-07 00:00:13', '2021-11-07 00:06:05', 'NTB', 'nugegoda', '123', '3456'),
(8, 'student2', NULL, 'student2', 'student2@gmail.com', '2021-11-06 18:30:00', '$2y$10$UX1HTrJHg3Ri5GCNxGGixOUgAEGQHmj746F0uU0ksEN3dwM8TANO6', 'STUDENT', 10, 'profilePic_1636266595.jpg', NULL, 'Nugegoda', NULL, 1, NULL, '2021-11-07 00:58:47', '2021-11-07 00:59:55', NULL, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
