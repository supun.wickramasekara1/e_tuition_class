var ratedAmount = 1;
mainClassBoard = {
    filterModule: function () {
        $('#filterModule').unbind();
        $('#filterModule').click(function (e) {
            $('#loader').addClass('loading');
            e.preventDefault();
            $.ajax({
                url: '/class_board/filter',
                type: 'POST',
                dataType: 'json',
                data: $('#formFilterModule').serialize(),
                success: function (data) {
                    if (data.valid) {
                        $('#modulesContainer').html(data.modulesHtml);
                    }
                    $('#loader').removeClass('loading');
                }
            });
        });
    },
    addReview: function () {
        $('.add-review').unbind();
        $('.add-review').click(function (e) {
            $('#addReviewModal').modal('show');
            $('#moduleId').val($(this).data('id'));
            mainClassBoard.ratingCount();
            mainClassBoard.changeRatingUI(1);
            mainClassBoard.submitRating();
            $('#comment').val('');
        });
    },
    ratingCount: function () {
        $('.rating-ul.edit-rating li').unbind();
        $('.rating-ul.edit-rating li').click(function (e) {
            ratedAmount = $(this).data('id');
            mainClassBoard.changeRatingUI($(this).data('id'));
            for (i = 0; i <= $(this).data('id'); i++) {
                $('.star' + i).addClass('on');
            }
        });
    },
    submitRating: function () {
        $('#submitRating').unbind();
        $('#submitRating').click(function (e) {
            $('#loader').addClass('loading');
            e.preventDefault();
            $.ajax({
                url: '/class_board/add_review',
                type: 'POST',
                dataType: 'json',
                data: $('#ratingForm').serialize(),
                success: function (data) {
                    let alertType = 'success';
                    if (!data.valid) {
                        alertType = 'danger';
                        common.alertAutoClose(data.message, alertType);
                    }
                    common.alertAutoClose(data.message, alertType);
                    $('#addReviewModal').modal('hide');
                    $('#loader').removeClass('loading');
                }
            });
        });
    },
    changeRatingUI: function (count) {
        $('.rating-li').removeClass('on');
        $('.star1').addClass('on');
        $('#ratedAmount').text(count);
        $('#starVal').val(count);
    },
    init: function () {
        mainClassBoard.filterModule();
        mainClassBoard.addReview();
        mainClassBoard.ratingCount();
        $('#filterSubject').selectpicker();
    }
};