var common = {
    alertAutoClose: function (message, alertType) {
        const alertHtml = '<div class="alert alert-' + alertType + '"  role="alert">' +
            '<button type="button" class="close" data-dismiss="alert">' +
            '<span aria-hidden="true">&times;</span></button>' + message +
            '</div>';
        $(".manual-msg").html(alertHtml);

        $(".manual-msg").fadeTo(2000, 500).slideUp(200, function () {
            $(".manual-msg").slideUp(5000);
        });
        common.scrollToTop();
    },
    scrollToTop: function () {
        $("html, body").animate({
            scrollTop: 0
        }, 900);
    },
    init: function () {
        bsCustomFileInput.init();
        $(".alert.alert-dismissible").fadeTo(2000, 500).slideUp(200, function () {
            $(".alert.alert-dismissible").slideUp(5000);
        });
        $('#userSubjects').select2({
            placeholder: "Please select a subject(s)"
        });
        // $('.subject-picker').selectpicker();
    }
};