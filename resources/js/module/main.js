var mainModule = {
    changeModuleType: function () {
        $('.module-type').on('change', function () {
            if ($(this).val() == 'OFFLINE') {
                $('.hideable-container').addClass('offline');
            } else {
                $('.hideable-container').removeClass('offline');
            }
        });
    },
    changeIsFree: function () {
        $('#isFree').on('change', function () {
            if ($(this).prop('checked')) {
                $('#moduleFee').prop('disabled', true);
            } else {
                $('#moduleFee').prop('disabled', false);
            }
        });
    },
    editModule: function () {
        // $('#moduleTable tbody').on('click', '.edit-module', function () {
        $('#moduleTable').off('click','.edit-module').on('click', '.edit-module', function () {
            $('#loader').addClass('loading');
            const moduleId = $(this).data('id');
            $.ajax({
                url: '/module/edit/' + moduleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        $('#moduleForm').html(data.moduleFormHtml);
                        common.scrollToTop();
                    } else {
                        alertType = 'danger';
                        common.alertAutoClose(data.message, alertType);
                    }
                    $('#loader').removeClass('loading');
                    mainModule.changeModuleType();
                }
            });
        })
    },
    publishModule: function () {
        $('#moduleTable').off('click','.publish-module').on('click', '.publish-module', function () {
            $('#loader').addClass('loading');
            const moduleId = $(this).data('id');
            $.ajax({
                url: '/module/publish/' + moduleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        $('#moduleTableContainer').html(data.moduleTableHtml);
                        $('#moduleTable').DataTable({
                            "responsive": true,
                            "order": [[0, "asc"]]
                        }).draw();
                    } else {
                        alertType = 'danger';
                    }
                    common.alertAutoClose(data.message, alertType);
                    mainModule.publishModule();
                    mainModule.unpublishModule();
                    mainModule.holdModule();
                    mainModule.editModule();
                    $('#loader').removeClass('loading');
                }
            });
        })
    },
    unpublishModule: function () {
        $('#moduleTable').off('click','.unpublish-module').on('click', '.unpublish-module', function () {
            $('#loader').addClass('loading');
            const moduleId = $(this).data('id');
            $.ajax({
                url: '/module/unpublish/' + moduleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        $('#moduleTableContainer').html(data.moduleTableHtml);
                        $('#moduleTable').DataTable({
                            "responsive": true,
                            "order": [[0, "asc"]]
                        }).draw();
                    } else {
                        alertType = 'danger';
                    }
                    common.alertAutoClose(data.message, alertType);
                    mainModule.publishModule();
                    mainModule.unpublishModule();
                    mainModule.holdModule();
                    mainModule.editModule();
                    $('#loader').removeClass('loading');
                }
            });
        })
    },
    holdModule: function () {
        $('#moduleTable').off('click','.hold-module').on('click', '.hold-module', function () {
            $('#loader').addClass('loading');
            const moduleId = $(this).data('id');
            $.ajax({
                url: '/module/hold/' + moduleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        $('#moduleTableContainer').html(data.moduleTableHtml);
                        $('#moduleTable').DataTable({
                            "responsive": true,
                            "order": [[0, "asc"]]
                        }).draw();
                    } else {
                        alertType = 'danger';
                    }
                    common.alertAutoClose(data.message, alertType);
                    mainModule.publishModule();
                    mainModule.unpublishModule();
                    mainModule.holdModule();
                    mainModule.editModule();
                    $('#loader').removeClass('loading');
                }
            });
        })
    },
    deleteModule: function (moduleTable) {
        // $('#moduleTable tbody').on('click', '.delete-module', function () {
        $('#moduleTable').off('click','.delete-module').on('click', '.delete-module', function () {
            $('#loader').addClass('loading');
            const parentTr = $(this).parents('tr');
            const moduleId = $(this).data('id');
            $.ajax({
                url: '/module/delete/' + moduleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        moduleTable.row(parentTr).remove().draw(false);
                    } else {
                        alertType = 'danger';
                    }
                    common.alertAutoClose(data.message, alertType);
                    mainModule.publishModule();
                    mainModule.unpublishModule();
                    mainModule.editModule();
                    $('#loader').removeClass('loading');
                }
            });
        })
    },
    init: function () {
        $('#moduleStartDate').datetimepicker({
            icons: {time: 'far fa-clock'},
            format: 'YYYY-MM-DD HH:mm',
            minDate: moment()
        });
        $('#moduleGrade').select2({
            placeholder: "Select a grade",
            allowClear: true
        });
        $('#moduleSubject').select2({
            placeholder: "Select a subject",
            allowClear: true
        });
        const moduleTable = $('#moduleTable').DataTable({
            "responsive": true,
            "order": [[0, "desc"]]
        });

        mainModule.changeModuleType();
        mainModule.editModule();
        mainModule.publishModule();
        mainModule.unpublishModule();
        mainModule.holdModule();
        mainModule.changeIsFree();
        mainModule.deleteModule(moduleTable);
    }
};