mainSchedule = {
    initCalendar: function () {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev, next today',
                center: 'title',
                right: 'month, agendaWeek, agendaDay, listMonth'
            },
            buttonText: {
                today: 'Today',
                month: 'Month',
                week: 'Week',
                day: 'Day',
                listMonth: 'Month List'
            },
            displayEventTime: true,
            timeFormat: 'HH:mm',
            events: '/schedule_details',
            eventRender: function (event, element) {
                // var content = '<div class="bg-red"> {event.description + " - " + event.title}</div>';
                // var content = '<div class="bg-red">'+ event.description +'</div>';
                $(element).tooltip({title: event.description});
            },
            eventClick: function (calEvent, jsEvent, view) {
                console.log(calEvent);
                console.log(jsEvent);
                console.log(view);
            }
        });

    },
    init: function () {
        // $('#scheduleTable').DataTable({
        //     "paging": true,
        //     "searching": true,
        //     "ordering": true,
        //     "info": true,
        //     "responsive": true,
        //     // "order": [[0, "asc"]]
        // });
        mainSchedule.initCalendar();
    }
};