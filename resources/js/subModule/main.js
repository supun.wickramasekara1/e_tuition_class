mainSubModule = {
    changeModule: function () {
        $('#module_id').change(function () {
            $('#loader').addClass('loading');
            const moduleId = $(this).val();
            $('#subModuleFormPart').hide();
            $.ajax({
                url: '/sub_module/change/module/' + moduleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        $('#subModuleTableContainer').html(data.subModuleTable);
                        $('#subModuleTable').DataTable({
                            responsive: true,
                            "order": [[0, "asc"]]
                        }).draw();

                        $('#sub_module_id').html(data.scheduleHtml);
                        $('#sub_module_id').selectpicker('refresh');
                    } else {
                        alertType = 'danger';
                        mainModule.alertAutoClose(data.message, alertType);
                    }
                    mainSubModule.editSubModule();
                    mainSubModule.cancelSubModule();
                    $('#loader').removeClass('loading');
                }
            });
        });
    },
    addSubModule: function () {
        $('#btnSubModule').unbind();
        $('#btnSubModule').click(function (e) {
            $('#loader').addClass('loading');
            mainSubModule.clearAllErrors();
            e.preventDefault();
            $.ajax({
                url: '/sub_module/add_module',
                type: 'POST',
                dataType: 'json',
                data: $('#formSubModule').serialize(),
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        var alertType = 'success';
                        if (data.valid) {
                            $('#subModuleFormPart').hide();
                            $('#subModuleTableContainer').html(data.subModuleTable);
                            $('#subModuleTable').DataTable({
                                responsive: true,
                                "order": [[0, "asc"]]
                            }).draw();

                            $('#sub_module_id').html(data.scheduleHtml);
                            $('#sub_module_id').selectpicker('refresh');
                            $('#module_id').selectpicker('val', '').selectpicker('refresh');
                            mainSubModule.clearFormData();
                        } else {
                            alertType = 'danger'
                        }
                        common.alertAutoClose(data.message, alertType);
                    } else {
                        $.each(data.error, function (key, value) {
                            var errorArray = value.split(' - ');
                            var id = errorArray[0];
                            var msg = errorArray[1];
                            mainSubModule.showFormErrors(id, msg);
                        });
                    }
                    mainSubModule.editSubModule();
                    mainSubModule.cancelSubModule();
                    $('#loader').removeClass('loading');
                }
            });
        });
    },
    clearFormData: function () {
        $('.form-control').val('');
        $('#btnSubModule').text('Create');
    },
    editSubModule: function () {
        // $('#subModuleTable tbody').on('click', '.edit-sub-module', function () {
        $('#subModuleTable').off('click', '.edit-sub-module').on('click', '.edit-sub-module', function () {
            $('#loader').addClass('loading');
            const subModuleId = $(this).data('id');
            const subModuleAction = $(this).data('action');
            const moduleId = $('#module_id').val();
            console.log(moduleId, subModuleId, subModuleAction);
            $.ajax({
                url: '/sub_module/edit/' + subModuleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        $('#subModuleFormPart').show();
                        $('#subModuleFormPart').html(data.subModuleFormPartHtml);
                        $('#sub_module_id').selectpicker('refresh');
                        $('#btnSubModule').text('Update');
                        $('#module_id').selectpicker('val', data.moduleId).selectpicker('refresh');
                        common.scrollToTop();
                    } else {
                        alertType = 'danger';
                        common.alertAutoClose(data.message, alertType);
                    }
                    mainSubModule.cancelSubModule();
                    $('#loader').removeClass('loading');
                }
            });
        })
    },
    cancelSubModule: function () {
        // $('#subModuleTable tbody').on('click', '.cancel-sub-module', function () {
        $('#subModuleTable').off('click', '.cancel-sub-module').on('click', '.cancel-sub-module', function () {
            $('#loader').addClass('loading');
            const subModuleId = $(this).data('id');
            const subModuleAction = $(this).data('action');
            const moduleId = $('#module_id').val();
            console.log(moduleId, subModuleId, subModuleAction);
            $.ajax({
                url: '/sub_module/cancel/' + subModuleId,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    let alertType = 'success';
                    if (data.valid) {
                        $('#subModuleTableContainer').html(data.subModuleTable);
                        $('#subModuleTable').DataTable({
                            responsive: true,
                            "order": [[0, "asc"]]
                        }).draw();
                    } else {
                        alertType = 'danger';
                    }
                    common.alertAutoClose(data.message, alertType);
                    mainSubModule.editSubModule();
                    mainSubModule.cancelSubModule();
                    $('#loader').removeClass('loading');
                }
            });
        })
    },
    showFormErrors: function (id, msg) {
        $('#' + id).addClass('is-invalid');
        $('#' + id).parent().addClass('is-invalid');
        $('#' + id).parent().next().text(msg);
        $('#' + id).parent().find('.invalid-feedback').text(msg);
    },
    clearAllErrors: function () {
        if ($('.form-group').children().hasClass('is-invalid')) {
            $('.form-group').children().removeClass('is-invalid');
            $('.input-group').removeClass('is-invalid');
            $('.form-control').removeClass('is-invalid');
            $('.invalid-feedback').text('');
        }
        else {
            $('.input-group').removeClass('is-invalid');
            $('.form-control').removeClass('is-invalid');
            $('.invalid-feedback').text('');
        }
    },
    uploadAnsSheet: function () {
        $('.upload-answers').unbind();
        $('.upload-answers').click(function (e) {
            $('#asdAnsModal').modal('show');
            $('#subModId').val($(this).data('smid'));
            $('#moduleContentId').val($(this).data('id'));
        });
    },
    uploadMarkSheet: function () {
        $('.upload-mark-sheet').unbind();
        $('.upload-mark-sheet').click(function (e) {
            $('#markSheetModal').modal('show');
            $('#modContent').val($(this).data('id'));
        });
    },
    submitMarks: function () {
        $('.submit-mark').unbind();
        $('.submit-mark').click(function (e) {
            e.preventDefault();
            const attr = $(this);
            $.ajax({
                url: '/sub_module/content/add_mark',
                type: 'POST',
                dataType: 'json',
                data: $(this).parents('form:first').serialize(),
                success: function (data) {
                    console.log(data);
                    let alertType = 'success';
                    if (!data.valid) {
                        alertType = 'danger';
                        common.alertAutoClose(data.message, alertType);
                    }
                    common.alertAutoClose(data.message, alertType);
                    $('#loader').removeClass('loading');
                }
            });
        });
    },
    init: function () {
        $('#module_id').selectpicker();
        $('#sub_module_id').selectpicker();
        $('#subModuleTable').DataTable({
            responsive: true,
            "order": [[0, "asc"]]
        });
        $('#contentTable').DataTable({
            "paging": false,
            "searching": true,
            "info": false,
            responsive: true,
            "order": [[2, "asc"]]
        });
        mainSubModule.changeModule();
        mainSubModule.addSubModule();
        mainSubModule.editSubModule();
        mainSubModule.cancelSubModule();
        mainSubModule.uploadAnsSheet();
        mainSubModule.uploadMarkSheet();
        mainSubModule.submitMarks();
    }
};