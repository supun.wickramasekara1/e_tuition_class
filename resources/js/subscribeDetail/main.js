mainSubcribeData = {
    filterDetails: function () {
        $('#btnFilter').click(function () {
            $('#loader').addClass('loading');
            let filterVal = $('#filterVal').val();
            const dateRange = $('#dateRange').val();
            filterVal = ($.trim(filterVal)) ? $.trim(filterVal) : 0;
            $.ajax({
                url: '/subscribe/filter/' + filterVal + '/' + dateRange,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    $('#subscribedTableContainer').html(data.tableHtml);
                    $('#subscribedTable').DataTable({
                        responsive: true,
                        searching: false,
                        order: [[0, "asc"]],
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend: 'excelHtml5',
                                className: 'btn-primary',
                                text: 'Download Excel',
                                title: mainSubcribeData.getExportFileTitle(),
                                filename: function () {
                                    var d = new Date();
                                    var n = d.getTime();
                                    return 'Subscribed Details ' + n;
                                },
                                footer: true
                            },
                            {
                                extend: 'pdfHtml5',
                                className: 'btn-primary',
                                text: 'Download PDF',
                                title: mainSubcribeData.getExportFileTitle(),
                                filename: function () {
                                    var d = new Date();
                                    var n = d.getTime();
                                    return 'Subscribed Details ' + n;
                                },
                                footer: true
                            }
                        ]
                    }).draw();
                    $('#loader').removeClass('loading');
                }
            });
        });
    },
    getExportFileTitle: function () {
        let dateRange = $('#dateRange').val();
        console.log('dateRange----------------------');
        console.log(dateRange);
        const dateArr = dateRange.split(" - ");
        return 'Subscribed Report between ' + dateArr[0] + ' and ' + dateArr[1];
    },
    changePaymentType: function () {
        $('.payment-type').change(function () {
            if ($(this).val() === 'PAYPAL') {
                $('#paypalContainer').show();
                $('.manual-container').hide();
            } else {
                $('#paypalContainer').hide();
                $('.manual-container').show();
            }
        });
    },
    init: function () {
        mainSubcribeData.filterDetails();
        mainSubcribeData.changePaymentType();
        $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                autoApply: true,
                buttonClasses: "btn-info",
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(13, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'))
            }
        );
        $('#approveSubsTable').DataTable({
            responsive: true
        });
        $('#subscribedTable').DataTable({
            responsive: true,
            searching: false,
            order: [[0, "asc"]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    className: 'btn-primary',
                    text: 'Download Excel',
                    title: mainSubcribeData.getExportFileTitle(),
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Subscribed Details ' + n;
                    },
                    footer: true
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn-primary',
                    text: 'Download PDF',
                    title: mainSubcribeData.getExportFileTitle(),
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Subscribed Details ' + n;
                    },
                    footer: true
                }
            ]
        });
    }
};