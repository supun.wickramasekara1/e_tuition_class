@extends('layouts.auth')
@section('title')
    User Reset Password
@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('status') }}
        </div>
    @endif
    <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>
    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="input-group mb-3">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                   placeholder="Email" value="{{ old('email') }}" autocomplete="email" autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="row">
            <div class="col-12">
                {{--<button type="submit" class="btn btn-primary btn-block">Request new password</button>--}}
                <button type="submit" class="btn btn-primary btn-block">Send Password Reset Link</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
    <div class="col-12 no-padding mt-3 mb-1">
        <a href="{{ route('login') }}">Remember your password?</a>
    </div>
@endsection
