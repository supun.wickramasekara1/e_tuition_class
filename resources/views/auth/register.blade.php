@extends('layouts.auth')
@section('title')
    User Registration
@endsection
@section('content')
    <p class="login-box-msg">Register a new user</p>

    <form method="POST" action="{{ route('register') }}">
            @csrf
        <div class="input-group mb-3">
            <input id="name" type="text" placeholder="Full name"
                   class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                   autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-3">
            <input id="userName" type="text" placeholder="User name"
                   class="form-control @error('user_name') is-invalid @enderror" name="user_name"
                   value="{{ old('user_name') }}">
            @error('user_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-3">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn active btn-teacher">
                    <input type="radio" value="TUTOR" name="user_type" id="option_a1" autocomplete="off" {{ (old('email')) ? '' : 'checked' }}>I'm Tutor
                </label>
                <label class="btn btn-student">
                    <input type="radio" value="STUDENT" name="user_type" id="option_a2" autocomplete="off">I'm Student
                </label>
            </div>
            @error('user_type')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-3">
            <input id="email" type="email" placeholder="Email"
                   class="form-control @error('email') is-invalid @enderror" name="email"
                   value="{{ old('email') }}">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-3">
            <input id="password" type="password" placeholder="Password"
                   class="form-control @error('password') is-invalid @enderror" name="password"
                   autocomplete="new-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="input-group mb-3">
            <input id="password-confirm" type="password" class="form-control" placeholder="Retype password"
                   name="password_confirmation">
        </div>
        <div class="row">
            <div class="col-8 form-link-container">
                <a href="{{ route('login') }}" class="text-center">I already registered</a>
            </div>
            <!-- /.col -->
            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
@endsection
