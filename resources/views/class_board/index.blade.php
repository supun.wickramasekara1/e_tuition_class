@extends('layouts.layout')
@section('title')
    Class Board
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Class Board</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="manual-msg"></div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <form id="formFilterModule" method="POST">
                            {{ csrf_field() }}
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Subject:</label>
                                            <div class="col-12">
                                                <select id="filterSubject" name="filter_subject" data-live-search="true"
                                                        class="form-control selectpicker custom-select">
                                                    <option value=0>All</option>
                                                    @if (isset($subjects))
                                                        @foreach($subjects as $subject)
                                                            <option value={{$subject['id']}}>{{$subject['name']}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Search Query:</label>
                                            <div class="input-group input-group-md">
                                                <input type="search" id="searchField" name="search_field"
                                                       class="form-control form-control-md"
                                                       placeholder="Type your keywords here">
                                                <div class="input-group-append">
                                                    <button type="submit" id="filterModule"
                                                            class="btn btn-md btn-default">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12" id="modulesContainer">
                    @include('class_board/related_modules')
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addReviewModal" tabindex="-1" role="dialog" aria-labelledby="addReviewLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addReviewLabel">Add Review</h5>
                    <button type="button" class="close popup-close-btn" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="ratingForm" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group text-center">
                                    <label for="bankName">Give Rating <div class="light">(<span id="ratedAmount">0</span>/5)</div></label>
                                    <ul class="rating-ul edit-rating">
                                        <li class="rating-li star1 on" data-id="1" title="Star1"><i class="fa fa-star"
                                                                                                    aria-hidden="true"></i>
                                        </li>
                                        <li class="rating-li star2" data-id="2" title="Stars 2"><i class="fa fa-star"
                                                                                                   aria-hidden="true"></i>
                                        </li>
                                        <li class="rating-li star3" data-id="3" title="Stars 3"><i class="fa fa-star"
                                                                                                   aria-hidden="true"></i>
                                        </li>
                                        <li class="rating-li star4" data-id="4" title="Stars 4"><i class="fa fa-star"
                                                                                                   aria-hidden="true"></i>
                                        </li>
                                        <li class="rating-li star5" data-id="5" title="Stars 5"><i class="fa fa-star"
                                                                                                   aria-hidden="true"></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <input type="hidden" id="starVal" name="star">
                            <input type="hidden" id="moduleId" name="module_id">
                            <div class="col-12">
                                <div class="form-group text-center">
                                    <label for="bankBranch">Your Comment</label>
                                    <textarea rows="4" name="comment" id="comment" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="submitRating" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection