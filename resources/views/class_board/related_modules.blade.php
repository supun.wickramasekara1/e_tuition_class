<div class="card">
    <div class="card-header">
        <h3 class="card-title">Related Modules</h3>
    </div>
    <div class="card-body">
        <div id="accordion">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title w-100">
                        <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                            All Modules - {{$counts['otherCount']}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="collapse show" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            {{--                            {{dd($modules, $subscribedModuleIds)}}--}}
                            @if (count($modules) > 0)
                                @foreach($modules as $module)
                                    @if (!in_array($module['id'], $subscribedModuleIds) && $module['status'] !== 'HOLD')
                                        <div class="col-lg-4 col-md-6 col-12 module-container">
                                            @if($module['is_free'])
                                                <div class="ribbon-wrapper">
                                                    <div class="ribbon bg-warning">
                                                        Free
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="card card-widget widget-user">
                                                <div class="widget-user-header text-white"
                                                     style="background: url('{{ asset('storage/modules/'.$module['module_image']) }}') center center;">
                                                    <h3 class="widget-user-username text-left">{{$module['topic']}}</h3>
                                                    <h5 class="widget-user-desc text-left">Start Date
                                                        : {{$module['scheduled_at']}}</h5>
                                                </div>
                                                <div class="widget-user-image">
                                                    <img class="img-circle"
                                                         src="{{ asset('storage/images/'.$module['user']['image']) }}"
                                                         alt="User Avatar">
                                                </div>
                                                <div class="card-footer">
                                                    <div class="row">
                                                        <span class="badge badge-{{($module['type'] == 'ONLINE') ? 'success' : 'danger'}} module-type">{{$module['type']}}</span>
                                                        <div class="col-12 text-center tutor-name">
                                                            <span>{{$module['user']['name']}}</span>
                                                        </div>
                                                        <div class="col-4 border-right">
                                                            <div class="description-block">
                                                                {{--<h5 class="description-header">{{$module['scheduled_at']}}</h5>--}}
                                                                {{--<span class="description-text">START DATE</span>--}}
                                                                <h5 class="description-header">{{$module['subject']['name']}}</h5>
                                                                <span class="description-text">SUBJECT</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-4 border-right">
                                                            <div class="description-block">
                                                                <h5 class="description-header">{{count($module['sub_modules'])}}</h5>
                                                                <span class="description-text">MODULES</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-4">
                                                            <div class="description-block">
                                                                <h5 class="description-header">{{count($module['subscribe_details'])}}</h5>
                                                                <span class="description-text">ENROLLED</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-12 btn-container flex-container">
                                                            <a href="{{ route('viewModule', $module['id']) }}"
                                                               class="btn btn-success" data-id="$module['id']">View</a>
                                                            {{--<button class="btn btn-outline-warning text-center">View--}}
                                                                {{--Ratings--}}
                                                            {{--</button>--}}
                                                            <a href="{{ route('paymentDetails', $module['id']) }}"
                                                               class="btn btn-primary">Subscribe</a>
                                                        </div>
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                            </div>
                                            <!-- /.widget-user -->
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-success">
                <div class="card-header">
                    <h4 class="card-title w-100">
                        <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo">
                            Subscribed Modules - {{$counts['subCount']}}
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            @if (count($modules) > 0)
                                {{--                                {{ $modules }}--}}
                                @foreach($modules as $module)
                                    @if (in_array($module['id'], $subscribedModuleIds))
                                        <div class="col-lg-4 col-md-6 col-12 module-container">
                                            @if($module['is_free'])
                                                <div class="ribbon-wrapper">
                                                    <div class="ribbon bg-warning">
                                                        Free
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="card card-widget widget-user">
                                                <div class="widget-user-header text-white"
                                                     style="background: url('{{ asset('storage/modules/'.$module['module_image']) }}') center center;">
                                                    <h3 class="widget-user-username text-left">{{$module['topic']}}</h3>
                                                    <h5 class="widget-user-desc text-left">Start Date
                                                        : {{$module['scheduled_at']}}</h5>
                                                </div>
                                                <div class="widget-user-image">
                                                    <img class="img-circle"
                                                         src="{{ asset('storage/images/'.$module['user']['image']) }}"
                                                         alt="User Avatar">
                                                </div>
                                                <div class="card-footer">
                                                    <div class="row">
                                                        <span class="badge badge-{{($module['type'] == 'ONLINE') ? 'success' : 'danger'}} module-type">{{$module['type']}}</span>
                                                        <div class="col-12 text-center tutor-name">
                                                            <span>{{$module['user']['name']}}</span>
                                                        </div>
                                                        <div class="col-4 border-right">
                                                            <div class="description-block">
                                                                {{--<h5 class="description-header">{{$module['scheduled_at']}}</h5>--}}
                                                                {{--<span class="description-text">START DATE</span>--}}
                                                                <h5 class="description-header">{{$module['subject']['name']}}</h5>
                                                                <span class="description-text">SUBJECT</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-4 border-right">
                                                            <div class="description-block">
                                                                <h5 class="description-header">{{count($module['sub_modules'])}}</h5>
                                                                <span class="description-text">MODULES</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-4">
                                                            <div class="description-block">
                                                                <h5 class="description-header">{{count($module['subscribe_details'])}}</h5>
                                                                <span class="description-text">ENROLLED</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-12 btn-container flex-container">
                                                            <a href="{{ route('viewModule', $module['id']) }}"
                                                               class="btn btn-outline-success">View</a>
                                                            @if (!in_array($module['id'], $pendingModuleIds))
                                                                <button class="btn btn-warning add-review"
                                                                        data-id="{{$module['id']}}">Add Review
                                                                </button>
                                                            @endif
                                                            <button class="btn btn-outline-info" disabled="disabled">
                                                                {{in_array($module['id'], $pendingModuleIds) ? 'Pending' : 'Subscribed'}}</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                            </div>
                                            <!-- /.widget-user -->
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>