@extends('layouts.layout')
@section('title')
    Dashboard
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard</h1>
                </div><!-- /.col -->
                {{--<div class="col-sm-6">--}}
                    {{--<ol class="breadcrumb float-sm-right">--}}
                        {{--<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">eTuition Class</a></li>--}}
                        {{--<li class="breadcrumb-item active">Dashboard</li>--}}
                    {{--</ol>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small card -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$stats['pubCounts']}}</h3>

                            <p>Published Coursers</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-shopping-cart"></i>
                        </div>
{{--                        <a href="#" class="small-box-footer">--}}
{{--                            More info <i class="fas fa-arrow-circle-right"></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small card -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{$stats['onCounts']}}</h3>
                            <p>Online Coursers</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
{{--                        <a href="#" class="small-box-footer">--}}
{{--                            More info <i class="fas fa-arrow-circle-right"></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small card -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{$stats['offCounts']}}</h3>
                            <p>Offline Coursers</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-user-plus"></i>
                        </div>
{{--                        <a href="#" class="small-box-footer">--}}
{{--                            More info <i class="fas fa-arrow-circle-right"></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small card -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{$stats['subCounts']}}</h3>
                            <p>Subscribed Students</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-chart-pie"></i>
                        </div>
{{--                        <a href="#" class="small-box-footer">--}}
{{--                            More info <i class="fas fa-arrow-circle-right"></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Recently added modules</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                @if (isset($modules) && count($modules) > 0)
                                    @foreach($modules as $module)
                                        <div class="col-md-4 module-container">
                                            @if($module['is_free'])
                                                <div class="ribbon-wrapper">
                                                    <div class="ribbon bg-warning">
                                                        Free
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="card card-widget widget-user">
                                                <div class="widget-user-header text-white"
                                                     style="background: url('{{ asset('storage/modules/'.$module['module_image']) }}') center center;">
                                                    <h3 class="widget-user-username text-left">{{$module['topic']}}</h3>
                                                    <h5 class="widget-user-desc text-left">Start Date
                                                        : {{$module['scheduled_at']}}</h5>
                                                </div>
                                                <div class="widget-user-image">
                                                    <img class="img-circle"
                                                         src="{{ asset('storage/images/'.$module['user']['image']) }}"
                                                         alt="User Avatar">
                                                </div>
                                                <div class="card-footer">
                                                    <div class="row">
                                                        <span class="badge badge-{{($module['type'] == 'ONLINE') ? 'success' : 'danger'}} module-type">{{$module['type']}}</span>
                                                        <div class="col-12 text-center tutor-name">
                                                            <span>{{$module['user']['name']}}</span>
                                                        </div>
                                                        <span class="badge badge-{{($module['status'] == 'PUBLISHED') ? 'info' : 'warning'}} module-status">{{$module['status']}}</span>
                                                        <div class="col-sm-4 border-right">
                                                            <div class="description-block">
                                                                {{--<h5 class="description-header">{{$module['scheduled_at']}}</h5>--}}
                                                                {{--<span class="description-text">START DATE</span>--}}
                                                                <h5 class="description-header">{{$module['subject']['name']}}</h5>
                                                                <span class="description-text">SUBJECT</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-sm-4 border-right">
                                                            <div class="description-block">
                                                                <h5 class="description-header">{{count($module['sub_modules'])}}</h5>
                                                                <span class="description-text">MODULES</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-sm-4">
                                                            <div class="description-block">
                                                                <h5 class="description-header">{{count($module['subscribe_details'])}}</h5>
                                                                <span class="description-text">ENROLLED</span>
                                                            </div>
                                                            <!-- /.description-block -->
                                                        </div>
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                            </div>
                                            <!-- /.widget-user -->
                                        </div>
                                    @endforeach
                                        @if (count($modules) < 3)
                                            <div class="col-md-4">
                                                <div class="card">
                                                    <div class="card-body text-center no-module-text">
                                                        <div class="col-12">
                                                            <a href="{{ route('moduleView') }}"><i class="fas fa-plus"></i></a>
                                                        </div>
                                                        <div class="col-12">
                                                            <small>Add module now...</small>
                                                        </div>
                                                    </div>
                                                    {{--<div class="card-footer text-right">--}}
                                                    {{--<a href="{{ route('moduleView') }}" class="btn btn-success">Add now</a>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                        @endif
                                    @else
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body text-center no-module-text">
                                                <div class="col-12">
                                                    <a href="{{ route('moduleView') }}"><i class="fas fa-plus"></i></a>
                                                </div>
                                                <div class="col-12">
                                                    <small>Add your first module now...</small>
                                                </div>
                                            </div>
                                            {{--<div class="card-footer text-right">--}}
                                            {{--<a href="{{ route('moduleView') }}" class="btn btn-success">Add now</a>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
@endsection
