<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eTuitionClass - @yield('title')</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    {{--<!-- daterange picker -->--}}
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
          href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- fullCalendar -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.css"/>--}}
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">--}}
<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/bootstrap-select.min.css') }}">
{{--<link rel="stylesheet"--}}
{{--href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">--}}
<!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Custom style -->
    <link rel="stylesheet" href="{{ asset('css/custom.scss') }}">
</head>
{{--<body class="hold-transition sidebar-mini layout-footer-fixed">--}}{{--  Footer fixed--}}
<body class="hold-transition sidebar-mini layout-navbar-fixed layout-fixed">
<div id='loader'>
    <ul>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            {{--<li class="nav-item d-none d-sm-inline-block">--}}
            {{--<a href="index3.html" class="nav-link">Home</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item d-none d-sm-inline-block">--}}
            {{--<a href="#" class="nav-link">Contact</a>--}}
            {{--</li>--}}
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Navbar Search -->
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" data-widget="navbar-search" href="#" role="button">--}}
        {{--<i class="fas fa-search"></i>--}}
        {{--</a>--}}
        {{--<div class="navbar-search-block">--}}
        {{--<form class="form-inline">--}}
        {{--<div class="input-group input-group-sm">--}}
        {{--<input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">--}}
        {{--<div class="input-group-append">--}}
        {{--<button class="btn btn-navbar" type="submit">--}}
        {{--<i class="fas fa-search"></i>--}}
        {{--</button>--}}
        {{--<button class="btn btn-navbar" type="button" data-widget="navbar-search">--}}
        {{--<i class="fas fa-times"></i>--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
        {{--</div>--}}
        {{--</li>--}}

        <!-- Messages Dropdown Menu -->
        {{--<li class="nav-item dropdown">--}}
        {{--<a class="nav-link" data-toggle="dropdown" href="#">--}}
        {{--<i class="far fa-comments"></i>--}}
        {{--<span class="badge badge-danger navbar-badge">3</span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
        {{--<a href="#" class="dropdown-item">--}}
        {{--<!-- Message Start -->--}}
        {{--<div class="media">--}}
        {{--<img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">--}}
        {{--<div class="media-body">--}}
        {{--<h3 class="dropdown-item-title">--}}
        {{--Brad Diesel--}}
        {{--<span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>--}}
        {{--</h3>--}}
        {{--<p class="text-sm">Call me whenever you can...</p>--}}
        {{--<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- Message End -->--}}
        {{--</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a href="#" class="dropdown-item">--}}
        {{--<!-- Message Start -->--}}
        {{--<div class="media">--}}
        {{--<img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">--}}
        {{--<div class="media-body">--}}
        {{--<h3 class="dropdown-item-title">--}}
        {{--John Pierce--}}
        {{--<span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>--}}
        {{--</h3>--}}
        {{--<p class="text-sm">I got your message bro</p>--}}
        {{--<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- Message End -->--}}
        {{--</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a href="#" class="dropdown-item">--}}
        {{--<!-- Message Start -->--}}
        {{--<div class="media">--}}
        {{--<img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">--}}
        {{--<div class="media-body">--}}
        {{--<h3 class="dropdown-item-title">--}}
        {{--Nora Silvester--}}
        {{--<span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>--}}
        {{--</h3>--}}
        {{--<p class="text-sm">The subject goes here</p>--}}
        {{--<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- Message End -->--}}
        {{--</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a href="#" class="dropdown-item dropdown-footer">See All Messages</a>--}}
        {{--</div>--}}
        {{--</li>--}}
        <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">{{auth()->user()->unreadNotifications->count()}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right notification-dropdown">
                    <span class="dropdown-header">{{auth()->user()->unreadNotifications->count()}} Notifications</span>
                    @if(auth()->user()->unreadNotifications->count() >0)
                        @foreach(auth()->user()->unreadNotifications as $notification)
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('viewSubModule', [$notification->data['id']]) }}"
                               class="dropdown-item text-{{$notification->data['type']}} text-sm">
                                Module
                                at {{\Carbon\Carbon::parse($notification->data['scheduled_at'])->format('Y-m-d H:i')}}
                                is canceled.
                                <span class="float-right text-muted text-xs"><i
                                            class="fas fa-clock"></i>{{$notification->created_at->diffForHumans()}}</span>
                            </a>
                        @endforeach
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>--}}
                    @endif
                    {{--<div class="dropdown-divider"></div>--}}
                    {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="fas fa-users mr-2"></i> 8 friend requests--}}
                    {{--<span class="float-right text-muted text-sm">12 hours</span>--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-divider"></div>--}}
                    {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="fas fa-file mr-2"></i> 3 new reports--}}
                    {{--<span class="float-right text-muted text-sm">2 days</span>--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-divider"></div>--}}

                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ (Auth::user()->image) ? asset('storage/images/'.(Auth::user()->image)) : asset('storage/images/default_user.png') }}"
                             class="user-image img-circle elevation-2" alt="User Image">
                        <span class="d-none d-md-inline">{{Auth::user()->name}}</span>
                    </a>
                    {{--        {{dd(auth()->user()->unreadNotifications)}}--}}
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <!-- User image -->
                        <li class="user-header bg-primary">
                            <img src="{{ (Auth::user()->image) ? asset('storage/images/'.(Auth::user()->image)) : asset('storage/images/default_user.png') }}"
                                 class="img-circle elevation-2"
                                 alt="User Image">
                            <p>
                                {{Auth::user()->name .' - '. Auth::user()->user_type}}
                                <small>Member since {{ date('M Y', strtotime(Auth::user()->created_at))}} </small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                    {{--<li class="user-body">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-4 text-center">--}}
                    {{--<a href="#">Followers</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-4 text-center">--}}
                    {{--<a href="#">Sales</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-4 text-center">--}}
                    {{--<a href="#">Friends</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.row -->--}}
                    {{--</li>--}}
                    <!-- Menu Footer-->
                        <li class="user-footer">
                            <a href="{{ route('userProfile') }}" class="btn btn-default btn-flat">Profile</a>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                               class="btn btn-default btn-flat float-right">Sign out</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                {{--<li class="nav-item dropdown">--}}
                {{--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"--}}
                {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
                {{--{{ Auth::user()->name }}--}}
                {{--</a>--}}

                {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
                {{--<a href="#" class="dropdown-item">--}}
                {{--<i class="fas fa-user mr-2"></i>User Profile--}}
                {{--</a>--}}
                {{--<div class="dropdown-divider"></div>--}}
                {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                {{--onclick="event.preventDefault();--}}
                {{--document.getElementById('logout-form').submit();">--}}
                {{--{{ __('Logout') }}--}}
                {{--</a>--}}

                {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">--}}
                {{--@csrf--}}
                {{--</form>--}}
                {{--</div>--}}
                {{--</li>--}}
            @endguest
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">--}}
            {{--<i class="fas fa-th-large"></i>--}}
            {{--</a>--}}
            {{--</li>--}}
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('dashboard') }}" class="brand-link">
            {{--<img src="dist/img/eTuitionCls.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"--}}
            <img src="{{ asset('dist/img/eTuitionCls.png') }}" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">eTuition Class</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
        {{--<div class="user-panel mt-3 pb-3 mb-3 d-flex">--}}
        {{--<div class="image">--}}
        {{--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">--}}
        {{--</div>--}}
        {{--<div class="info">--}}
        {{--<a href="#" class="d-block">Alexander Pierce</a>--}}
        {{--</div>--}}
        {{--</div>--}}

        <!-- SidebarSearch Form -->
        {{--<div class="form-inline">--}}
        {{--<div class="input-group" data-widget="sidebar-search">--}}
        {{--<input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">--}}
        {{--<div class="input-group-append">--}}
        {{--<button class="btn btn-sidebar">--}}
        {{--<i class="fas fa-search fa-fw"></i>--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        <!-- Sidebar Menu -->
            {{--<nav class="mt-2">--}}
            <nav>
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    @if (auth()->user()->user_type == 'ADMIN')
                        <li class="nav-item">
                            <a href="{{ route('adminBoard') }}"
                               class="nav-link {{ request()->is('admin_board') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-chalkboard"></i>
                                <p>
                                    Admin Board
                                    {{--<span class="right badge badge-danger">New</span>--}}
                                </p>
                            </a>
                        </li>
                    @else
                    <!-- Classboard menu -->
                        @if (auth()->user()->user_type == 'STUDENT')
                            <li class="nav-item">
                                <a href="{{ route('classBoard') }}"
                                   class="nav-link {{ request()->is('class_board') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-chalkboard"></i>
                                    <p>
                                        Classboard
                                        {{--<span class="right badge badge-danger">New</span>--}}
                                    </p>
                                </a>
                            </li>
                        @else
                        <!-- Dashboard menu -->
                            <li class="nav-item">
                                <a href="{{ route('dashboard') }}"
                                   class="nav-link {{ request()->is('dashboard') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                        {{--<span class="right badge badge-danger">New</span>--}}
                                    </p>
                                </a>
                            </li>
                        @endif
                        <li class="nav-item {{ request()->is(['module*', 'sub_module*', 'schedule_details*']) ? 'menu-open' : '' }}">
                            <a href="#"
                               class="nav-link {{ request()->is(['module*', 'sub_module*', 'schedule_details*']) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-chalkboard-teacher"></i>
                                <p>
                                    Module Management
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @if (auth()->user()->user_type == 'TUTOR')
                                    <li class="nav-item">
                                        <a href="{{ route('moduleView') }}"
                                           class="nav-link {{ request()->is('module') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Create Module</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('subModuleView') }}"
                                           class="nav-link {{ request()->is('sub_module') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Create Sub Module</p>
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a href="{{ route('scheduleView') }}"
                                       class="nav-link {{ request()->is('schedule_details') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Schedule Details</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item {{ request()->is(['subscribe*']) ? 'menu-open' : '' }}">
                            <a href="#"
                               class="nav-link {{ request()->is(['subscribe*']) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-file-invoice-dollar"></i>
                                <p>
                                    Subscribe Management
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('subscribedData') }}"
                                       class="nav-link {{ request()->is('subscribe') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>
                                            Subscribe Data
                                            {{--<span class="right badge badge-danger">New</span>--}}
                                        </p>
                                    </a>
                                </li>
                                @if (auth()->user()->user_type == 'TUTOR')
                                    <li class="nav-item">
                                        <a href="{{ route('approveSubscription') }}"
                                           class="nav-link {{ request()->is('subscribe/approve') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>
                                                Approve Subscription
                                                {{--<span class="right badge badge-danger">New</span>--}}
                                            </p>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('userProfile') }}"
                               class="nav-link {{ request()->is('user_details*') ? 'active' : '' }}">
                                {{--<i class="nav-icon fas fa-users-gear"></i>--}}
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    User Profile
                                    {{--<span class="right badge badge-danger">New</span>--}}
                                </p>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @yield('content')
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            eTuition Class
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">etuitionlk.com</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script type="text/javascript">
    var base_url = '{!! json_encode(url('/')) !!}';
</script>

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- bs-custom-file-input -->
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
{{--<script src="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}"></script>--}}
<!-- Latest compiled and minified JavaScript -->
{{--<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>--}}
<!-- InputMask -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- fullCalendar 2.2.5 -->
<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src='https://unpkg.com/popper.js/dist/umd/popper.min.js'></script>
<script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.js"></script>--}}
<!-- date-range-picker -->
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('js/common.js') }}"></script>
</body>
</html>
