<div class="row">
    <div class="col-12">
        <div class="card">
            {{--<div class="card-body">--}}
            <form method="POST" action="{{ route('addModule') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label>Grade</label>
                                <select id="moduleGrade" name="grade_id"
                                        class="form-control select2 custom-select @error('grade_id') is-invalid @enderror">
                                    {{--<option selected disabled>Select grade</option>--}}
                                    <option></option>
                                    @if (isset($grades))
                                        @if (old('grade_id'))
                                            @foreach($grades as $grade)
                                                @if (old('grade_id') == $grade['id'])
                                                    <option selected
                                                            value={{$grade['id']}}>{{$grade['name']}}</option>
                                                @else
                                                    <option value={{$grade['id']}}>{{$grade['name']}}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach($grades as $grade)
                                                @if ((isset($moduleData['grade_id'])) && ($moduleData['grade_id'] == $grade['id']))
                                                    <option selected
                                                            value={{$grade['id']}}>{{$grade['name']}}</option>
                                                @else
                                                    <option value={{$grade['id']}}>{{$grade['name']}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                </select>
                                @error('grade_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label>Subject</label>
                                <select id="moduleSubject" name="subject_id"
                                        class="form-control select2 custom-select @error('subject_id') is-invalid @enderror">
                                    {{--<option selected disabled>Select subject</option>--}}
                                    <option></option>
                                    @if (isset($subjects))
                                        @if (old('subject_id'))
                                            @foreach($subjects as $subject)
                                                @if (old('subject_id') == $subject['id'])
                                                    <option selected
                                                            value={{$subject['id']}}>{{$subject['name']}}</option>
                                                @else
                                                    <option value={{$subject['id']}}>{{$subject['name']}}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach($subjects as $subject)
                                                @if ((isset($moduleData['subject_id'])) && ($moduleData['subject_id'] == $subject['id']))
                                                    <option selected
                                                            value={{$subject['id']}}>{{$subject['name']}}</option>
                                                @else
                                                    <option value={{$subject['id']}}>{{$subject['name']}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                </select>
                                @error('subject_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label>Topic</label>
                                <input type="text" name="topic" id="moduleTopic" placeholder="Topic"
                                       class="form-control @error('topic') is-invalid @enderror"
                                       value="{{(old('topic')) ? old('topic') : (isset($moduleData['topic']) ? $moduleData['topic'] : '')}}">
                                @error('topic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-9">
                                    <div class="form-group required">
                                        <label for="userName">Module Fee</label>
                                        <input type="number" name="module_fee" id="moduleFee" placeholder="Module Fee"
                                               class="no-spinner form-control @error('module_fee') is-invalid @enderror"
                                               value="{{(old('module_fee')) ? old('module_fee') : (isset($moduleData['module_fee']) ? $moduleData['module_fee'] : '')}}"
                                                {{(old('is_free')) ? 'disabled' : ((isset($moduleData['is_free']) && ($moduleData['is_free'])) ? 'disabled' : '')}}>
                                        @error('module_fee')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="userName">Is Free</label>
                                        <div class="custom-control custom-switch custom-switch-md">
                                            <input type="checkbox" name="is_free"
                                                   {{(old('is_free')) ? 'checked' : ((isset($moduleData['is_free']) && ($moduleData['is_free'])) ? 'checked' : '')}}
                                                   value=true
                                                   class="custom-control-input" id="isFree">
                                            <label class="custom-control-label" for="isFree"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                {{--                                <div class="{{(isset($moduleData['type']) && ($moduleData['type'] === 'ONLINE')) ? 'col-5' : 'col-12'}}">--}}
                                <div class="col-5">
                                    <div class="form-group">
                                        <label for="userName">Type</label>
                                        <div class="form-inline">
                                            <div class="form-check">
                                                <input class="form-check-input module-type" value="ONLINE" type="radio"
                                                       name="type"
                                                        {{((old('type') === 'ONLINE') ? 'checked' : ((isset($moduleData['type']) && ($moduleData['type'] === 'ONLINE')) ? 'checked' : 'checked'))}}>
                                                <label class="form-check-label">Online</label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input module-type" value="OFFLINE" type="radio"
                                                       name="type"
                                                        {{((old('type') === 'OFFLINE') ? 'checked' : ((isset($moduleData['type']) && ($moduleData['type'] === 'OFFLINE')) ? 'checked' : ''))}}>
                                                <label class="form-check-label">Offline</label>
                                            </div>
                                        </div>
                                        @error('topic')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-7 hideable-container {{ ((old('type')) ? strtolower(old('type')) : ((isset($moduleData['type'])) ? strtolower($moduleData['type']) : '')) }}">
                                    <div class="form-group required">
                                        <label for="userName">Module Start Date/Time</label>
                                        <div class="input-group date @error('scheduled_at') is-invalid @enderror"
                                             id="moduleStartDate" data-target-input="nearest">
                                            <input type="text" data-target="#moduleStartDate" name="scheduled_at"
                                                   data-date-format="YYYY-MM-DD HH:mm"
                                                   class="form-control datetimepicker-input @error('scheduled_at') is-invalid @enderror"
                                                   value="{{(old('scheduled_at')) ? old('scheduled_at') : (isset($moduleData['scheduled_at']) ? $moduleData['scheduled_at'] : '')}}"
                                            />
                                            <div class="input-group-append" data-target="#moduleStartDate"
                                                 data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @error('scheduled_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 hideable-container {{ ((old('type')) ? strtolower(old('type')) : ((isset($moduleData['type'])) ? strtolower($moduleData['type']) : '')) }}">
                            <div class="form-group required">
                                <div class="row">
                                    <div class="col-6">
                                        <label for="userName">Module Duration
                                            <small>(Hours)</small>
                                        </label>
                                        <input type="text" name="hours" placeholder="Module Duration (XX:XX)"
                                               class="form-control @error('hours') is-invalid @enderror"
                                               value="{{(old('hours')) ? old('hours') : (isset($moduleData['hours']) ? $moduleData['hours'] : '')}}">
                                        @error('hours')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label for="userName">No of Sub Modules</label>
                                        <input type="number" min="0" name="occurrence"
                                               placeholder="No of Sub Modules"
                                               class="form-control @error('occurrence') is-invalid @enderror"
                                               value="{{(old('occurrence')) ? old('occurrence') : (isset($moduleData['occurrence']) ? $moduleData['occurrence'] : '')}}">
                                        @error('occurrence')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label for="userProfilePic">Module Image</label>
                                <div class="input-group">
                                    <div class="custom-file @error('module_image') is-invalid @enderror">
                                        <input type="file" name="module_image" id="moduleBgPic"
                                               class="custom-file-input @error('module_image') is-invalid @enderror"
                                               value="{{(old('module_image')) ? old('module_image') : (isset($moduleData['module_image']) ? $moduleData['module_image'] : '')}}">
                                        <label class="custom-file-label" for="moduleBgPic">Choose picture</label>
                                        <input type="hidden" name="module_image_hidden"
                                               value="{{(old('module_image_hidden')) ? old('module_image_hidden') : (isset($moduleData['module_image']) ? $moduleData['module_image'] : '')}}">
                                    </div>
                                    @error('module_image')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label for="userDescription">Short Description
                                    <small>(Min 100 characters)</small>
                                </label>
                                <textarea id="userDescription" name="short_desc"
                                          class="form-control @error('short_desc') is-invalid @enderror"
                                          rows="4">{{(old('short_desc')) ? old('short_desc') : (isset($moduleData['short_desc']) ? $moduleData['short_desc'] : '')}}</textarea>
                                @error('short_desc')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <input type="hidden" name="id"
                               value="{{(old('id')) ? old('id') : (isset($moduleData['id']) ? $moduleData['id'] : '')}}">
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <div class="text-right">
                        <button type="submit"
                                class="btn btn-primary float-left">{{ (old('id')) ? 'Update' : (isset($moduleData['id']) ? 'Update' : 'Create') }}</button>
                        @if (isset($moduleData['id']))
                            <a href="{{ route('moduleView') }}" class="btn btn-success">Add New</a>
                        @endif
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </form>
            {{--</div>--}}
        </div>
    </div>
</div>