<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Added Modules</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="moduleTable" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Start Date</th>
                        <th>Topic</th>
                        <th>Type</th>
                        <th>Fee</th>
                        <th>Grade</th>
                        <th>Subject</th>
                        <th>Sub Module Count</th>
                        <th>Status</th>
                        <th>Action(s)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tableData as $data)
                        <tr>
                            <td>{{$data['scheduled_at']}}</td>
                            <td>{{$data['topic']}}</td>
                            <td>{{$data['type']}}</td>
                            @if ($data['is_free'])
                                <td class="text-center"><span class="badge bg-success">Free</span></td>
                            @else
                                <td class="text-right">{{'Rs.'.$data['module_fee']}}</td>
                            @endif
                            <td>{{$data['grade']['name']}}</td>
                            <td>{{$data['subject']['name']}}</td>
                            <td>{{$data['occurrence']}}</td>
                            <td>
                                <span class="badge bg-{{($data['status'] === 'PUBLISHED') ? 'success' : (($data['status'] === 'EXPIRED') ? 'danger' : 'warning')}}">{{$data['status']}}</span>
                            </td>
                            <td class="text-center action-column">
                                <div class="btn-group btn-group-sm">
                                    @if ($data['status'] === 'PUBLISHED')
                                        <a href="{{ route('viewModule', $data['id']) }}" title="View Module"
                                           data-action="view" class="btn btn-sm btn-primary"><i
                                                    class="fa fa-eye"></i> <span
                                                    class="hide-on-mobile"></span>
                                        </a>
                                        <button data-id="{{$data['id']}}" title="Unpubiish Module"
                                                class="btn btn-sm btn-warning unpublish-module">Unpublish
                                        </button>
                                        <button data-id="{{$data['id']}}" title="Stop Advertising"
                                                class="btn btn-sm btn-default hold-module">Hold
                                        </button>
                                    @elseif ($data['status'] === 'UNPUBLISHED')
                                        <button data-id="{{$data['id']}}" class="btn btn-sm btn-success publish-module">
                                            Publish
                                        </button>
                                        <button data-id="{{$data['id']}}" class="btn btn-sm btn-primary edit-module"
                                                title="Edit Module"><i class="fas fa-edit"></i></button>
                                        <button data-id="{{$data['id']}}" class="btn btn-sm btn-danger delete-module"
                                                title="Delete Module"><i class="fas fa-trash"></i></button>
                                    @elseif ($data['status'] === 'HOLD')
                                        <a href="{{ route('viewModule', $data['id']) }}" title="View Module"
                                           data-action="view" class="btn btn-sm btn-primary"><i
                                                    class="fa fa-eye"></i> <span
                                                    class="hide-on-mobile"></span>
                                        </a>
                                        <button data-id="{{$data['id']}}" class="btn btn-sm btn-success publish-module">
                                            Publish
                                        </button>
                                    @else
                                        <a href="{{ route('viewSubModule', $data['id']) }}" title="View Module"
                                           data-action="view" class="btn btn-sm btn-primary"><i
                                                    class="fa fa-eye"></i> <span
                                                    class="hide-on-mobile"></span>
                                        </a>
                                        <button data-id="{{$data['id']}}" class="btn btn-sm btn-danger"
                                                title="Delete Module"><i class="fas fa-trash"></i></button>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>