@extends('layouts.layout')
@section('title')
    Module View
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Module View</h1>
                </div><!-- /.col -->
                {{--<div class="col-sm-6 text-right">--}}
                {{--<div class="custom-back-btn">--}}
                {{--<a href="{{ (Auth::user()->user_type === 'STUDENT') ?  route('classBoard') :  url()->previous() }}" type="button"--}}
                {{--class="btn btn-warning btn-block"><i--}}
                {{--class="fas fa-angle-double-left"></i> Back--}}
                {{--to Module--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                {{--<ol class="breadcrumb float-sm-right">--}}
                {{--<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">eTuition Class</a></li>--}}
                {{--<li class="breadcrumb-item active">Subscribed Data</li>--}}
                {{--</ol>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content module-view">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="manual-msg"></div>
                    @if($userType !== 'REG')
                        <div class="alert alert-warning" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{($userTypeMsg)}}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-sm-8 col-12">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Module details</h3>
                                    @if (auth()->user()->user_type !== 'STUDENT')
                                        <span class="badge badge-{{($moduleData['status'] == 'PUBLISHED') ? 'success' : (($moduleData['status'] == 'EXPIRED') ? 'danger' : 'warning')}} module-status">{{$moduleData['status']}}</span>
                                    @endif
                                </div>
                                <div class="card-body">
                                    <form>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Grade</label>
                                                        <input type="text" name="topic" id="moduleTopic"
                                                               placeholder="Topic"
                                                               class="form-control" disabled
                                                               value="{{isset($moduleData['grade']['name']) ? $moduleData['grade']['name'] : ''}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Subject</label>
                                                        <input type="text" name="topic" id="moduleTopic"
                                                               placeholder="Topic"
                                                               class="form-control" disabled
                                                               value="{{isset($moduleData['subject']['name']) ? $moduleData['subject']['name'] : ''}}">

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Topic</label>
                                                        <input type="text" name="topic" id="moduleTopic"
                                                               placeholder="Topic"
                                                               class="form-control" disabled
                                                               value="{{isset($moduleData['topic']) ? $moduleData['topic'] : ''}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <div class="col-9">
                                                            <label for="userName">Module Fee</label>
                                                            <input type="number" name="module_fee" id="moduleFee"
                                                                   disabled
                                                                   placeholder="Module Fee"
                                                                   class="no-spinner form-control"
                                                                   value="{{isset($moduleData['module_fee']) ? $moduleData['module_fee'] : ''}}">
                                                        </div>
                                                        <div class="col-3">
                                                            <label for="userName">Is Free</label>
                                                            <div class="custom-control custom-switch custom-switch-md">
                                                                <input type="checkbox" name="is_free" disabled
                                                                       {{(isset($moduleData['is_free']) && ($moduleData['is_free'])) ? 'checked' : ''}}
                                                                       value=true
                                                                       class="custom-control-input" id="isFree">
                                                                <label class="custom-control-label"
                                                                       for="isFree"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="{{(isset($moduleData['type']) && ($moduleData['type'] === 'ONLINE')) ? 'col-5' : 'col-12'}}">
                                                            <div class="form-group">
                                                                <label for="userName">Type</label>
                                                                <div class="form-inline">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input module-type"
                                                                               value="ONLINE" type="radio" name="type"
                                                                               disabled
                                                                                {{((isset($moduleData['type']) && ($moduleData['type'] === 'ONLINE')) ? 'checked' : 'checked')}}>
                                                                        <label class="form-check-label">Online</label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input module-type"
                                                                               value="OFFLINE" type="radio" name="type"
                                                                               disabled
                                                                                {{((isset($moduleData['type']) && ($moduleData['type'] === 'OFFLINE')) ? 'checked' : '')}}>
                                                                        <label class="form-check-label">Offline</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if (isset($moduleData['type']) && ($moduleData['type'] === 'ONLINE'))
                                                            <div class="col-7 hideable-container ">
                                                                <div class="form-group">
                                                                    <label>Module Start Date/Time</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" disabled
                                                                               value="{{isset($moduleData['scheduled_at']) ? $moduleData['scheduled_at'] : ''}}">
                                                                        <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i
                                                                                class="fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                @if (isset($moduleData['type']) && ($moduleData['type'] === 'ONLINE'))
                                                    <div class="col-md-6 hideable-container ">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <label for="userName">Module Duration
                                                                        <small>(Hours)</small>
                                                                    </label>
                                                                    <input type="text" name="hours" disabled
                                                                           placeholder="Module Duration (XX:XX)"
                                                                           class="form-control @error('hours') is-invalid @enderror"
                                                                           value="{{isset($moduleData['hours']) ? $moduleData['hours'] : ''}}">
                                                                </div>
                                                                <div class="col-6">
                                                                    <label for="userName">No of Sub Modules</label>
                                                                    <input type="number" min="0" name="occurrence"
                                                                           placeholder="No of Sub Modules" disabled
                                                                           class="form-control"
                                                                           value="{{isset($moduleData['occurrence']) ? $moduleData['occurrence'] : ''}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="userProfilePic">Module Image</label>
                                                        <div class="input-group">
                                                            <div class="custom-file ">
                                                                <input type="text" name="module_image" id="moduleBgPic"
                                                                       class="custom-file-input" disabled
                                                                       value="{{isset($moduleData['module_image']) ? $moduleData['module_image'] : ''}}">
                                                                <label class="custom-file-label"
                                                                       for="moduleBgPic">{{isset($moduleData['module_image']) ? $moduleData['module_image'] : ''}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="userDescription">Short Description
                                                            <small>(Min 100 characters)</small>
                                                        </label>
                                                        <textarea id="userDescription" name="short_desc" disabled
                                                                  class="form-control @error('short_desc') is-invalid @enderror"
                                                                  rows="4">{{isset($moduleData['short_desc']) ? $moduleData['short_desc'] : ''}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Sub modules</h3>
                                </div>
                                <div class="card-body">
                                    <table id="subModuleTable" class="table table-bordered table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Schedule Date</th>
                                            <th>Topic</th>
                                            <th>URL</th>
                                            <th class="text-center">Status</th>
                                            @if ($userType === 'REG')
                                                <th width="175px">Action</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($moduleData['sub_modules']))
                                            @foreach($moduleData['sub_modules'] as $data)
                                                <tr>
                                                    <td>{{$data['scheduled_at']}}</td>
                                                    <td>{{$data['sub_topic']}}</td>
                                                    <td>{{$data['url']}}</td>
                                                    <td class="text-center">
                                                        <span class="badge bg-{{($data['status'] === 'ACTIVE') ? 'success' : (($data['status'] === 'INITIAL') ? 'warning' : 'danger')}}">{{$data['status']}}</span>
                                                    </td>
                                                    @if ($userType === 'REG')
                                                        <td class="text-center action-column">
                                                            <div class="btn-group btn-group-sm">
                                                                <a href="{{ route('viewSubModule', $data['id']) }}"
                                                                   title="View Sub Module"
                                                                   data-action="view" class="btn btn-sm btn-primary"><i
                                                                            class="fa fa-eye"></i> <span
                                                                            class="hide-on-mobile">View</span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Tutor Details</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 tutor-image text-center">
                                                <img class="img-circle elevation-2"
                                                     src="{{ asset('storage/images/'.$moduleData['user']['image'])   }}"
                                                     alt="User Avatar">
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group text-center tutor-rating">
                                                    <label for="bankName">Overall Rating Gained
                                                        <div class="light">(<span
                                                                    id="ratedAmount">{{$overallRating}}</span>/5)
                                                        </div>
                                                    </label>
                                                    <ul class="rating-ul">
                                                        @for($i = 1; $i <= 5; $i++)
                                                            <li class="rating-li {{($i <= $overallRating) ? 'on' : ''}}"
                                                                title="Star 1"><i class="fa fa-star"
                                                                                  aria-hidden="true"></i>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Experience
                                                        <small>(Years)</small>
                                                    </label>
                                                    <input type="text" class="form-control" disabled
                                                           value="{{isset($moduleData['user']['experience']) ? $moduleData['user']['experience'] : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Location</label>
                                                    <input type="text" class="form-control" disabled
                                                           value="{{isset($moduleData['user']['location']) ? $moduleData['user']['location'] : ''}}">

                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="userDescription">About Tutor</label>
                                                    <textarea class="form-control" disabled
                                                              rows="4">{{isset($moduleData['user']['description']) ? $moduleData['user']['description'] : ''}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Module Comments</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group text-center tutor-rating">
                                                    <label for="bankName">Overall Rating for this Module
                                                        <div class="light">(<span
                                                                    id="ratedAmount">{{$ratingByModule['overallRating']}}</span>/5)
                                                        </div>
                                                    </label>
                                                    <ul class="rating-ul">
                                                        @for($i = 1; $i <= 5; $i++)
                                                            <li class="rating-li {{($i <= $ratingByModule['overallRating']) ? 'on' : ''}}"
                                                                title="Star 1"><i class="fa fa-star"
                                                                                  aria-hidden="true"></i>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-12">
                                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        @foreach($ratingByModule['ratingDetails'] as $key => $comment)
                                                            <div class="item carousel-item {{($key === 0) ? 'active' : ''}}">
                                                                <p class="testimonial">
                                                                    <i class="fa fa-quote-left" aria-hidden="true"></i>
                                                                    {{$comment['comment']}}
                                                                    <i class="fa fa-quote-right" aria-hidden="true"></i>
                                                                </p>
                                                                <div class="star-rating"></div>

                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection