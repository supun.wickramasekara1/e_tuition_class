<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="scheduleTable" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Schedule Date</th>
                        <th>Module Topic</th>
                        <th>Module Type</th>
                        <th>Action(s)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tableData as $module)
                        @if($module['type'] == 'ONLINE')
                            @foreach($module['schedule_details'] as $subModule)
                                <tr>
                                    <td>{{$subModule['scheduled_at']}}</td>
                                    <td>{{$module['topic']}}</td>
                                    <td>{{$module['type']}}</td>
                                    <td class="text-center"><a href="{{route('subModuleView')}}"
                                                               class="btn btn-sm btn-primary">View</a></td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>