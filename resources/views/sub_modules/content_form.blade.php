<div class="row">
    <div class="col-12">
        <div class="card">
            <form class="content-upload" method="POST" action="{{ route('contentsUpload') }}"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Upload Video(s)
                                    <small> (Only allowed Mp4, Wmv and Webm files)</small>
                                </label>
                                <div class="input-group">
                                    <div class="custom-file @error('videos') is-invalid @enderror">
                                        <input type="file" name="videos[]" id="videoFile" maxlength="3" max="3" multiple
                                               class="custom-file-input @error('videos') is-invalid @enderror" {{($counts['vCount'] >= 3) ? 'disabled' : ''}}>
                                        <label class="custom-file-label" for="videoFile">Choose video(s) (Maximum 3
                                            videos)</label>
                                    </div>
                                </div>
                                @if ($counts['vCount'] >= 3)
                                    <code>You already uploaded maximum videos count for this module.</code>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Upload Document(s)
                                    <small> (Only allowed Pdf, Word, Powerpoint and Zip files)</small>
                                </label>
                                <div class="input-group">
                                    <div class="custom-file @error('docs') is-invalid @enderror">
                                        <input type="file" name="docs[]" id="docFile" maxlength="3" max="3" multiple
                                               class="custom-file-input @error('docs') is-invalid @enderror" {{($counts['dCount'] >= 3) ? 'disabled' : ''}}>
                                        <label class="custom-file-label" for="docFile">Choose document(s) (Maximum 3
                                            documents)</label>
                                    </div>
                                </div>
                                @if ($counts['dCount'] >= 3)
                                    <code>You already uploaded maximum documents count for this module.</code>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Upload Assignment(s)
                                    <small> (Only allowed Pdf, Word and Zip files)</small>
                                </label>
                                <div class="input-group">
                                    <div class="custom-file @error('assignments') is-invalid @enderror">
                                        <input type="file" name="assignments[]" id="assignmentFile" maxlength="3"
                                               max="3" multiple
                                               class="custom-file-input @error('assignments') is-invalid @enderror" {{($counts['aCount'] >= 3) ? 'disabled' : ''}}>
                                        <label class="custom-file-label" for="assignmentFile">Choose assignment(s)
                                            (Maximum 3
                                            assignments)</label>
                                    </div>
                                </div>
                                @if ($counts['aCount'] >= 3)
                                    <code>You already uploaded maximum assignments count for this module.</code>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{isset($id) ? $id : ''}}">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Upload</button>
                    <button type="reset" class="btn btn-default float-right">Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>