<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Added Contents</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="contentTable" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Topic</th>
                        <th>Content name</th>
                        <th>Content type</th>
                        <th>Action(s)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($tableData['module_contents']))
                        @foreach($tableData['module_contents'] as $data)
                            <tr>
                                <td>{{$tableData['sub_topic']}}</td>
                                <td>{{$data['content_name']}}</td>
                                <td class="text-center"><span
                                            class="badge {{($data['content_type'] == 'VIDEO') ? 'bg-info' : (($data['content_type'] == 'DOCUMENT') ? 'bg-success' : 'bg-danger')}}">{{$data['content_type']}}</span>
                                </td>
                                <td class="text-center">
{{--                                    {{dd($tableData)}}--}}
{{--                                    @if (Auth::user()->user_type === 'TUTOR')--}}
                                        <a href="{{ route('moduleContentDelete', $data['id']) }}"
                                           class="btn btn-sm btn-danger delete-content" title="Delete Content">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                        @if ($data['content_type'] == 'ASSIGNMENT')
                                            <a href="{{ route('moduleAssignmentView', $data['id']) }}"
                                               class="btn btn-sm btn-warning view-assignment"
                                               title="Review Answers"><i class="fa fa-comments"
                                                                         aria-hidden="true"></i> Review
                                            </a>
                                            <button class="btn  btn-sm btn-success upload-mark-sheet"
                                                    data-id="{{$data['id']}}"
                                                    title="Upload Answer Sheet">
                                                <i class="fa fa-upload"
                                                   aria-hidden="true"></i> Answers
                                            </button>
                                        @endif
                                    {{--@else--}}
                                    {{--<a href="{{ route('downloadContent', ['ANSWER', $data['id']]) }}"--}}
                                    {{--class="btn btn-sm btn-danger">Download--}}
                                    {{--</a>--}}
                                    {{--@endif--}}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="markSheetModal" tabindex="-1" role="dialog" aria-labelledby="markSheetLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="markSheetLabel">Upload Mark Sheet</h5>
                <button type="button" class="close popup-close-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="markSheetForm" method="POST" action="{{ route('markSheetUpload') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="modContent" name="mod_content">
                        {{--<input type="hidden" id="moduleContentId" name="module_content_id">--}}
                        <div class="col-12">
                            <div class="form-group">
                                <label for="markSheet">Your Mark Sheet</label>
                                <div class="input-group">
                                    <div class="custom-file @error('answer_sheet') is-invalid @enderror">
                                        <input type="file" name="answer_sheet" id="markSheet"
                                               class="custom-file-input @error('answer_sheet') is-invalid @enderror">
                                        <label class="custom-file-label" for="asdAnsSheet">Choose File</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitMarkSheet" class="btn btn-primary">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>