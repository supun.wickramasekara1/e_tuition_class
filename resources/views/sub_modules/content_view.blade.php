@extends('layouts.layout')
@section('title')
    Assignment View
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Assignment View</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content sub-module-view">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="manual-msg"></div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Assignment details for - {{$moduleData['sub_topic']}}</h3>
                        </div>
                        <div class="card-body">
                            <table id="contentTable" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>Student Name</th>
                                    <th>Assignment Name</th>
                                    <th>Student Answer File Name</th>
                                    <th>Marks</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($tableData))
                                    @foreach($tableData as $data)
                                        <tr>
                                            <td>{{$data['user']['name']}}</td>
                                            <td>{{$data['module_content']['content_name']}}</td>
                                            <td>{{$data['uploaded_file']}}</td>
                                            <td class="text-center">
                                                <form class="add-mark-form" method="POST">
                                                    {{ csrf_field() }}
                                                    <div class="mark-form flex-container assignment-tbl">
                                                        <input type="hidden" name="id" class="form-control"
                                                               value="{{$data['id']}}">
                                                        <input type="hidden" name="module_content_id"
                                                               class="form-control"
                                                               value="{{$data['module_content_id']}}">
                                                        <input min="0" max="100" maxlength=3 type="numb er"
                                                               value="{{$data['mark']}}" name="std_mark"
                                                               class="form-control std_mark table-input">
                                                        <button class="btn btn-sm btn-success submit-mark">
                                                            Save
                                                        </button>
                                                    </div>
                                                    <div class="updated-mark"></div>
                                                </form>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('downloadAnswer', $data['id']) }}"
                                                   class="btn btn-sm btn-warning" title="Download student answers">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection