@extends('layouts.layout')
@section('title')
    Add Sub Module Contents
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 class="m-0">Sub Module Contents - <small>(Scheduled at {{$tableData['scheduled_at']}})</small></h1>
                </div><!-- /.col -->
                {{--<div class="col-sm-6 text-right">--}}
                    {{--<div class="custom-back-btn">--}}
                        {{--<a href="{{ route('subModuleView') }}" type="button" class="btn btn-warning btn-block"><i class="fas fa-angle-double-left"></i> Back--}}
                        {{--<a href="{{ url()->previous() }}" type="button" class="btn btn-warning btn-block"><i class="fas fa-angle-double-left"></i> Back--}}
                            {{--to Sub Module--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content module-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fas fa-video"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Video Counts <b>({{$counts['vCount']}}/3)</b></span>
                                    <span class="info-box-number">
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-info progress-bar-striped" role="progressbar"
                                                 aria-valuenow="{{($counts['vCount'] / 3) * 100}}" aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 style="width: {{($counts['vCount'] / 3) * 100}}%">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Document Counts <b>({{$counts['dCount']}}
                                            /3)</b></span>
                                    <span class="info-box-number">
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-success progress-bar-striped" role="progressbar"
                                                 aria-valuenow="{{($counts['dCount'] / 3) * 100}}" aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 style="width: {{($counts['dCount'] / 3) * 100}}%">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-danger"><i class="far fa-star"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Assignment Counts <b>({{$counts['aCount']}}
                                            /3)</b></span>
                                    <span class="info-box-number">
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-danger progress-bar-striped" role="progressbar"
                                                 aria-valuenow="{{($counts['aCount'] / 3) * 100}}" aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 style="width: {{($counts['aCount'] / 3) * 100}}%">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                </div>

                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    {{--<div class="manual-msg"></div>--}}
                </div>
                <div class="col-12 form-container" id="subModuleContentForm">
                    @include('sub_modules.content_form')
                </div>
                <div class="col-12 table-container" id="moduleTableContainer">
                    @include('sub_modules.content_table', ['tableData' => $tableData])
                </div>
            </div>
        </div>
    </div>
@endsection