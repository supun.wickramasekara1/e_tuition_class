<div class="row">
    <div class="col-12">
        <div class="card">
            {{--<div class="card-body">--}}
            <form method="POST" id="formSubModule">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group required">
                                {{--<label>Module <small class="text-danger">(Select module first to load all sub modules in below table.)</small></label>--}}
                                <label>Select Module</label>
{{--                                {{isset($subModuleData['module_id']) ? 'Update' : 'Create'}}--}}
                                <select id="module_id" name="module_id" data-live-search="true" title="Select a Module"
                                        class="form-control selectpicker custom-select @error('module_id') is-invalid @enderror">
                                    @if (isset($modules))
                                        @if (old('module_id'))
                                            @foreach($modules as $module)
                                                @if (old('module_id') == $module['id'])
                                                    <option selected class="{{($module['status'] == 'PUBLISHED') ? 'published' : ''}}"
                                                            value={{$module['id']}}>{{$module['topic']}} {{($module['status'] == 'PUBLISHED') ? ' - Published' : ''}}</option>
                                                @else
                                                    <option class="{{($module['status'] == 'PUBLISHED') ? 'published' : ''}}" value={{$module['id']}}>{{$module['topic']}} {{($module['status'] == 'PUBLISHED') ? ' - Published' : ''}}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach($modules as $module)
                                                @if ((isset($subModuleData['module_id'])) && ($subModuleData['module_id'] == $module['id']))
                                                    <option selected class="{{($module['status'] == 'PUBLISHED') ? 'published' : ''}}"
                                                            value={{$module['id']}}>{{$module['topic']}} {{($module['status'] == 'PUBLISHED') ? ' - Published' : ''}}</option>
                                                @else
                                                    <option class="{{($module['status'] == 'PUBLISHED') ? 'published' : ''}}" value={{$module['id']}}>{{$module['topic']}} {{($module['status'] == 'PUBLISHED') ? ' - Published' : ''}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                </select>
                                <span class="invalid-feedback text-bold" role="alert"></span>
                            </div>
                        </div>
                        <div class="sub-module-form-part col-12" id="subModuleFormPart">
{{--                            @include('sub_modules/form_part')--}}
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" id="btnSubModule" class="btn btn-primary">Update</button>
                    <button type="reset" class="btn btn-default float-right">Reset</button>
                </div>
            </form>
            {{--</div>--}}
        </div>
    </div>
</div>