<div class="row">
    <div class="col-md-6">
        <div class="form-group required">
            <label>Schedule</label>
            <select id="sub_module_id" name="sub_module_id" data-live-search="true" title="Select a Schedule"
                    class="form-control selectpicker custom-select @error('sub_module_id') is-invalid @enderror">
                @if (isset($subModules))
                    @if ((isset($subModuleData['id'])) && ($subModuleData['id'] == $subModules['id']))
                        <option selected
                                value={{$subModules['id']}}>{{$subModules['scheduled_at']}}
                            - {{$subModules['sub_topic']}}</option>
                    @else
                        <option value={{$subModules['id']}}>{{$subModules['scheduled_at']}}
                            - {{$subModules['sub_topic']}}</option>
                    @endif
                @endif
            </select>
            <span class="invalid-feedback text-bold" role="alert"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group required">
            <label>Sub Topic</label>
            <input type="text" name="sub_topic" id="sub_topic" placeholder="Sub Topic"
                   class="form-control @error('sub_topic') is-invalid @enderror"
                   value="{{(old('sub_topic')) ? old('sub_topic') : (isset($subModuleData['sub_topic']) ? $subModuleData['sub_topic'] : '')}}">
            <span class="invalid-feedback text-bold" role="alert"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group required">
            <label for="moduleUrl">Course URL
                <small> (Teams, Zoom and Youtube etc...)</small>
            </label>
            <input type="text" id="module_url" name="module_url" placeholder="Module URL"
                   class="form-control @error('module_url') is-invalid @enderror"
                   value="{{(old('module_url')) ? old('module_url') : (isset($subModuleData['url']) ? $subModuleData['url'] : '')}}">
            <span class="invalid-feedback text-bold" role="alert"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group required">
            <label for="subModuleContent">Content
                <small>(Min 200 characters)</small>
            </label>
            <textarea id="content" name="content" class="form-control @error('content') is-invalid @enderror"
                      rows="4">{{(old('content')) ? old('content') : (isset($subModuleData['content']) ? $subModuleData['content'] : '')}}</textarea>
            <span class="invalid-feedback text-bold" role="alert"></span>
        </div>
    </div>
</div>