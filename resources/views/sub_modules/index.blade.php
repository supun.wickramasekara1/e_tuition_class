@extends('layouts.layout')
@section('title')
    Create Sub Module
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create Sub Module</h1>
                </div><!-- /.col -->
                {{--<div class="col-sm-6">--}}
                {{--<ol class="breadcrumb float-sm-right">--}}
                {{--<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">eTuition Class</a></li>--}}
                {{--<li class="breadcrumb-item active">Create Module</li>--}}
                {{--</ol>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="manual-msg"></div>
                </div>
                <div class="col-12 form-container" id="subModuleForm">
                    @include('sub_modules.form')
                </div>
                <div class="col-12 table-container" id="subModuleTableContainer">
                    @include('sub_modules.table', ['tableData' => $tableData])
                </div>
            </div>
        </div>
    </div>
@endsection