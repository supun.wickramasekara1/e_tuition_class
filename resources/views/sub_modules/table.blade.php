<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Added Sub Modules</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="subModuleTable" class="table table-bordered table-striped" width="100%">
                    <thead>
                    {{--{{dd($tableData)}}--}}
                    <tr>
                        <th>Schedule Date</th>
                        <th>Topic</th>
                        <th>Video Count</th>
                        <th>Document Count</th>
                        <th>Assignment Count</th>
                        <th>URL</th>
                        <th class="text-center">Status</th>
                        <th width="225px">Action(s)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($tableData))
                        @foreach($tableData as $data)
                            <tr>
                                <td>{{$data['scheduled_at']}}</td>
                                <td>{{$data['sub_topic']}}</td>
                                {{--                                <td>{{$data['video_count']}}</td>--}}
                                <td class="text-center">
                                    <span>{{$data['counts']['vCount']}}/3</span>
                                    <span>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-info"
                                                 style="width: {{($data['counts']['vCount']/3)*100}}%"></div>
                                        </div>
                                    </span>
                                </td>
                                <td class="text-center">
                                    <span>{{$data['counts']['dCount']}}/3</span>
                                    <span>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-success"
                                                 style="width: {{($data['counts']['dCount']/3)*100}}%"></div>
                                        </div>
                                    </span>
                                </td>
                                <td class="text-center">
                                    <span>{{$data['counts']['aCount']}}/3</span>
                                    <span>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-danger"
                                                 style="width: {{($data['counts']['aCount']/3)*100}}%"></div>
                                        </div>
                                    </span>
                                </td>
                                <td>{{$data['url']}}</td>
                                <td class="text-center">
                                    <span class="badge bg-{{($data['status'] === 'ACTIVE') ? 'success' : (($data['status'] === 'INITIAL') ? 'warning' : 'danger')}}">{{$data['status']}}</span>
                                </td>
                                <td class="text-center action-column">
                                    <div class="btn-group btn-group-sm">
                                        @if ($data['module']['status'] === 'UNPUBLISHED')
                                            <button data-id="{{$data['id']}}" data-action="edit"
                                                    class="btn btn-sm btn-success edit-sub-module">Edit
                                            </button>
                                            <a href="{{  route('moduleContentView', $data['id']) }}"
                                               title="Add Contents" class="btn btn-sm btn-default add-content"><i
                                                        class="fa fa-plus"></i> <span
                                                        class="hide-on-mobile">Add Contents</span></a>
                                            {{--@if ($data['status'] === 'ACTIVE')--}}
                                                {{--<button data-id="{{$data['id']}}" data-action="cancel"--}}
                                                        {{--class="btn btn-sm btn-warning cancel-sub-module">Cancel--}}
                                                {{--</button>--}}
                                            {{--@endif--}}
                                        @else
                                            @if ($data['status'] === 'INITIAL'  || $data['status'] === 'ACTIVE')
                                                <button data-id="{{$data['id']}}" data-action="edit"
                                                        class="btn btn-sm btn-success edit-sub-module">Edit
                                                </button>
                                                <a href="{{  route('moduleContentView', $data['id']) }}"
                                                   title="Add Contents" class="btn btn-sm btn-default add-content"><i
                                                            class="fa fa-plus"></i> <span
                                                            class="hide-on-mobile">Add Contents</span></a>
                                                <button data-id="{{$data['id']}}" data-action="cancel"
                                                        class="btn btn-sm btn-warning cancel-sub-module">Cancel
                                                </button>
                                            @else
                                                <a href="{{ route('viewSubModule', $data['id']) }}"
                                                   data-action="view" class="btn btn-sm btn-primary"><i
                                                            class="fa fa-eye"></i> <span
                                                            class="hide-on-mobile">View</span>
                                                </a>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>