@extends('layouts.layout')
@section('title')
    Sub Module View
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Sub Module View</h1>
                </div><!-- /.col -->
                {{--<div class="col-sm-6 text-right">--}}
                {{--<div class="custom-back-btn">--}}
                {{--<a href="{{ route('viewSubModule', $subModuleData['id']) }}" type="button"--}}
                {{--class="btn btn-warning btn-block"><i--}}
                {{--class="fas fa-angle-double-left"></i> Back--}}
                {{--to Sub Module--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                {{--<ol class="breadcrumb float-sm-right">--}}
                {{--<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">eTuition Class</a></li>--}}
                {{--<li class="breadcrumb-item active">Subscribed Data</li>--}}
                {{--</ol>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content sub-module-view">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="manual-msg"></div>
                    @if ($subModuleData['status'] == 'CANCELED' && Auth::user()->user_type === 'STUDENT')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            This sub module is canceled by Tutor.
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Module details</h3>
                            @if (auth()->user()->user_type !== 'STUDENT')
                                <span class="badge badge-{{($subModuleData['status'] == 'ACTIVE') ? 'success' : (($subModuleData['status'] == 'INITIAL') ? 'warning' : 'danger')}} module-status">{{$subModuleData['status']}}</span>
                            @endif
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Module</label>
                                                <input type="text" name="topic" id="moduleTopic" placeholder="Topic"
                                                       class="form-control" disabled
                                                       value="{{isset($subModuleData['module']['topic']) ? $subModuleData['module']['topic'] : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label>Schedule</label>
                                                <input type="text" name="topic" id="moduleTopic" placeholder="Topic"
                                                       class="form-control" disabled
                                                       value="{{isset($subModuleData['scheduled_at']) ? $subModuleData['scheduled_at'] .' - '. $subModuleData['sub_topic'] : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label>Sub Topic</label>
                                                <input type="text" name="sub_topic" id="sub_topic" disabled
                                                       placeholder="Sub Topic" class="form-control"
                                                       value="{{isset($subModuleData['sub_topic']) ? $subModuleData['sub_topic'] : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label for="moduleUrl">Course URL
                                                    <small> (Teams, Zoom and Youtube etc...)</small>
                                                </label>
                                                <a class="external-url" target="_blank"
                                                   href="//{{isset($subModuleData['url']) ? $subModuleData['url'] : ''}}">
                                                    <input type="text" id="module_url" name="module_url" disabled
                                                           placeholder="Module URL" class="form-control module-url"
                                                           title="Click here go to {{isset($subModuleData['url']) ? $subModuleData['url'] : ''}}"
                                                           value="{{isset($subModuleData['url']) ? $subModuleData['url'] : ''}}"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label for="subModuleContent">Content
                                                    <small>(Min 200 characters)</small>
                                                </label>
                                                <textarea id="content" name="content" class="form-control" disabled
                                                          rows="4">{{isset($subModuleData['content']) ? $subModuleData['content'] : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Module Contents</h3>
                            @if (Auth::user()->user_type === 'TUTOR' && in_array($subModuleData['status'], ['INITIAL', 'ACTIVE']))
                                <a href="{{  route('moduleContentView', $subModuleData['id']) }}"
                                   title="Add Contents" class="btn btn-sm btn-warning add-content"><i
                                            class="fa fa-plus"></i> <span
                                            class="hide-on-mobile">Add Contents</span></a>
                            @endif
                        </div>
                        <div class="card-body">
                            <table id="contentTable" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>Topic</th>
                                    <th>Content name</th>
                                    <th>Content type</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($subModuleData['module_contents']))
                                    @foreach($subModuleData['module_contents'] as $data)
                                        <tr>
                                            <td>{{$subModuleData['sub_topic']}}</td>
                                            <td>{{$data['content_name']}}</td>
                                            <td class="{{($data['content_type'] !== 'ASSIGNMENT') ? 'text-center' : 'manual-align'}}">
                                                <span class="content-status badge {{($data['content_type'] == 'VIDEO') ? 'bg-info' : (($data['content_type'] == 'DOCUMENT') ? 'bg-success' : 'bg-danger')}}">{{$data['content_type']}}</span>
                                                @if ($data['content_type'] === 'ASSIGNMENT')
                                                    @if ($data['answer_sheet'])<span class="status-icons up-down"><i
                                                                class="fa fa-file-download"
                                                                title="Tutor have uploaded answers sheet"></i></span>@endif
                                                    @if ($data['assignment'])
                                                        <span class="status-icons up-down"
                                                              title="You have uploaded answers"><i
                                                                    class="fa fa-file-upload"></i></span>
                                                        @if ($data['assignment']->mark)
                                                            <span class="status-icons marks"
                                                                  title="This is your marks">{{$data['assignment']->mark}}</span>
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if (Auth::user()->user_type === 'TUTOR')
                                                    <a href="{{ route('moduleContentDelete', $data['id']) }}"
                                                       class="btn btn-sm btn-danger delete-content"
                                                       title="Delete Content"><i
                                                                class="fa fa-remove"></i> Delete
                                                    </a>
                                                @else
                                                    <a href="{{ route('downloadContent', $data['id']) }}"
                                                       class="btn btn-sm btn-warning" title="Download">Download
                                                    </a>
{{--                                                    {{dd($subModuleData)}}--}}
                                                    @if ($data['content_type'] == 'ASSIGNMENT')
                                                        {{--{{dd($data)}}--}}
                                                        {{--{{dd((!(isset($data['assignment']->mark) && $data['assignment']->mark)) ? 'dddddddddd' : 'gggggggggg')}}--}}
                                                        @if (!$data['assignment'] || ($data['assignment']&& !$data['assignment']->is_review))
                                                            <button class="btn  btn-sm btn-success upload-answers"
                                                                    data-id="{{$data['id']}}"
                                                                    data-smid="{{$subModuleData['id']}}"
                                                                    title="Upload your answers">
                                                                <i class="fa fa-upload"></i> Answers
                                                            </button>
                                                        @endif
                                                        @if ($data['answer_sheet'])
                                                            <a href="{{ route('downloadMark', $data['id'])}}"
                                                               class="btn btn-sm btn-primary"
                                                               title="Download answer sheet">
                                                                <i class="fa fa-download"></i> Download
                                                            </a>
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="asdAnsModal" tabindex="-1" role="dialog" aria-labelledby="asdAnsLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="asdAnsLabel">Upload Answer Sheet</h5>
                    <button type="button" class="close popup-close-btn" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="asdAnsForm" method="POST" action="{{ route('asdAnsUpload') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" id="subModId" name="sub_mod_id">
                            <input type="hidden" id="moduleContentId" name="module_content_id">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="bankBranch">Your Answer Sheet</label>
                                    <div class="input-group">
                                        <div class="custom-file @error('uploaded_file') is-invalid @enderror">
                                            <input type="file" name="uploaded_file" id="asdAnsSheet"
                                                   class="custom-file-input @error('uploaded_file') is-invalid @enderror">
                                            <label class="custom-file-label" for="asdAnsSheet">Choose File</label>
                                        </div>
                                    </div>
                                    <small class="text-warning">Your tutor review this and will update your marks
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="submitAnswer" class="btn btn-primary">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection