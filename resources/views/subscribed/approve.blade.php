@extends('layouts.layout')
@section('title')
    Subscribed Details
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Approve Subscription</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="approveSubsTable" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>Module Topic</th>
                                    <th>Subject</th>
                                    {{--<th>Payment Method</th>--}}
                                    <th>Payment Status</th>
                                    <th>Bank Name</th>
                                    <th>Bank Branch</th>
                                    <th>Bank Account</th>
                                    <th>{{(Auth::user()->user_type == 'TUTOR') ? 'Student Name' : 'Tutor Name'}}</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($tableData))
                                    @foreach($tableData as $key => $data)
                                        <tr>
                                            <td>{{$data['module']['topic']}}</td>
                                            <td>{{$data['subject']['name']}}</td>
                                            {{--                                            <td>{{$data['type']}}</td>--}}
                                            <td>{{$data['status']}}</td>
                                            <td>{{$data['bank_name']}}</td>
                                            <td>{{$data['bank_branch']}}</td>
                                            <td>{{$data['bank_account']}}</td>
                                            <td>{{(Auth::user()->user_type == 'TUTOR') ? $data['user']['name'] : $data['tutor_name']}}</td>
                                            <td>{{\Carbon\Carbon::parse($data['updated_at'])->format('Y-m-d')}}</td>
                                            @if ($data['module']['is_free'])
                                                <td class="text-center"><span
                                                            class="badge badge-warning module-type">Free</span>
                                                </td>
                                            @else
                                                <td class="text-right">{{$data['module']['module_fee']}}</td>
                                            @endif
                                            <td class="text-center action-column">
                                                <div class="btn-group btn-group-sm">
                                                    <a href="{{  route('doApprove', $data['id']) }}"
                                                       title="Approve this subscription" class="btn btn-sm btn-info">
                                                        <span class="hide-on-mobile">Approve</span></a>
                                                    <button type="button" class="btn btn-default" data-toggle="modal"
                                                            data-target="#bankSlip" .{{$key}}>
                                                        View Bank Slip
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- Modal -->
                                        <div class="modal fade" id="bankSlip" .{{$key}}>
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Bank Slip
                                                            of {{$data['user']['name']}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <img width="400px" src="{{asset('storage/bankSlip/'.$data['bank_slip'])}}">
                                                    </div>
                                                    <div class="modal-footer text-right">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>
@endsection