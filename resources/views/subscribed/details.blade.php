@extends('layouts.layout')
@section('title')
    Subscribed Details
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Subscribed Details</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                </div>
                <div class="col-12 form-container">
                    @include('subscribed.form', ['modules' => $modules])
                </div>
                <div id="subscribedTableContainer" class="col-12 table-container">
                    @include('subscribed.table', ['tableData' => $tableData])
                </div>
            </div>
        </div>
    </div>
@endsection