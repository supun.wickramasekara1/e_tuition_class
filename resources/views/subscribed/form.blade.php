<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form>
                    <div class="row">
                        @if (Auth()->user()->user_type === 'TUTOR')
                            <div class="form-group col-6">
                                <label>Student Name:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="filter_val" id="filterVal">
                                </div>
                            </div>
                        @else
                            <div class="form-group col-6">
                                <label>Tutor Name:</label>
                                <div class="input-group">
                                    {{--<input type="text" class="form-control" name="filter_name" id="tutorName">--}}
                                    <select id="filterVal" name="filter_val" data-live-search="true"
                                            class="form-control selectpicker custom-select">
                                        <option value=0>All</option>
                                        @if (isset($modules))
                                            @foreach($modules as $module)
                                                <option value={{$module['user']['id']}}>{{$module['user']['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="form-group col-6">
                            <label>Select Date Range:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar"></i></span>
                                </div>
                                <input type="text" name="date_range" class="form-control float-right"
                                       id="dateRange">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-info" id="btnFilter">Filter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>