@extends('layouts.layout')
@section('title')
    Subscribe Module
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1 class="m-0">Payment Details</h1>
                </div><!-- /.col -->
                {{--<div class="col-sm-6 text-right">--}}
                {{--<div class="custom-back-btn text-right">--}}
                {{--<a href="{{ route('classBoard') }}" type="button" class="btn btn-warning btn-block"><i class="fas fa-angle-double-left"></i> Back--}}
                {{--to Class Board--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content payment-method-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-5">
                                    <h3 class="card-title">Add Payment Details</h3>
                                </div>
                                <div class="col-7 text-right clearfix">
                                    <div class="col-12 no-padding">
                                        <div class="icheck-success d-inline">
                                            <input type="radio" class="payment-type" value="MANUAL"
                                                   id="radioPrimary2"
                                                   name="payment_type" {{(old('payment_type') == 'MANUAL') ? 'checked' : 'checked'}}>
                                            <label for="radioPrimary2">
                                                MANUAL
                                            </label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" class="payment-type" value="PAYPAL"
                                                   id="radioPrimary1" name="payment_type"
                                                    {{(old('payment_type') == 'PAYPAL') ? 'checked' : ''}}>
                                            <label for="radioPrimary1">
                                                PAYHERE
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {{--<div class="col-md-12 text-center">--}}
                            {{--<div class="form-group clearfix">--}}
                            {{--<label for="userName">Payment Method</label>--}}
                            {{--<div class="col-12 no-padding">--}}
                            {{--<div class="icheck-success d-inline">--}}
                            {{--<input type="radio" class="payment-type" value="MANUAL"--}}
                            {{--id="radioPrimary2"--}}
                            {{--name="payment_type" {{(old('payment_type') == 'MANUAL') ? 'checked' : 'checked'}}>--}}
                            {{--<label for="radioPrimary2">--}}
                            {{--MANUAL--}}
                            {{--</label>--}}
                            {{--</div>--}}
                            {{--<div class="icheck-primary d-inline">--}}
                            {{--<input type="radio" class="payment-type" value="PAYPAL"--}}
                            {{--id="radioPrimary1" name="payment_type"--}}
                            {{--{{(old('payment_type') == 'PAYPAL') ? 'checked' : ''}}>--}}
                            {{--<label for="radioPrimary1">--}}
                            {{--PAYHERE--}}
                            {{--</label>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-12 no-padding manual-container" id="manualContainer">
                                <div class="col-12 no-padding">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Tutor Payment Details</h3>

                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="tModuleName">Module Name</label>
                                                        <input type="text" id="tModuleName" class="form-control" disabled
                                                               value="{{$moduleData['topic']}}">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="tModuleFee">Module Fee</label>
                                                        <input type="text" id="tModuleFee" class="form-control" disabled
                                                               value="{{$moduleData['module_fee']}}">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="bankName">Bank Name</label>
                                                        <input type="text" id="bankName" class="form-control" disabled
                                                               value="{{$moduleData['user']['bank_name']}}">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="bankBranch">Bank Branch</label>
                                                        <input type="text" id="bankBranch" class="form-control" disabled
                                                               value="{{$moduleData['user']['bank_branch']}}">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="swiftCode">Bank Swift Code</label>
                                                        <input type="text" id="swiftCode" class="form-control" disabled
                                                               value="{{$moduleData['user']['swift_code']}}">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="accNo">Bank Account</label>
                                                        <input type="text" id="accNo" class="form-control" disabled
                                                               value="{{$moduleData['user']['account_no']}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                {{ dd($moduleData) }}--}}
                                <form method="POST" action="{{ route('addPayment') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label>Your Bank Name</label>
                                                <input type="text" name="bank_name" id="bankName"
                                                       placeholder="Bank Name"
                                                       class="form-control @error('bank_name') is-invalid @enderror"
                                                       value="{{(old('bank_name')) ? old('bank_name') : ''}}">
                                                @error('bank_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label>Your Bank Branch</label>
                                                <input type="text" name="bank_branch" id="bankBranch"
                                                       placeholder="Bank Branch"
                                                       class="form-control @error('bank_branch') is-invalid @enderror"
                                                       value="{{(old('bank_branch')) ? old('bank_branch') : ''}}">
                                                @error('bank_branch')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label>Your Bank Account</label>
                                                <input type="text" name="bank_account" id="bankAccount"
                                                       placeholder="Bank Account"
                                                       class="form-control @error('bank_account') is-invalid @enderror"
                                                       value="{{(old('bank_account')) ? old('bank_account') : ''}}">
                                                @error('bank_account')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label for="userProfilePic">Your Bank Slip</label>
                                                <div class="input-group">
                                                    <div class="custom-file @error('bank_slip') is-invalid @enderror">
                                                        <input type="file" name="bank_slip" id="bankSlip"
                                                               class="custom-file-input @error('bank_slip') is-invalid @enderror"
                                                               value="{{ asset('storage/images/'.(Auth::user()->image)) }}">
                                                        <label class="custom-file-label" for="bankSlip">Choose
                                                            picture</label>
                                                    </div>
                                                    @error('bank_slip')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="module_id" value="{{isset($moduleData['id']) ? $moduleData['id'] : ''}}">
                                    </div>
                                    <div class="col-12 no-padding text-right manual-container">
                                        <button type="submit" class="btn btn-primary">Proceed Payment</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-12 no-padding payhere-container" id="paypalContainer">
                                <form method="POST" action="https://sandbox.payhere.lk/pay/checkout">
                                    {{--<input type="hidden" name="merchant_id" value="1217729">--}}
                                    <input type="hidden" name="merchant_id" value="{{ env('MERCHANT_ID') }}">
                                    <!-- Replace your Merchant ID -->
                                    <input type="hidden" name="return_url"
                                           value="{{ env('PAYHERE_RETURN') }}">
                                    <input type="hidden" name="cancel_url"
                                           value="{{ env('PAYHERE_CANCEL') }}">
                                    <input type="hidden" name="notify_url"
                                           value="{{ env('PAYHERE_NOTIFY') }}">
                                    <div class="row">
                                        {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Invoice No</label>--}}
                                        {{--<input type="hidden" type="text" name="order_id" readonly--}}
                                        {{--class="form-control" value="{{$invId}}">--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        <input type="hidden" name="order_id" readonly
                                               class="form-control" value="{{$invId}}">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Module</label>
                                                <input type="text" name="items"
                                                       class="form-control" readonly
                                                       value="Module name - {{$moduleData['topic']}}">
                                            </div>
                                        </div>
                                        <input type="hidden" name="currency" value="LKR">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Module Fee</label>
                                                <input type="text" name="amount"
                                                       class="form-control" readonly
                                                       value="{{$moduleData['module_fee']}}">
                                            </div>
                                        </div>
                                        {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Module Fee</label>--}}
                                        <input type="hidden" name="first_name"
                                               class="form-control" readonly
                                               value="{{Auth::user()->name}}">
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        <input type="hidden" name="last_name" value="{{Auth::user()->name}}">
                                        <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                        <input type="hidden" name="phone" value="{{Auth::user()->email}}">
                                        <input type="hidden" name="address" value="No Address">
                                        <input type="hidden" name="city" value="{{Auth::user()->name}}">
                                        <input type="hidden" name="country" value="Sri Lanka">
                                    </div>
                                    {{--<button type="submit" class="btn btn-outline-info"><i class="fab fa-paypal"></i>--}}
                                    {{--Proceed Payment with PAYPAL--}}
                                    {{--</button>--}}
                                    {{--<button class="btn btn-outline-light btn-payhere" type="submit"></button>--}}
                                    <button class="btn btn-outline-info pull-right btn-payhere" type="submit">Pay with
                                        <span
                                                class="first">Pay</span><span class="second">Here</span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
