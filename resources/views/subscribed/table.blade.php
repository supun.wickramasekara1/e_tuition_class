<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="subscribedTable" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->user_type == 'ADMIN')
                            <th>Tutor Name</th>
                        @else
                            <th>Module Topic</th>
                        @endif
                        <th>Subject</th>
                        <th>Payment Method</th>
                        <th>Payment Status</th>
                        @if(Auth::user()->user_type !== 'ADMIN')
                            <th>{{(Auth::user()->user_type == 'TUTOR') ? 'Student Name' : 'Tutor Name'}}</th>
                        @endif
                        <th>Date</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($tableData))
                        @foreach($tableData as $data)
                            <tr>
                                @if(Auth::user()->user_type == 'ADMIN')
                                    <td>{{$data['tutor_name']}}</td>
                                @else
                                    <td>{{$data['module']['topic']}}</td>
                                @endif
                                <td>{{$data['module']['topic']}}</td>
                                <td>{{$data['type']}}</td>
                                <td>{{$data['status']}}</td>
                                @if(Auth::user()->user_type !== 'ADMIN')
                                    <td>{{(Auth::user()->user_type == 'TUTOR') ? $data['user']['name'] : $data['tutor_name']}}</td>
                                @endif
                                <td>{{\Carbon\Carbon::parse($data['updated_at'])->format('Y-m-d')}}</td>
                                @if ($data['module']['is_free'])
                                    <td class="text-center"><span class="badge badge-warning module-type">Free</span>
                                    </td>
                                @else
                                    <td class="text-right">{{$data['module']['module_fee']}}</td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    {{--<tfoot>--}}
                    {{--<tr>--}}
                    {{--<th colspan="5">--}}
                    {{--<th class="text-right">Total:</th>--}}
                    {{--<th class="text-right">3,000.00</th>--}}
                    {{--</tr>--}}
                    {{--</tfoot>--}}
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>