@extends('layouts.layout')
@section('title')
    User Profile
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">User Profile</h1>
                </div><!-- /.col -->
                {{--<div class="col-sm-6">--}}
                {{--<ol class="breadcrumb float-sm-right">--}}
                {{--<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">eTuition Class</a></li>--}}
                {{--<li class="breadcrumb-item active">User Profile</li>--}}
                {{--</ol>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 form-error-container">
                    @if(session()->has('message'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                </div>
                <div class="col-md-5">
                    <!-- Profile Image -->
                    <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-warning">
                            <div class="widget-user-image">
                                <img class="img-circle elevation-2"
                                     src="{{ (Auth::user()->image) ? asset('storage/images/'.(Auth::user()->image)) : asset('storage/images/default_user.png') }}"
                                     alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">{{Auth::user()->name}}</h3>
                            <h5 class="widget-user-desc">{{Auth::user()->user_type}}</h5>
                        </div>
                        <div class="card-footer p-0">
                            <ul class="nav flex-column">
                                @if (Auth::user()->user_type == 'TUTOR')
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Completed Modules <span
                                                    class="float-right badge bg-info">{{$dashboardCounts['pubCounts']}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Online Modules <span
                                                    class="float-right badge bg-success">{{$dashboardCounts['onCounts']}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Offline Modules <span
                                                    class="float-right badge bg-warning">{{$dashboardCounts['offCounts']}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Subscribed Student <span
                                                    class="float-right badge bg-danger">{{$dashboardCounts['subCounts']}}</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Subscribed Online Modules <span
                                                    class="float-right badge bg-success">{{$dashboardCounts['onCounts']}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Subscribed Offline Modules <span
                                                    class="float-right badge bg-warning">{{$dashboardCounts['offCounts']}}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Change Password</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action=" {{ route('changePassword') }}">
                            @csrf
                            <div class="card-body">
                                <div class="input-group mb-3">
                                    <input type="password"
                                           class="form-control @error('current_password') is-invalid @enderror"
                                           id="userCurrentPwd" name="current_password" placeholder="Current Password"
                                           autocomplete="off">
                                    @error('current_password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input-group mb-3">
                                    <input id="password" type="password" placeholder="Password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           autocomplete="off">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input-group mb-3">
                                    <input type="password" class="  form-control" placeholder="Retype password"
                                           name="password_confirmation" autocomplete="off">
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-warning">Submit</button>
                                <button type="button" class="btn btn-default float-right">Cancel</button>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-7">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">User Details</h3>
                        </div>
                        <form method="POST" action="{{ route('updateUser') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group required">
                                    <label for="userName">Name</label>
                                    <input type="text" name="name" id="userName" placeholder="Name"
                                           class="form-control @error('name') is-invalid @enderror"
                                           value="{{(old('name')) ? old('name') : (isset($userData['name']) ? $userData['name'] : '')}}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group required">
                                    <label for="userUserName">User Name</label>
                                    <input type="text" name="user_name" id="userUserName" placeholder="User name"
                                           class="form-control" disabled="disabled"
                                           value="{{isset($userData['user_name']) ? $userData['user_name'] : ''}}">
                                </div>
                                <div class="form-group required">
                                    <label for="userEmail">Email</label>
                                    <input type="email" name="email" id="userEmail" placeholder="Email"
                                           class="form-control" disabled="disabled"
                                           value="{{isset($userData['email']) ? $userData['email'] : ''}}">
                                </div>
                                @if (Auth::user()->user_type == 'TUTOR')
                                    <div class="form-group required">
                                        <label for="userDescription">About Me</label>
                                        <textarea id="userDescription" name="description"
                                                  class="form-control @error('description') is-invalid @enderror"
                                                  rows="4">{{(old('description')) ? old('description') : (isset($userData['description']) ? $userData['description'] : '')}}</textarea>
                                        @error('description')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @endif
                                <div class="form-group required">
                                    <label for="userProfilePic">Profile Picture</label>
                                    <div class="input-group">
                                        <div class="custom-file @error('image') is-invalid @enderror">
                                            <input type="file" name="image" id="userProfilePic"
                                                   class="custom-file-input @error('image') is-invalid @enderror"
                                                   {{--                                                   value="{{(old('image')) ? old('image') : (isset($userData['image']) ? $userData['image'] : '')}}">--}}
                                                   value="{{ asset('storage/images/'.(Auth::user()->image)) }}">
                                            <label class="custom-file-label" for="userProfilePic">Choose picture (Better
                                                to upload 100*100 image)</label>
                                            <input type="hidden" name="image_hidden"
                                                   value="{{(old('image')) ? old('image') : (isset($userData['image']) ? $userData['image'] : '')}}">
                                        </div>
                                        @error('image')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                @if (Auth::user()->user_type == 'TUTOR')
                                    <div class="form-group required">
                                        <label>Subject(s)</label>
                                        <select name="subject[]" id="userSubjects"
                                                class="form-control select2 @error('subject') is-invalid @enderror"
                                                multiple="multiple" data-placeholder="Select subject(s)"
                                                style="width: 100%;">
                                            @if (isset($subjects))
                                                @if (old('subject'))
                                                    @foreach($subjects as $subject)
                                                        @if (in_array($subject['id'], old('subject')))
                                                            <option selected
                                                                    value={{$subject['id']}}>{{$subject['name']}}</option>
                                                        @else
                                                            <option value={{$subject['id']}}>{{$subject['name']}}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @foreach($subjects as $subject)
                                                        @if ((isset($userData['selected_subjects'])) && in_array($subject['id'], $userData['selected_subjects']))
                                                            <option selected
                                                                    value={{$subject['id']}}>{{$subject['name']}}</option>
                                                        @else
                                                            <option value={{$subject['id']}}>{{$subject['name']}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                        @error('subject')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @endif
                                <div class="row">
                                    @if (Auth::user()->user_type == 'TUTOR')
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group required">
                                                <label for="userExperience">Teaching Experience (Years)</label>
                                                <input type="number" min="0" placeholder="User experience"
                                                       name="experience" id="userExperience"
                                                       class="form-control @error('experience') is-invalid @enderror"
                                                       value="{{(old('experience')) ? old('experience') : (isset($userData['experience']) ? $userData['experience'] : '')}}">
                                                @error('experience')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group required">
                                                <label for="userExperience">Grade</label>
                                                <select id="userType" name="grade_id"
                                                        class="form-control custom-select @error('grade_id') is-invalid @enderror">
                                                    <option selected disabled>Select grade</option>
                                                    @if (isset($grades))
                                                        @if (old('grade_id'))
                                                            @foreach($grades as $grade)
                                                                @if (old('grade_id') == $grade['id'])
                                                                    <option selected
                                                                            value={{$grade['id']}}>{{$grade['name']}}</option>
                                                                @else
                                                                    <option value={{$grade['id']}}>{{$grade['name']}}</option>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @foreach($grades as $grade)
                                                                @if ((isset($userData['grade_id'])) && ($userData['grade_id'] == $grade['id']))
                                                                    <option selected
                                                                            value={{$grade['id']}}>{{$grade['name']}}</option>
                                                                @else
                                                                    <option value={{$grade['id']}}>{{$grade['name']}}</option>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </select>
                                                @error('grade_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-sm-6 col-12">
                                        <div class="form-group required">
                                            <label for="userHomeTown">Home Town</label>
                                            <input type="text" id="userHomeTown" placeholder="Home town" name="location"
                                                   class="form-control @error('location') is-invalid @enderror"
                                                   value="{{(old('location')) ? old('location') : (isset($userData['location']) ? $userData['location'] : '')}}">
                                            @error('location')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @if (Auth::user()->user_type == 'TUTOR')
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card card-default">
                                                <div class="card-header">
                                                    <h3 class="card-title">Bank Details</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12 col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="bankName">Bank Name</label>
                                                                <input type="text" name="bank_name" id="bankName"
                                                                       placeholder="Bank Name"
                                                                       class="form-control @error('bank_name') is-invalid @enderror"
                                                                       value="{{(old('bank_name')) ? old('bank_name') : (isset($userData['bank_name']) ? $userData['bank_name'] : '')}}">
                                                                @error('bank_name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="bankBranch">Bank Branch</label>
                                                                <input type="text" name="bank_branch" id="bankBranch"
                                                                       placeholder="Bank Branch"
                                                                       class="form-control @error('bank_branch') is-invalid @enderror"
                                                                       value="{{(old('bank_branch')) ? old('bank_branch') : (isset($userData['bank_branch']) ? $userData['bank_branch'] : '')}}">
                                                                @error('bank_branch')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="swiftCode">Bank Swift Code</label>
                                                                <input type="text" name="swift_code" id="swiftCode"
                                                                       placeholder="Bank Swift Code"
                                                                       class="form-control @error('swift_code') is-invalid @enderror"
                                                                       value="{{(old('swift_code')) ? old('swift_code') : (isset($userData['swift_code']) ? $userData['swift_code'] : '')}}">
                                                                @error('swift_code')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="accNo">Account Number</label>
                                                                <input type="text" name="account_no" id="accNo"
                                                                       placeholder="Account Number"
                                                                       class="form-control @error('account_no') is-invalid @enderror"
                                                                       value="{{(old('account_no')) ? old('account_no') : (isset($userData['account_no']) ? $userData['account_no'] : '')}}">
                                                                @error('account_no')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <input type="hidden" name="id"
                                       value="{{ (isset($userData['id']) && $userData['id']) ? $userData['id'] : '' }}">
                                <input type="hidden" name="user_type"
                                       value="{{ (isset($userData['user_type']) && $userData['user_type'] == 'TUTOR') ? 'TUTOR' : 'STUDENT' }}">
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection