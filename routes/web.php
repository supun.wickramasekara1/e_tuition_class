<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes(['verify' => true]);


//Route::get('/home', 'UserController@userProfileView')->name('userProfile');

Route::get('/dashboard', 'DashboardController@dashboardView')->name('dashboard');

Route::group(['prefix' => 'user_details'], function () {
    Route::get('/', 'UserController@userProfileView')->name('userProfile');
    Route::post('/change_password', 'UserController@changePassword')->name('changePassword');
    Route::post('/update_user', 'UserController@update')->name('updateUser');
});

Route::group(['prefix' => 'module'], function () {
    Route::get('/', 'ModuleController@moduleView')->name('moduleView');
    Route::post('/add_module', 'ModuleController@addModule')->name('addModule');
    Route::get('/edit/{id}', 'ModuleController@getModuleView');
//    Route::post('/edit_module', 'ModuleController@editModule')->name('editModule');
    Route::get('/delete/{id}', 'ModuleController@deleteModule')->name('deleteModule');
    Route::get('/view/{id}', 'ModuleController@viewModule')->name('viewModule');
//    Route::get('/subscribe/{id}', 'ModuleController@viewSubscribe')->name('viewSubscribe');
    Route::get('/publish/{id}', 'ModuleController@publishModule')->name('publishModule');
    Route::get('/unpublish/{id}', 'ModuleController@unpublishModule')->name('unpublishModule');
    Route::get('/hold/{id}', 'ModuleController@holdModule')->name('holdModule');
//    Route::get('/get_all_schedule', 'ModuleController@getSchedule')->name('getSchedule');
});

Route::group(['prefix' => 'class_board'], function () {
    Route::get('/', 'ModuleController@classBoardView')->name('classBoard');
    Route::post('/filter', 'ModuleController@filterClassBoardView');
    Route::post('/add_review', 'ModuleController@addReview');
});

Route::group(['prefix' => 'subscribe'], function () {
    Route::get('/view/{id}', 'SubscribedController@viewSubscribe')->name('viewSubscribe');
    Route::get('/', 'SubscribedController@subscribedDataView')->name('subscribedData');
    Route::get('/filter/{filterVal}/{dateRange}', 'SubscribedController@filterSubscribedData')->name('filterSubscribedData');

    Route::get('/payment_details/{id}', 'SubscribedController@paymentDetails')->name('paymentDetails');
    Route::post('/addPayment', 'SubscribedController@addPayment')->name('addPayment');
    Route::get('/approve', 'SubscribedController@approveSubscription')->name('approveSubscription');
    Route::get('/do_approve/{id}', 'SubscribedController@doApprove')->name('doApprove');



    Route::get('/return', 'SubscribedController@returnUrl')->name('returnUrl');
    Route::get('/cancel', 'SubscribedController@cancelUrl')->name('cancelUrl');
    Route::post('/notify', 'SubscribedController@notifyUrl')->name('notifyUrl');

//    Route::get('/payment/{id}', 'SubscribedController@payment')->name('payment');
//    Route::get('/cancel', 'SubscribedController@cancel')->name('payment.cancel');
//    Route::get('/success', 'SubscribedController@success')->name('payment.success');
});


Route::group(['prefix' => 'admin_board'], function () {
    Route::get('/', 'SubscribedController@subscribedDataView')->name('adminBoard');
});

Route::group(['prefix' => 'sub_module'], function () {
    Route::get('/', 'SubModuleController@subModuleView')->name('subModuleView');
    Route::get('/view/{id}', 'SubModuleController@viewSubModule')->name('viewSubModule');
    Route::get('/change/module/{id}', 'SubModuleController@schedulesByModule')->name('schedulesByModule');
    Route::post('/add_module', 'SubModuleController@addSubModule')->name('addSubModule');
    Route::get('/edit/{id}', 'SubModuleController@getSubModuleView');
    Route::get('/cancel/{id}', 'SubModuleController@cancelSubModule');
    Route::post('/edit_module', 'SubModuleController@editModule')->name('editSubModule');
    Route::get('/delete_module/{id}', 'SubModuleController@deleteModule')->name('deleteSubModule');
//    Route::get('/get_content/{id}', 'SubModuleController@getContent')->name('getContent');
    Route::get('/content/{id}', 'SubModuleController@moduleContentView')->name('moduleContentView');
    Route::get('/content/download_paper/{id}', 'SubModuleController@downloadContent')->name('downloadContent');
    Route::get('/content/download_mark/{id}', 'SubModuleController@downloadMark')->name('downloadMark');
    Route::get('/content/download_answer/{id}', 'SubModuleController@downloadAnswer')->name('downloadAnswer');
    Route::get('/content/delete/{id}', 'SubModuleController@moduleContentDelete')->name('moduleContentDelete');
    Route::post('/content_upload', 'SubModuleController@contentsUpload')->name('contentsUpload');

    Route::get('/content/view/{id}', 'SubModuleController@moduleAssignmentView')->name('moduleAssignmentView');
    Route::post('/content/add_mark', 'SubModuleController@addMarks')->name('addMarks');
    Route::post('/content/update_assignment', 'SubModuleController@uploadStdAnsSheet')->name('asdAnsUpload');
    Route::post('/content/upload_mark_sheet', 'SubModuleController@uploadMarkSheet')->name('markSheetUpload');
});

Route::group(['prefix' => 'schedule_details'], function () {
    Route::get('/', 'ModuleController@scheduleView')->name('scheduleView');
});


