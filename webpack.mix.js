const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css/app.scss');


mix.styles([
    'resources/sass/custom.scss',
], 'public/css/custom.scss');

mix.scripts([
    'resources/js/common.js',
    'resources/js/module/main.js',
    'resources/js/subModule/main.js',
    'resources/js/scheduleDetail/main.js',
    'resources/js/classBoard/main.js',
    'resources/js/subscribeDetail/main.js',
    'resources/js/main.js',
], 'public/js/common.js');